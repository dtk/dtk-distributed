// dtkDistributedGraphTopology.h
//

#pragma once

#include <dtkDistributedExport>

#include <map>
#include <set>

#include "dtkDistributedArray.h"
#include "dtkDistributedNavigator.h"
#include "dtkDistributedContainer.h"
#include "dtkDistributedIterator.h"

#include <QtCore>

class dtkDistributedGraphTopology;

// /////////////////////////////////////////////////////////////////
// dtkDistributedGraphTopologyVertex declaration
// /////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////
// dtkDistributedGraphTopology declaration
// /////////////////////////////////////////////////////////////////

class DTKDISTRIBUTED_EXPORT dtkDistributedGraphTopology : public dtkDistributedContainer
{
public:

    using graph_type = dtkDistributedGraphTopology;
    using base_type = dtkDistributedContainer;
    using array_type = dtkDistributedArray<qlonglong>;
    using edge_set = std::set<qlonglong>;
    using edge_map = std::map<qlonglong, edge_set>;

    using neighbours = dtkDistributedNavigator<array_type>;

    using value_type = neighbours;
    using pointer = value_type*;
    using const_pointer = const value_type*;
    using reference = value_type&;
    using const_reference = const value_type&;

    using size_type = qlonglong;
    using difference_type = std::ptrdiff_t;

    using iterator = dtkDistributedIterator<graph_type>;
    using const_iterator = dtkDistributedIterator<graph_type>;

    enum GraphFile {
        MetisFormat,
        MetisDirectedFormat,
        MatrixMarketFormat
    };

    class vertex
    {
    public:
        using const_iterator = typename array_type::const_iterator;
        using graph_type = typename dtkDistributedGraphTopology::graph_type;

         vertex(const graph_type *, qlonglong);
         vertex(const vertex&);
        ~vertex(void) = default;

        vertex& operator = (const vertex&);

        qlonglong id(void) const;
        qlonglong neighbourCount(void) const;
        qlonglong neighbourPos(qlonglong j) const;
        qlonglong neighbourLocalPos(qlonglong j) const;

        const_iterator begin(void) const;
        const_iterator end(void) const;
        const_iterator cbegin(void) const;
        const_iterator cend(void) const;

        bool operator == (const vertex&) const;
        bool operator != (const vertex&) const;
        bool operator <  (const vertex&) const;
        bool operator <= (const vertex&) const;
        bool operator >  (const vertex&) const;
        bool operator >= (const vertex&) const;

        vertex& operator ++ (void);
        vertex& operator -- (void);

        vertex operator ++ (int);
        vertex operator -- (int);

        vertex& operator += (qlonglong j);
        vertex& operator -= (qlonglong j);

        vertex operator + (qlonglong j) const;
        vertex operator - (qlonglong j) const;

    private:
        void init(void);
        void advance(void);
        void advance(qlonglong j);
        void rewind(void);
        void rewind(qlonglong j);

        const graph_type *g = nullptr;
        qlonglong m_id = -1;
        const_iterator c_beg;
        const_iterator c_it;
        const_iterator v_it;
        const_iterator n_it;
        qlonglong first_pos = -1;
    };
    friend vertex;

    dtkDistributedGraphTopology(void);
    dtkDistributedGraphTopology(qlonglong vertex_count);
    dtkDistributedGraphTopology(qlonglong vertex_count, dtkDistributedMapper *mapper);
    dtkDistributedGraphTopology(const graph_type&);

    ~dtkDistributedGraphTopology(void);

    graph_type& operator = (const graph_type&);

    void rlock(void);
    void wlock(void);
    void unlock(void);

    void clear(void);
    void resize(qlonglong vertex_count);

    void addEdge(qlonglong from, qlonglong to);

    void assemble(void);
    void assembleDomainDecompositionFeatures(void);

    bool isAssembled(void) const;

    qlonglong vertexCount(void) const;
    qlonglong edgeCount(void) const;

    qlonglong vertexCount(qint32 wid) const;
    qlonglong edgeCount(qint32 wid) const;

    qlonglong neighbourCount(qlonglong vertex_id) const;

    value_type operator[](qlonglong vertex_id) const;

    qlonglong firstNeighbourPos(qlonglong vertex_id) const;
    qlonglong firstNeighbourId(qlonglong vertex_id) const;

    vertex beginVertex(void) const;
    vertex endVertex(void) const;

    iterator begin(void);
    iterator end(void);

    const_iterator begin(void) const;
    const_iterator end(void) const;

    const_iterator cbegin(void) const;
    const_iterator cend(void) const;

    void stats(void) const;

    dtkDistributedMapper *edgeMapper(void) const;

    bool read(const QString& filename, GraphFile format = MetisFormat);
    bool write(const QString& filename, GraphFile format = MetisDirectedFormat);

    template <typename T = double>
    bool readWithValues(const QString& filename, GraphFile format, dtkDistributedArray<T> *&values);

    void setEdgeCountArray(array_type *edge_count_array);
    void setNeighbourCountArray(array_type *neighbour_count_array);
    void setEdgeToVertexArray(array_type *edge_to_vertex_array);
    void setVertexToEdgeArray(array_type *vertex_to_edge_array);
    void setAssembleFlag(bool is_assembled);

    const array_type *vertexToEdgeArray(void) const;
          array_type *vertexToEdgeArray(void);

    const array_type *neighbourCountArray(void) const;
          array_type *neighbourCountArray(void);

    struct domain_decomposition
    {
        edge_map map;
        edge_map map_hybrid;
        edge_map map_remote;

        edge_map map_itf;
        qlonglong local_intern_size;
        qlonglong local_itf_size;
        qlonglong global_itf_size;

        dtkDistributedMapper *mapper = nullptr;
        array_type *positions = nullptr;

        std::vector<qlonglong> local_vertex_to_edge;
        std::vector<qlonglong> local_edge_to_vertex;

        std::map<qlonglong, qlonglong> glob_to_loc;
        std::vector<qlonglong> loc_to_glob;
    } m_dd;

protected:
    void initialize(void);

    void buildDomainDecompositionMaps(void);
    void buildDomainDecompositionData(void);

    bool m_is_assembled = false;

    array_type *m_edge_count = nullptr;       // number of edges for each worker
    array_type *m_neighbour_count = nullptr;  // number of neighbours (edges) for each vertex
    array_type *m_vertex_to_edge = nullptr;   // for each vertex, store the global index of edges in the m_edge_to_vertex array
    array_type *m_edge_to_vertex = nullptr;   // store the list of edges in vertex order
};

// /////////////////////////////////////////////////////////////////

#include "dtkDistributedGraphTopology.tpp"

//
// dtkDistributedGraphTopology.h ends here
