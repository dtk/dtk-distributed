// dtkDistributedBufferManagerQThread.h
//

#pragma once

#include <dtkDistributedExport>

#include "dtkDistributedBufferManager.h"

#include <QtCore>

class dtkDistributedCommunicator;

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferManagerQThread
// ///////////////////////////////////////////////////////////////////

class DTKDISTRIBUTED_EXPORT dtkDistributedBufferManagerQThread : public dtkDistributedBufferManager
{
public:
     dtkDistributedBufferManagerQThread(dtkDistributedCommunicator *comm);
    ~dtkDistributedBufferManagerQThread(void);

    bool shouldCache(qint32 owner) override;

    void rlock(qint32 wid) override;
    void rlock(void) override;
    void wlock(qint32 wid) override;
    void wlock(void) override;

    void unlock(qint32 wid) override;
    void unlock(void) override;

    bool locked(qint32 wid) override;

    void get(qint32 from, std::size_t position, void *array, std::size_t nelements = 1) override;
    void put(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) override;
    void addAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) override;
    void subAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) override;
    void mulAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) override;
    void divAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) override;
    bool compareAndSwap(qint32 dest, std::size_t position, void *array, void *compare) override;

    bool canHandleOperationManager(void) override;

    void *allocate(std::size_t objectSize, std::size_t capacity, int metatype_id) override;
    void  deallocate(void *buffer, std::size_t objectSize) override;

    class dtkDistributedBufferManagerQThreadPrivate *d = nullptr;
};

//
// dtkDistributedBufferManagerQThread.h ends here
