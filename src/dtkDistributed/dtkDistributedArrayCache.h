// dtkDistributedArrayCache.h
//

#pragma once

#include <QtCore>

#include <vector>

template <typename T> class dtkDistributedArray;

// ///////////////////////////////////////////////////////////////////
// dtkDistributedArrayCache
// ///////////////////////////////////////////////////////////////////

template <typename T, int Prealloc = 128> class dtkDistributedArrayCache
{
public:
    using Line = QVarLengthArray<T, Prealloc>;

             dtkDistributedArrayCache(dtkDistributedArray<T> *a);
             dtkDistributedArrayCache(dtkDistributedArray<T> *a, int length);
    virtual ~dtkDistributedArrayCache(void) = default;

    void init(void);
    void clear(void);
    void resize(int);
    T value(std::size_t entry_id, qint32 owner);
    double hitrate(void) const;

private:
    int m_length = 32;
    std::vector<Line> m_lines;
    std::vector<std::size_t> m_ids;
    std::size_t m_hit = 0;
    std::size_t m_miss = 0;
    int m_last_line = 0;
    int m_current_line = -1;
    dtkDistributedArray<T> *m_array = nullptr;
};

// ///////////////////////////////////////////////////////////////////

#include "dtkDistributedArray.h"

template <typename T, int Prealloc>
inline dtkDistributedArrayCache<T, Prealloc>::dtkDistributedArrayCache(dtkDistributedArray<T> *a) : m_array(a)
{
    this->init();
}

template <typename T, int Prealloc>
inline dtkDistributedArrayCache<T, Prealloc>::dtkDistributedArrayCache(dtkDistributedArray<T> *a, int length) : m_array(a), m_length(length)
{
    this->init();
}

template <typename T, int Prealloc>
inline void dtkDistributedArrayCache<T, Prealloc>::init(void)
{
    m_ids.resize(2*m_length, 0);
    m_lines.resize(m_length);
    for (int i = 0; i < m_length; ++i) {
        m_lines[i].resize(Prealloc);
    }
}

template <typename T, int Prealloc>
inline void dtkDistributedArrayCache<T, Prealloc>::clear(void)
{
    std::fill(m_ids.begin(), m_ids.end(), 0);
    m_last_line = 0;
    m_current_line = -1;
}

template <typename T, int Prealloc>
inline void dtkDistributedArrayCache<T, Prealloc>::resize(int length)
{
    this->clear();
    m_length = length;
    this->init();
}

template <typename T, int Prealloc>
inline T dtkDistributedArrayCache<T, Prealloc>::value(std::size_t entry_id, qint32 owner)
{
    // Cache lines are updated from back to front in a circular manner.
    // m_last_line index gives the id of the line that was the most recently updated.
    // m_current_line is the index of the line that contains the previous asked entry_id.

    // After initialization, m_current_line is non-sense, hence the last line is used to store the entry value and its followers.

    // Next, we try to find a line that already contains the required entry.
    // To do so, we perform a circular lookup through the cache lines.
    // We first search in the current line given by m_current_line index.
    // Then we search in the next lines until the back.
    // If the entry is still not found, we search from the front line to the current line.

    // If the entry is not found, the line before the last updated one is updated.
    // When the front line is reached, we loop to the back line.

    bool entry_found = false;

    if (m_current_line != -1) {
        for (int i = m_current_line; i < m_length; ++i) {
            if (entry_id < m_ids[2*i+1] && entry_id >= m_ids[2*i]) {
                m_current_line = i;
                entry_found = true;
                break;
            }
        }
        for (int i = 0; i < m_current_line; ++i) {
            if (entry_id < m_ids[2*i+1] && entry_id >= m_ids[2*i]) {
                m_current_line = i;
                entry_found = true;
                break;
            }
        }
    }

    // If not then find an available cache line and store remote values into it
    if (!entry_found) {
        ++m_miss;
        if (m_last_line == 0) {
            m_current_line = m_length - 1;
        } else {
            m_current_line = m_last_line - 1;
        }
        m_last_line = m_current_line;

        qlonglong size = Prealloc;
        m_array->copyIntoArray(entry_id, owner, m_lines[m_current_line].data(), size);
        m_lines[m_current_line].resize(size);

        m_ids[2*m_current_line] = entry_id;
        m_ids[2*m_current_line + 1] = entry_id + size;

    } else {
        ++m_hit;
    }

    return m_lines[m_current_line].at(entry_id - m_ids[2*m_current_line]);
}

template <typename T, int Prealloc>
inline double dtkDistributedArrayCache<T, Prealloc>::hitrate(void) const
{
    double sum = m_miss + m_hit;
    qDebug() << Q_FUNC_INFO << "misses:" << m_miss << "hits:" << m_hit ;

    return sum != 0 ? m_hit / sum : 0;
}

//
// dtkDistributedArrayCache.h ends here
