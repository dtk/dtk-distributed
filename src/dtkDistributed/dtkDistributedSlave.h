// dtkDistributedSlave.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

class QTcpSocket;

class DTKDISTRIBUTED_EXPORT dtkDistributedSlave : public QObject
{
    Q_OBJECT

public:
     dtkDistributedSlave(void);
     dtkDistributedSlave(QTcpSocket *socket);
    ~dtkDistributedSlave(void);

    static QString jobId(void);

    bool    isConnected(void);
    bool isDisconnected(void);

    void    connectFromJob(const QUrl& server);
    void disconnectFromJob(const QUrl& server);

    QTcpSocket *socket(void);
    void flush(void);

signals:
    void    connected(const QUrl& server);
    void disconnected(const QUrl& server);

public slots:
    void    connect(const QUrl& server);
    void disconnect(const QUrl& server);

protected slots:
    void error(int error);

private:
    class dtkDistributedSlavePrivate *d = nullptr;
};

//
// dtkDistributedSlave.h ends here
