// dtkDistributedMapper.cpp
//

#include "dtkDistributedMapper.h"

#include <dtkLog>

#include <QtCore>

#include <vector>

// /////////////////////////////////////////////////////////////////
// dtkDistributedMapperPrivate interface
// /////////////////////////////////////////////////////////////////

class dtkDistributedMapperPrivate
{
public:
    void clear(void);

public:
    void setMapping(qlonglong id_number, qint32 pu_number);
    void initMap(qlonglong map_size, qint32 pu_size);
    void setMap(qlonglong local_map_size, qint32 pu_id);

public:
    qlonglong localToGlobal(qlonglong local_id, qint32 pu_id) const;

    qlonglong globalToLocal(qlonglong global_id) const;
    qlonglong globalToLocal(qlonglong global_id, qint32 owner) const;

    qlonglong count(void) const;
    qlonglong count(qint32 pu_id) const;
    qlonglong countMax(void) const;

    qint32 owner(qlonglong global_id) const;

public:
    QAtomicInt ref = QAtomicInt(0);

    qlonglong id_count = 0;
    qint32 pu_count = 0;
    mutable qint32 last_pu_id = 0;

    std::vector<qlonglong> map;
};

// /////////////////////////////////////////////////////////////////
// dtkDistributedMapperPrivate implementation
// /////////////////////////////////////////////////////////////////

void dtkDistributedMapperPrivate::clear(void)
{
    id_count = qlonglong(0);
    pu_count = qint32(0);
    last_pu_id = qint32(0);
    this->map.clear();
}

void dtkDistributedMapperPrivate::setMapping(qlonglong id_number, qint32 pu_number)
{
    this->map.clear();
    this->id_count = id_number;
    this->pu_count = pu_number;
    this->map.reserve(this->pu_count + 1);

    if (this->pu_count == 1) {
        this->map.emplace_back(0);

    } else if (this->id_count < this->pu_count  ) {

        dtkDebug() << Q_FUNC_INFO << "Number of ids less than process count: NOT YET IMPLEMENTED";
        return;

    } else {
        qlonglong step = this->id_count / this->pu_count + 1;

        qlonglong rest = this->id_count % this->pu_count;

        for (qlonglong i = 0; i < rest + 1; ++i) {
            this->map.emplace_back(i * step);
        }

        qlonglong last = rest * step;

        for (qlonglong i = 1; i < this->pu_count - rest; ++i) {
            this->map.emplace_back(last + i * (step - 1));
        }
    }

    this->map.emplace_back(this->id_count);
}

void dtkDistributedMapperPrivate::initMap(qlonglong map_size, qint32 pu_size)
{
    this->id_count = map_size;
    this->pu_count = pu_size;

    this->map.resize(this->pu_count + 1);
    this->map[this->pu_count] = map_size;
}

void dtkDistributedMapperPrivate::setMap(qlonglong offset, qint32 pu_id)
{
    this->map[pu_id] = offset;
    if (pu_id == this->pu_count) {
        this->id_count = offset;
    }
}

qlonglong dtkDistributedMapperPrivate::localToGlobal(qlonglong local_id, qint32 pu_id) const
{
    return ( local_id + this->map[pu_id] );
}

qlonglong dtkDistributedMapperPrivate::globalToLocal(qlonglong global_id) const
{
    qlonglong owner = this->owner(global_id);

    return ( global_id - this->map[owner] );
}

qlonglong dtkDistributedMapperPrivate::globalToLocal(qlonglong global_id, qint32 owner) const
{
    return ( global_id - this->map[owner] );
}

qlonglong dtkDistributedMapperPrivate::count(void) const
{
    return this->id_count ;
}

qlonglong dtkDistributedMapperPrivate::count(qint32 pu_id) const
{
    return ( this->map[pu_id + 1] - this->map[pu_id] );
}

qlonglong dtkDistributedMapperPrivate::countMax(void) const
{
    qlonglong count_max = this->map[1] - this->map[0];

    for (qlonglong i = 1; i < this->map.size() - 1; ++i) {
        count_max = std::max(count_max, this->map[i + 1] - this->map[i]);
    }
    return count_max;
}

qint32 dtkDistributedMapperPrivate::owner(qlonglong global_id) const
{
    if (global_id >= this->map[last_pu_id + 1]) {
        ++last_pu_id;
        while (global_id >= this->map[last_pu_id + 1]) {
            ++last_pu_id;
        }
    } else if (global_id  < this->map[last_pu_id]) {
        --last_pu_id;
        while (global_id < this->map[last_pu_id]) {
            --last_pu_id;
        }
    }
    return last_pu_id;
}

// /////////////////////////////////////////////////////////////////
// dtkDistributedMapper implementation
// /////////////////////////////////////////////////////////////////

dtkDistributedMapper::dtkDistributedMapper(void) : QObject(), d(new dtkDistributedMapperPrivate)
{
}

dtkDistributedMapper::dtkDistributedMapper(const dtkDistributedMapper& o) : QObject(), d(new dtkDistributedMapperPrivate)
{
    d->id_count = o.d->id_count;
    d->pu_count = o.d->pu_count;
    d->last_pu_id = o.d->last_pu_id;
    d->map = o.d->map;
}

dtkDistributedMapper::dtkDistributedMapper(dtkDistributedMapper&& o) : QObject(), d(o.d)
{
    o.d = nullptr;
}

dtkDistributedMapper::~dtkDistributedMapper(void)
{
    if (d) {
        delete d;
        d = nullptr;
    }
}

dtkDistributedMapper *dtkDistributedMapper::scaledClone(qlonglong factor) const
{
    dtkDistributedMapper *mapper = new dtkDistributedMapper;

    mapper->initMap(d->id_count * factor, d->pu_count);

    for (qlonglong i = 0; i < d->pu_count; ++i) {
        mapper->setMap(d->map[i] * factor, i);
    }

    return mapper;
}

bool dtkDistributedMapper::deref(void)
{
    return d->ref.deref();
}

bool dtkDistributedMapper::ref(void)
{
    return d->ref.ref();
}

void dtkDistributedMapper::clear(void)
{
    d->clear();
}

void dtkDistributedMapper::setMapping(qlonglong id_count, qint32 pu_count)
{
    d->setMapping(id_count, pu_count);
}

void dtkDistributedMapper::initMap(qlonglong map_size, qint32 pu_size)
{
    d->initMap(map_size, pu_size);
}

void dtkDistributedMapper::setMap(qlonglong offset, qint32 pu_id)
{
    d->setMap(offset, pu_id);
}

qlonglong dtkDistributedMapper::localToGlobal(qlonglong local_id, qint32 pu_id) const
{
    return d->localToGlobal(local_id, pu_id);
}

qlonglong dtkDistributedMapper::globalToLocal(qlonglong global_id) const
{
    return d->globalToLocal(global_id);
}

qlonglong dtkDistributedMapper::globalToLocal(qlonglong global_id, qint32 owner ) const
{
    return d->globalToLocal(global_id, owner);
}

qlonglong dtkDistributedMapper::count(void) const
{
    return d->count();
}

qlonglong dtkDistributedMapper::count(qint32 pu_id) const
{
    Q_ASSERT(pu_id < d->pu_count);
    return d->count(pu_id);
}

qlonglong dtkDistributedMapper::countMax(void) const
{
    return d->countMax();
}

qlonglong dtkDistributedMapper::firstIndex(qint32 pu_id) const
{
    Q_ASSERT(pu_id < d->pu_count);
    return d->map[pu_id];
}

qlonglong dtkDistributedMapper::lastIndex(qint32 pu_id) const
{
    Q_ASSERT(pu_id < d->pu_count);
    return d->map[pu_id + 1] - 1;
}

qint32 dtkDistributedMapper::owner(qlonglong global_id) const
{
    Q_ASSERT(global_id < d->id_count);
    return d->owner(global_id);
}

// check validity of mapper: each pu_id should have at least one entry
bool dtkDistributedMapper::check(void)
{
    for (qlonglong i = 0; i < d->pu_count; ++i) {
        if (d->count(i) < 1) {
            dtkError() << "Bad mapper: wid" << i  << "has no values";
            return false;
        }
    }
    return true;
}

QDebug operator<<(QDebug dbg, dtkDistributedMapper  &mapper)
{
    dbg << mapper.d->map;
    return dbg;
}

QDebug operator<<(QDebug dbg, dtkDistributedMapper  *mapper)
{
    dbg << mapper->d->map;
    return dbg;
}
