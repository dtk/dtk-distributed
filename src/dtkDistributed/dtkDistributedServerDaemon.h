// dtkDistributedServerDaemon.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>
#include <QtNetwork>

class dtkDistributedResourceManager;

class DTKDISTRIBUTED_EXPORT dtkDistributedServerDaemon : public QTcpServer
{
    Q_OBJECT

public:
     dtkDistributedServerDaemon(quint16 port, QObject *parent = 0);
    ~dtkDistributedServerDaemon(void);

    void setManager(const QString& type);
    void setWorkingDirectory(const QString& type);

    dtkDistributedResourceManager *manager(void);
    QString workingDirectory(void);
    void endjob(const QString& jobid);

    void waitForConnection(int rank, QString jobid);
    QByteArray waitForData(int rank, QString jobid);

protected:
    void incomingConnection(qintptr descriptor);

private slots:
    virtual void read(void);
    void discard(void);

private:
    class dtkDistributedServerDaemonPrivate *d = nullptr;
};

//
// dtkDistributedServerDaemon.h ends here
