// dtkDistributedBufferManager.h
//

#pragma once

#include <dtkDistributedExport>

#include <dtkCore>

#include <functional>

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferOperationManager
// ///////////////////////////////////////////////////////////////////

class DTKDISTRIBUTED_EXPORT dtkDistributedBufferOperationManager
{
public:
    virtual ~dtkDistributedBufferOperationManager(void) = default;

    virtual void addAssign(char *result, void *source, std::size_t count) = 0;
    virtual void subAssign(char *result, void *source, std::size_t count) = 0;
    virtual void mulAssign(char *result, void *source, std::size_t count) = 0;
    virtual void divAssign(char *result, void *source, std::size_t count) = 0;

    virtual bool compareAndSwap(char *result, void *source, void *compare) = 0;

    virtual void negate(void *result, void *source, std::size_t count) = 0;
    virtual void invert(void *result, void *source, std::size_t count) = 0;
};

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferOperationManagerTyped
// ///////////////////////////////////////////////////////////////////

template <typename T>
class dtkDistributedBufferOperationManagerTyped : public dtkDistributedBufferOperationManager
{
public:
     dtkDistributedBufferOperationManagerTyped(void) = default;
    ~dtkDistributedBufferOperationManagerTyped(void) = default;

    void addAssign(char *result, void *source, std::size_t count) override;
    void subAssign(char *result, void *source, std::size_t count) override;
    void mulAssign(char *result, void *source, std::size_t count) override;
    void divAssign(char *result, void *source, std::size_t count) override;

    bool compareAndSwap(char *result, void *source, void *compare) override;

    void negate(void *result, void *source, std::size_t count) override;
    void invert(void *result, void *source, std::size_t count) override;

private:
    QMutex mutex;
};

// ///////////////////////////////////////////////////////////////////

template <typename T>
inline void dtkDistributedBufferOperationManagerTyped<T>::addAssign(char *result, void *source, std::size_t count)
{
    T *Tresult = reinterpret_cast<T *>(result);
    T *Tsource = reinterpret_cast<T *>(source);
    std::transform(Tresult, Tresult + count, Tsource, Tresult, std::plus<T>());
}

template <typename T>
inline void dtkDistributedBufferOperationManagerTyped<T>::subAssign(char *result, void *source, std::size_t count)
{
    T *Tresult = reinterpret_cast<T *>(result);
    T *Tsource = reinterpret_cast<T *>(source);
    std::transform(Tresult, Tresult + count, Tsource, Tresult, std::minus<T>());
}

template <typename T>
inline void dtkDistributedBufferOperationManagerTyped<T>::mulAssign(char *result, void *source, std::size_t count)
{
    T *Tresult = reinterpret_cast<T *>(result);
    T *Tsource = reinterpret_cast<T *>(source);
    std::transform(Tresult, Tresult + count, Tsource, Tresult, std::multiplies<T>());
}

template <typename T>
inline void dtkDistributedBufferOperationManagerTyped<T>::divAssign(char *result, void *source, std::size_t count)
{
    T *Tresult = reinterpret_cast<T *>(result);
    T *Tsource = reinterpret_cast<T *>(source);
    std::transform(Tresult, Tresult + count, Tsource, Tresult, std::divides<T>());
}

template <typename T>
inline bool dtkDistributedBufferOperationManagerTyped<T>::compareAndSwap(char *result, void *source, void *compare)
{
    T *Tresult = reinterpret_cast<T *>(result);
    T *Tsource = reinterpret_cast<T *>(source);
    T *Tcompare = reinterpret_cast<T *>(compare);

    mutex.lock();

    if (*Tresult == *Tcompare) {
        *Tresult = *Tsource ;
        mutex.unlock();
        return true;
    } else {
        mutex.unlock();
        return false;
    }

    /*  m_atomic_value.store(Tresult) */
    /* if ( std::atomic_compare_exchange_strong( &m_atomic_value, Tcompare , *Tsource  ) ) { */
    /*     *Tresult = m_atomic_value.load(); */
}

template <typename T>
inline void dtkDistributedBufferOperationManagerTyped<T>::negate(void *result, void *source, std::size_t count)
{
    T *Tresult = reinterpret_cast<T *>(result);
    T *Tsource = reinterpret_cast<T *>(source);
    std::transform(Tsource, Tsource + count, Tresult, [](auto&& s) { return -1 * s; });
}

template <typename T>
inline void dtkDistributedBufferOperationManagerTyped<T>::invert(void *result, void *source, std::size_t count)
{
    T *Tresult = reinterpret_cast<T *>(result);
    T *Tsource = reinterpret_cast<T *>(source);
    std::transform(Tsource, Tsource + count, Tresult, [](auto&& s) { return 1 / s; });
}

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferManager
// ///////////////////////////////////////////////////////////////////

class DTKDISTRIBUTED_EXPORT dtkDistributedBufferManager
{
public:
             dtkDistributedBufferManager(void) = default;
    virtual ~dtkDistributedBufferManager(void) = default;

    template <typename T> T *allocate(std::size_t capacity);
    template <typename T> void  deallocate(T *&buffer);

    virtual bool shouldCache(qint32 owner) = 0;

    virtual void rlock(qint32 wid) = 0;
    virtual void rlock(void) = 0;
    virtual void wlock(qint32 wid) = 0;
    virtual void wlock(void) = 0;

    virtual void unlock(qint32 wid) = 0;
    virtual void unlock(void) = 0;

    virtual bool locked(qint32 wid) = 0;

    virtual void get(qint32 from, std::size_t position, void *array, std::size_t count = 1) = 0;
    virtual void put(qint32 dest, std::size_t position, void *array, std::size_t count = 1) = 0;
    virtual void addAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) = 0;
    virtual void subAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) = 0;
    virtual void mulAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) = 0;
    virtual void divAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) = 0;
    virtual bool compareAndSwap(qint32 dest, std::size_t position, void *array, void *compare) = 0;

protected:
    virtual bool canHandleOperationManager(void);

    virtual void *allocate(std::size_t objectSize, std::size_t capacity, int metatype_id) = 0;
    virtual void  deallocate(void *buffer, std::size_t objectSize) = 0;

    dtkDistributedBufferOperationManager *operation_manager = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferManager templated member functions
// ///////////////////////////////////////////////////////////////////

inline bool dtkDistributedBufferManager::canHandleOperationManager(void)
{
    return true;
}

template <typename T>
inline T *dtkDistributedBufferManager::allocate(std::size_t capacity)
{
    if (this->canHandleOperationManager() && !this->operation_manager) {
        this->operation_manager = new dtkDistributedBufferOperationManagerTyped<T>();
    }

    return static_cast<T *>(this->allocate(sizeof(T), capacity, qMetaTypeId<T>()));
}

template <typename T>
inline void dtkDistributedBufferManager::deallocate(T *&buffer)
{
    if (this->operation_manager && this->canHandleOperationManager()) {
        delete this->operation_manager;
        this->operation_manager = nullptr;
    }

    this->deallocate(buffer, sizeof(T));
    buffer = nullptr;
}

//
// dtkDistributedBufferManager.h ends here
