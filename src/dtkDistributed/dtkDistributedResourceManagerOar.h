// dtkDistributedResourceManagerOar.h
//

#pragma once

#include <dtkDistributedExport>

#include "dtkDistributedResourceManager.h"

class DTKDISTRIBUTED_EXPORT dtkDistributedResourceManagerOar : public dtkDistributedResourceManager
{
    Q_OBJECT

public:
     dtkDistributedResourceManagerOar(void);
    ~dtkDistributedResourceManagerOar(void);

public slots:
    QByteArray status(void) override;
    QString submit(QString input) override;
    QString deljob(QString input) override;

public:
    bool   jobStatus(const QString &);
    bool nodesStatus(const QString &);

private:
    class dtkDistributedResourceManagerOarPrivate *d;
};

//
// dtkDistributedResourceManagerOar.h ends here
