// dtkDistributedCommunicatorQThread.cpp
//

#include "dtkDistributedCommunicatorQThread.h"

#include "dtkDistributedBufferManagerQThread.h"

#include "dtkDistributedRequest.h"

#include "dtkDistributedDoubleInt.h"

#include <dtkLog>

#include <QtCore>

namespace dtkDistributed {

    class spinLock : private QAtomicInt
    {
    public:
         spinLock(void);
        ~spinLock(void) = default;

        void lock(void);
        void unlock(void);
        bool tryLock(void);

    private:
        static const int Unlocked = 1;
        static const int Locked = 0;
    };

    spinLock::spinLock(void) : QAtomicInt(Unlocked)
    {
    }

    void spinLock::lock(void)
    {
        while (!testAndSetOrdered(Unlocked, Locked));
    }

    void spinLock::unlock(void)
    {
        while (!testAndSetOrdered(Locked, Unlocked));
    }

    bool spinLock::tryLock(void)
    {
        return testAndSetOrdered(Unlocked, Locked);
    }

    // ///////////////////////////////////////////////////////////////////

    template <typename T>
    void applyOperation(T *output, T *input, std::size_t size, dtkDistributedCommunicator::OperationType operationType)
    {
        auto o_begin = output;
        auto o_end = output + size;
        auto i_begin = input;

        switch (operationType) {
        case dtkDistributedCommunicator::Sum:
            std::transform(o_begin, o_end, i_begin, o_begin, std::plus<T>());
            break;

        case dtkDistributedCommunicator::Min:
        case dtkDistributedCommunicator::MinLoc:
            std::transform(o_begin, o_end, i_begin, o_begin, [](auto&& o, auto&& i) { return o < i ? o : i; });
            break;

        case dtkDistributedCommunicator::Max:
        case dtkDistributedCommunicator::MaxLoc:
            std::transform(o_begin, o_end, i_begin, o_begin, [](auto&& o, auto&& i) { return o > i ? o : i; });
            break;

        case dtkDistributedCommunicator::Product:
            std::transform(o_begin, o_end, i_begin, o_begin, std::multiplies<T>());
            break;

        case dtkDistributedCommunicator::None:
            break;

        default:
            qWarning() << "operation" << operationType << "not implemented in qthread plugin";
            break;
        }
    }

    // ///////////////////////////////////////////////////////////////////

    class task : public QRunnable
    {
    public:
        dtkDistributedCommunicatorQThread *m_comm;
        QRunnable *m_task;
        qint32 m_wid;

        void run(void) override;
    };

    void task::run(void)
    {
        Q_ASSERT(m_comm);
        Q_ASSERT(m_task);

        m_comm->setWid(m_wid);
        dtkTrace() << "run task wid" << m_wid;
        m_comm->barrier();
        m_task->run();
        m_comm->barrier();
    }

    // ///////////////////////////////////////////////////////////////////

    class barrier
    {
    public:
         barrier(int count);
        ~barrier(void) = default;

        void wait(void);

    private:
        int m_count;
        int initial_count;
        QMutex mutex;
        QWaitCondition condition;
    };

    barrier::barrier(int count) : m_count(count), initial_count(count)
    {
    }

    void barrier::wait(void)
    {
        mutex.lock();
        --m_count;
        if (m_count > 0) {
            condition.wait(&mutex);
            mutex.unlock();
        } else {
            m_count = initial_count;
            mutex.unlock();
            condition.wakeAll();
        }
    }

} // end namespace

// ///////////////////////////////////////////////////////////////////
// qthDistributedRequest
// ///////////////////////////////////////////////////////////////////

class dtkDistributedRequestQThread : public dtkDistributedRequest
{
public:
    void *m_data;
    qint64  m_size;
    QMetaType::Type m_dataType;
    qint32 m_source;
    qint32 m_tag;

public:
    dtkDistributedRequestQThread(void *&data, qint64 size, QMetaType::Type dataType, qint32 source, qint32 tag);
};

dtkDistributedRequestQThread::dtkDistributedRequestQThread(void *&data, qint64 size, QMetaType::Type dataType, qint32 source, qint32 tag) :
    m_data(data), m_size(size), m_dataType(dataType), m_source(source), m_tag(tag)
{
}

// ///////////////////////////////////////////////////////////////////
// class dtkDistributedLocalMessageQThread
// ///////////////////////////////////////////////////////////////////

class dtkDistributedLocalMessageQThread
{
public:
     dtkDistributedLocalMessageQThread(void) = default;
     dtkDistributedLocalMessageQThread(QVariant v, qint32 source, qint32 tag);
     dtkDistributedLocalMessageQThread(QByteArray a, qint32 source, qint32 tag);
     dtkDistributedLocalMessageQThread(void *buf, std::size_t bytesize, qint32 source, qint32 tag);
    ~dtkDistributedLocalMessageQThread(void);

public:
    qint32 tag = 0;
    qint32 source = -1;
    QVariant variant;
    QByteArray array;
    void *buffer = nullptr;
    std::size_t buffer_size = 0;
};

// ///////////////////////////////////////////////////////////////////

dtkDistributedLocalMessageQThread::dtkDistributedLocalMessageQThread(QVariant v, qint32 source, qint32 tag)
{
    this->tag    = tag;
    this->source = source;
    this->variant   = v;
}

dtkDistributedLocalMessageQThread::dtkDistributedLocalMessageQThread(QByteArray a, qint32 source, qint32 tag)
{
    this->tag    = tag;
    this->source = source;
    this->array   = a;
}

dtkDistributedLocalMessageQThread::dtkDistributedLocalMessageQThread(void *b, std::size_t bytesize, qint32 source, qint32 tag)
{
    buffer_size  = bytesize;
    this->tag    = tag;
    this->source = source;
    this->buffer = malloc(bytesize);
    memcpy(this->buffer, b, bytesize);
}

dtkDistributedLocalMessageQThread::~dtkDistributedLocalMessageQThread(void)
{
    if (buffer) {
        free(buffer);
    }
    buffer = nullptr;
}

// /////////////////////////////////////////////////////////////////
// dtkDistributedCommunicatorQThreadPrivate
// /////////////////////////////////////////////////////////////////

class dtkDistributedCommunicatorQThreadPrivate
{
public:
    std::size_t id = 0;  // for buffers
    qint32 size = 1;
    QVector<void *> buffers;
    QVector<QReadWriteLock *> locks;
    QHash<QThread *, qint32 > wids;
    QVector<QList<dtkDistributedLocalMessageQThread *>> msgbox;
    QVector<QList<dtkDistributedLocalMessageQThread *>> freelist;
    QVector<dtkDistributed::spinLock *> msg_mutex;
    QVector<std::size_t> size_map; //store the size of items
    QVector<bool> is_locked;
    bool active = false;

    QThreadPool pool;

    QVector<std::size_t> buffer_ids;
    QVector<QVector<std::size_t>> buffer_map;
    QMutex mutex; //global mutex

    dtkDistributedBufferManager *buffer_manager = nullptr;

    dtkDistributed::barrier *barrier = nullptr;

    bool initialized = false;
    bool use_gui = false;
};

// /////////////////////////////////////////////////////////////////
// dtkDistributedCommunicatorQThread
// /////////////////////////////////////////////////////////////////

dtkDistributedCommunicatorQThread::dtkDistributedCommunicatorQThread(void) : dtkDistributedCommunicator(), d(new dtkDistributedCommunicatorQThreadPrivate)
{
}

dtkDistributedCommunicatorQThread::~dtkDistributedCommunicatorQThread(void)
{
    d->mutex.lock();

    for (int i = 0; i < d->msg_mutex.size(); ++i) {
        if (d->msg_mutex[i]) {
            delete d->msg_mutex[i];
        }
    }

    for (int i = 0; i < d->freelist.size(); ++i) {
        for (dtkDistributedLocalMessageQThread *msg : d->freelist[i]) {
            if (msg) {
                delete msg;
            }
        }
        d->freelist[i].clear();
    }

    d->msg_mutex.clear();
    d->mutex.unlock();

    this->uninitialize();

    if (d->barrier) {
        delete d->barrier;
    }

    delete d;
    d = nullptr;
}

void dtkDistributedCommunicatorQThread::spawn(QStringList hostnames, QString wrapper, QMap<QString, QString> options)
{
    Q_UNUSED(wrapper);
    Q_UNUSED(options);

    qint32 np = hostnames.count();

    if (options.contains("gui")) {
        d->use_gui = options.value("gui") == "true";
    }

    dtkDebug() << "spawning" << np << "qthreads on "<< hostnames;

    if (!d->barrier)
        d->barrier = new dtkDistributed::barrier(np);

    d->size = np;
    if (d->use_gui) {
        d->pool.setMaxThreadCount(np-1);
    } else {
        d->pool.setMaxThreadCount(np);
    }
    d->pool.setExpiryTimeout(-1);
    d->buffer_ids.resize(np);
    d->buffer_map.resize(np);
    d->locks.resize(np);
    d->buffers.resize(np);
    d->msgbox.resize(np);
    d->freelist.resize(np);
    d->msg_mutex.resize(np);

    for (int i = 0; i < np; ++i) {
        dtkDistributed::spinLock *mutex = new dtkDistributed::spinLock;
        d->msg_mutex[i] = mutex;
        d->buffer_ids[i] = 0;
    }
}

void dtkDistributedCommunicatorQThread::unspawn(void)
{
    d->id = 0;
}

void dtkDistributedCommunicatorQThread::initialize(void)
{
    d->initialized = true;
}

// qthread communicators are always active
bool dtkDistributedCommunicatorQThread::active(void)
{
    return d->active;
}

void dtkDistributedCommunicatorQThread::exec(QRunnable *work)
{
    d->active = true;
    if (!d->use_gui) {
        for (int i = 0; i< d->size; ++i) {
            dtkDistributed::task *task = new dtkDistributed::task;
            task->m_wid  = i;
            task->m_comm = this;
            task->m_task = work;
            task->setAutoDelete(true);
            d->pool.start(task);
        }
    } else {
        dtkDebug() << "GUI application, run rank 0 in main thread";

        for (int i = 1; i < d->size; ++i) {
            dtkDistributed::task *task = new dtkDistributed::task;
            task->m_wid  = i;
            task->m_comm = this;
            task->m_task = work;
            task->setAutoDelete(true);
            d->pool.start(task);
        }

        dtkDebug() << "GUI application, run main thread code";
        dtkDistributed::task task;
        task.m_wid  = 0;
        task.m_comm = this;
        task.m_task = work;
        task.run();
        dtkDebug() << "GUI application, done, wait for other threads";
    }

    d->pool.waitForDone();
    d->active = false;
}

bool dtkDistributedCommunicatorQThread::initialized(void)
{
    return d->initialized;
}

void dtkDistributedCommunicatorQThread::uninitialize(void)
{
    d->pool.waitForDone();
    d->wids.clear();
}

void dtkDistributedCommunicatorQThread::barrier(void)
{
    if (!d->active) {
        return;
    }

    d->barrier->wait();
}

qint32 dtkDistributedCommunicatorQThread::wid(void)
{
    if (!d->active) {
        return 0;
    }

    return d->wids.value(QThread::currentThread(), 0);
}

void dtkDistributedCommunicatorQThread::setWid(qint32 id)
{
    d->mutex.lock();
    d->wids.insert(QThread::currentThread(), id);
    d->mutex.unlock();
}

qint32 dtkDistributedCommunicatorQThread::size(void)
{
    return d->active ? d->size : 1;
}

dtkDistributedBufferManager *dtkDistributedCommunicatorQThread::createBufferManager(void)
{
    this->barrier();
    d->mutex.lock();

    if (this->wid() == 0) {
        d->buffer_manager = new dtkDistributedBufferManagerQThread(this);
    }

    d->mutex.unlock();
    this->barrier();

    return d->buffer_manager;
}

void dtkDistributedCommunicatorQThread::destroyBufferManager(dtkDistributedBufferManager *&buffer_manager)
{
    this->barrier();
    d->mutex.lock();

    if (this->wid() == 0) {
        delete buffer_manager;
    }

    d->mutex.unlock();
    this->barrier();
    buffer_manager = nullptr;
}

void dtkDistributedCommunicatorQThread::send(void *data, std::size_t size, QMetaType::Type dataType, qint32 target, qint32 tag)
{
    qint32 source = wid();
    dtkDistributedLocalMessageQThread *msg;

    dtkDistributed::spinLock *mutex = d->msg_mutex[target];
    std::size_t bytesize = size * QMetaType(dataType).sizeOf();

    mutex->lock();

    if (d->freelist[target].isEmpty()) {
        msg = new dtkDistributedLocalMessageQThread(data, bytesize , source, tag);
    } else {
        msg = d->freelist[target].takeFirst();
        msg->tag = tag;
        msg->source = source;

        if (msg->buffer_size != bytesize) {
            msg->buffer = realloc(msg->buffer, bytesize);
            msg->buffer_size = bytesize;
        }

        memcpy(msg->buffer, data, bytesize);
    }

    d->msgbox[target].append(msg);
    mutex->unlock();
}

void dtkDistributedCommunicatorQThread::send(QByteArray& array, qint32 target, qint32 tag)
{
    qint32 source = wid();
    dtkDistributedLocalMessageQThread *msg;

    dtkDistributed::spinLock *mutex = d->msg_mutex[target];

    mutex->lock();

    if (d->freelist[target].isEmpty()) {
        msg = new dtkDistributedLocalMessageQThread(array, source, tag);
    } else {
        msg = d->freelist[target].takeFirst();
        msg->tag = tag;
        msg->source = source;
        msg->array = array;
    }

    d->msgbox[target].append(msg);
    mutex->unlock();
}

void dtkDistributedCommunicatorQThread::broadcast(void *data, std::size_t size, QMetaType::Type dataType, qint32 source)
{
    if (d->size == 1) {
        return;
    }

    qint32 me = wid();
    dtkDistributedLocalMessageQThread *msg ;
    std::size_t bytesize = size * QMetaType(dataType).sizeOf();

    if (me == source) {

        if (d->freelist[source].isEmpty()) {
            msg = new dtkDistributedLocalMessageQThread(data, bytesize, source, dtkDistributedCommunicator::BCAST_TAG);
        } else {
            msg = d->freelist[source].takeFirst();
            msg->source = source;
            msg->tag    = dtkDistributedCommunicator::BCAST_TAG;

            if (msg->buffer_size != bytesize) {
                msg->buffer = realloc(msg->buffer, bytesize);
                msg->buffer_size = bytesize;
            }

            memcpy(msg->buffer, data, bytesize);
        }

        for (int i = 0; i < d->size; ++i) {
            if (i != source) {
                d->msg_mutex[i]->lock();
                d->msgbox[i].append(msg);
                d->msg_mutex[i]->unlock();
            }
        }

        barrier();
        d->freelist[source].append(msg);
        //FIXME:should we lock the freelist ?
    } else {
        QList< dtkDistributedLocalMessageQThread * > *box = &(d->msgbox[me]);

        while (true) { // busy wait for a message

            d->msg_mutex[me]->lock();
            int size = box->size();

            for (int i = 0; i < size; ++i) {
                msg = box->at(i);

                if (msg->tag == dtkDistributedCommunicator::BCAST_TAG) {
                    msg = d->msgbox[me].takeAt(i);
                    d->msg_mutex[me]->unlock();
                    // FIXME: handle null pointer ?
                    memcpy(data, msg->buffer, bytesize);

                    d->barrier->wait();
                    return;
                }
            }

            d->msg_mutex[me]->unlock();
        }
    }
}
void dtkDistributedCommunicatorQThread::broadcast(QByteArray& array, qint32 source)
{
    if (d->size == 1) {
        return;
    }

    QVariant v(array);
    this->broadcast(v, source);
    qint32 me = wid();

    if (me != source) {
        array = v.toByteArray();
    }
}

void dtkDistributedCommunicatorQThread::broadcast(QVariant& v, qint32 source)
{
    if (d->size == 1) {
        return;
    }

    qint32 me = wid();
    dtkDistributedLocalMessageQThread *msg ;

    if (me == source) {

        if (d->freelist[source].isEmpty()) {
            msg = new dtkDistributedLocalMessageQThread(v, source, dtkDistributedCommunicator::BCAST_TAG);
        } else {
            msg = d->freelist[source].takeFirst();
            msg->source = source;
            msg->tag    = dtkDistributedCommunicator::BCAST_TAG;
            msg->variant = v;
        }

        for (int i = 0; i < d->size; ++i) {
            if (i != source) {
                d->msg_mutex[i]->lock();
                d->msgbox[i].append(msg);
                d->msg_mutex[i]->unlock();
            }
        }

        barrier();
        d->freelist[source].append(msg);
        //FIXME:should we lock the freelist ?
    } else {
        QList< dtkDistributedLocalMessageQThread * > *box = &(d->msgbox[me]);

        while (true) { // busy wait for a message

            d->msg_mutex[me]->lock();
            int size = box->size();

            for (int i = 0; i < size; ++i) {
                msg = box->at(i);

                if (msg->tag == dtkDistributedCommunicator::BCAST_TAG) {
                    msg = d->msgbox[me].takeAt(i);
                    d->msg_mutex[me]->unlock();

                    v = msg->variant;
                    d->barrier->wait();
                    return;
                }
            }

            d->msg_mutex[me]->unlock();
        }
    }
}

void dtkDistributedCommunicatorQThread::receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag)
{
    qint32 target = wid();

    dtkDistributed::spinLock *mutex = d->msg_mutex[target];

    dtkDistributedLocalMessageQThread *msg ;

    QList< dtkDistributedLocalMessageQThread * > *box = &( d->msgbox[target]);
    std::size_t bytesize = size * QMetaType(dataType).sizeOf();

    while (true) { // busy wait for a message
        mutex->lock();
        int bsize = box->size();

        for (int i = 0; i < bsize; ++i) {
            msg = box->at(i);

            if (msg->tag == tag && msg->source == source ) {
                memcpy(data, msg->buffer, bytesize);
                d->msgbox[target].removeAt(i);
                d->freelist[target].append(msg);
                mutex->unlock();
                return;
            }
        }

        mutex->unlock();
    }
}
void dtkDistributedCommunicatorQThread::receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status)
{
    qWarning() << "receive with status not yet implemented in qthread plugin";
    receive (data, size, dataType, source, tag);
}

void dtkDistributedCommunicatorQThread::receive(QByteArray& array, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status)
{
    receive(array, source, tag);
}

void dtkDistributedCommunicatorQThread::receive(QByteArray& array, qint32 source, qint32 tag)
{
    qint32 target = wid();

    dtkDistributed::spinLock *mutex = d->msg_mutex[target];

    dtkDistributedLocalMessageQThread *msg ;

    QList< dtkDistributedLocalMessageQThread * > *box = &( d->msgbox[target]);

    while (true) { // busy wait for a message
        mutex->lock();
        int size = box->size();

        for (int i = 0; i < size; ++i) {
            msg = box->at(i);

            if (msg->tag == tag && msg->source == source ) {
                array = msg->array;
                d->msgbox[target].removeAt(i);
                d->freelist[target].append(msg);
                mutex->unlock();
                return;
            }
        }

        mutex->unlock();
    }
}

dtkDistributedRequest *dtkDistributedCommunicatorQThread::ireceive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag)
{
    return new dtkDistributedRequestQThread(data, size, dataType, source, tag);
}

void dtkDistributedCommunicatorQThread::gather(void *send, void *recv, std::size_t size, QMetaType::Type dataType, qint32 root, bool all)
{
    qint32 wid  = this->wid();
    qint32 world_size = this->size();

    std::size_t bytesize = size * QMetaType(dataType).sizeOf();
    barrier();

    if (wid == root) {
        char *offset_ptr = static_cast<char *>(recv) ;

        for (int i = 0; i < d->size; ++i ) {
            if (i != root) {
                offset_ptr += bytesize;
                receive(static_cast<void *>(offset_ptr), size, dataType, i, TagGather);
            } else {
                memcpy(recv, send, bytesize);
            }
        }

        if (all) {
            broadcast(recv, size * world_size, dataType, root);
        }

    } else {
        this->send(send, size, dataType, root, TagGather);

        if (all) {
            broadcast(recv, size * world_size, dataType, root);
        }
    }
}

// void dtkDistributedCommunicatorQThread::reduce(void *send, void *recv, std::size_t size, QMetaType::Type dataType, OperationType operationType, qint32 root, bool all)
// {
//     qint32 wid = this->wid();

//     std::size_t bytesize = size * QMetaType(dataType).sizeOf();
//     barrier();

//     if (wid == root) {
//         void *buffer = malloc(bytesize);
//         memcpy(recv, send, bytesize);

//         for (int i = 0; i < d->size; ++i ) {
//             if (i != root) {
//                 receive(buffer, size, dataType, i, TagReduce);

//                 switch (dataType) {
//                 case QMetaType::LongLong:
//                 case QMetaType::Long:
//                     dtkDistributed::applyOperation(static_cast<qlonglong *>(recv), static_cast<qlonglong *>(buffer), size, operationType);
//                     break;

//                 case QMetaType::Int:
//                     dtkDistributed::applyOperation(static_cast<int *>(recv), static_cast<int *>(buffer), size, operationType);
//                     break;

//                 case QMetaType::Double:
//                     dtkDistributed::applyOperation(static_cast<double *>(recv), static_cast<double *>(buffer), size, operationType);
//                     break;

//                 case QMetaType::Float:
//                     dtkDistributed::applyOperation(static_cast<float *>(recv), static_cast<float *>(buffer), size, operationType);
//                     break;

//                 default:
//                     qWarning() << Q_FUNC_INFO << "type" << dataType << "is not handle in reduce";
//                     break;
//                 }
//             }
//         }

//         if (all) {
//             broadcast(recv, size, dataType, root);
//         }
//         free(buffer);

//     } else {
//         this->send(send, size, dataType, root, TagReduce);

//         if (all) {
//             broadcast(recv, size, dataType, root);
//         }
//     }
// }

void dtkDistributedCommunicatorQThread::reduce(void *send, void *recv, std::size_t size, const QMetaType& metaType, OperationType operationType, qint32 root, bool all)
{
    qint32 wid = this->wid();

    std::size_t bytesize = size * metaType.sizeOf();
    barrier();

    if (wid == root) {
        void *buffer = malloc(bytesize);
        memcpy(recv, send, bytesize);

        for (int i = 0; i < d->size; ++i ) {
            if (i != root) {
                receive(buffer, size, QMetaType::Type(metaType.id()), i, TagReduce);

                switch (metaType.id()) {
                case QMetaType::LongLong:
                case QMetaType::Long:
                    dtkDistributed::applyOperation(static_cast<qlonglong *>(recv), static_cast<qlonglong *>(buffer), size, operationType);
                    break;

                case QMetaType::Int:
                    dtkDistributed::applyOperation(static_cast<int *>(recv), static_cast<int *>(buffer), size, operationType);
                    break;

                case QMetaType::Double:
                    dtkDistributed::applyOperation(static_cast<double *>(recv), static_cast<double *>(buffer), size, operationType);
                    break;

                case QMetaType::Float:
                    dtkDistributed::applyOperation(static_cast<float *>(recv), static_cast<float *>(buffer), size, operationType);
                    break;

                default:
                    if (metaType == QMetaType::fromType<dtkDistributedDoubleInt>()) {
                        dtkDistributed::applyOperation(static_cast<dtkDistributedDoubleInt *>(recv), static_cast<dtkDistributedDoubleInt *>(buffer), size, operationType);

                    } else {
                        qWarning() << Q_FUNC_INFO << "type" << metaType.id() << "is not handle in reduce";
                    }
                    break;
                }
            }
        }

        if (all) {
            broadcast(recv, size, QMetaType::Type(metaType.id()), root);
        }
        free(buffer);

    } else {
        this->send(send, size, QMetaType::Type(metaType.id()), root, TagReduce);

        if (all) {
            broadcast(recv, size, QMetaType::Type(metaType.id()), root);
        }
    }
}

void dtkDistributedCommunicatorQThread::wait(dtkDistributedRequest *dtkreq)
{
    auto *req = dynamic_cast<dtkDistributedRequestQThread *>(dtkreq);
    if (req) {
        receive(req->m_data, req->m_size, req->m_dataType, req->m_source, req->m_tag);
    }
}

//
// dtkDistributedCommunicatorQThread.cpp ends here
