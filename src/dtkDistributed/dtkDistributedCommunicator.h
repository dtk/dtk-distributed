// dtkDistributedCommunicator.h
//

#pragma once

#include <dtkDistributedExport>

#include <dtkCore>

class dtkDistributedCommunicatorStatus;
class dtkDistributedBufferManager;
class dtkDistributedRequest;

class DTKDISTRIBUTED_EXPORT dtkDistributedCommunicator : public QObject
{
public:
             dtkDistributedCommunicator(void) = default;
    virtual ~dtkDistributedCommunicator(void) = default;

    // use the same value as defined in mpi.h from openmpi
    static const qint32 ANY_TAG    = -1;
    static const qint32 BCAST_TAG  = -7;
    static const qint32 ANY_SOURCE = -1;
    static const qint16 ROOT       = -4;
    static const qint16 PROC_NULL  = -2;

    enum OperationType {
        None,
        Min,
        Max,
        MinLoc,
        MaxLoc,
        Sum,
        Product,
        BitwiseAnd,
        BitwiseOr,
        BitwiseXor,
        LogicalAnd,
        LogicalOr,
        LogicalXor
    };

    enum MessageTag {
        TagSend    = 2001,
        TagReceive = 2002,
        TagReduce  = 2003,
        TagGather  = 2004
    };

    virtual void initialize(void) = 0;
    virtual bool initialized(void) = 0;
    virtual void uninitialize(void) = 0;
    virtual bool active(void) = 0;

    virtual void spawn(QStringList hostnames, QString wrapper = "", QMap<QString, QString> options = QMap<QString, QString>()) = 0;
    virtual void  exec(QRunnable *work) = 0;

    virtual void barrier(void) = 0;
    virtual void unspawn(void) = 0;

    virtual dtkDistributedBufferManager *createBufferManager(void) = 0;
    virtual void destroyBufferManager(dtkDistributedBufferManager *&) = 0;

    virtual void send(void *data, std::size_t size, QMetaType::Type dataType, qint32 target, qint32 tag) = 0;
    template <typename T> void send(T* data, std::size_t size, qint32 target, qint32 tag);

    virtual void send(QByteArray& array, qint32 target, qint32 tag) = 0;
    template <typename T> void send(T data, qint32 target, qint32 tag);
    virtual void send(const QVariant& v, qint32 target, qint32 tag);

    virtual void broadcast(void *data, std::size_t size, QMetaType::Type dataType, qint32 source) = 0;
    template <typename T> void broadcast(T *data, std::size_t size, qint32 source);

    virtual void broadcast(QByteArray& array, qint32 source) = 0;
    virtual void broadcast(QVariant& v, qint32 source) = 0;

    virtual void receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag) = 0;
    virtual void receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status) = 0;
    template <typename T> void receive(T* data, std::size_t size, qint32 source, qint32 tag);
    template <typename T> void receive(T* data, std::size_t size, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status);

    virtual void receive(QByteArray& v, qint32 source, qint32 tag) = 0 ;
    template <typename T> void receive(T& data, qint32 source, qint32 tag);
    virtual void receive(QVariant& v,  qint32 source, qint32 tag);

    virtual void receive(QByteArray& v, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status) = 0;
    template <typename T> void receive(T& data, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status);
    virtual void receive(QVariant& v,  qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status);

    virtual dtkDistributedRequest *ireceive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, int tag) = 0;
    template <typename T> dtkDistributedRequest *ireceive(T *data, std::size_t size, qint32 source, int tag);

    virtual void wait(dtkDistributedRequest *req) = 0;

    //virtual void reduce(void *send, void *recv, std::size_t size, QMetaType::Type dataType, OperationType operationType, qint32 target, bool all = false) = 0;
    virtual void reduce(void *send, void *recv, std::size_t size, const QMetaType& metaType, OperationType operationType, qint32 target, bool all = false) = 0;
    template <typename T> void reduce(T *send, T *recv, std::size_t size, OperationType operationType, qint32 target, bool all = false);

    virtual void gather(void *send, void *recv, std::size_t size, QMetaType::Type dataType, qint32 target, bool all = false) = 0;
    template <typename T> void gather(T *send, T *recv, std::size_t size, qint32 target, bool all = false);

    virtual qint32  wid(void) = 0;
            qint32 rank(void) { return wid(); };
    virtual qint32 size(void) = 0;
    virtual void  *data(void) { return nullptr; };

    virtual void setWid(qint32 id);

    //template <typename T, typename U> void run(T *t, U (T::*functionPointer)());
};

DTK_DECLARE_OBJECT(dtkDistributedCommunicator *)
DTK_DECLARE_PLUGIN(dtkDistributedCommunicator, DTKDISTRIBUTED_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkDistributedCommunicator, DTKDISTRIBUTED_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkDistributedCommunicator, DTKDISTRIBUTED_EXPORT)

DTK_DECLARE_OBJECT(dtkDistributedCommunicator **);

// ///////////////////////////////////////////////////////////////////

template <typename T>
inline void dtkDistributedCommunicator::send(T *data, std::size_t size, qint32 target, qint32 tag)
{
    return this->send(data, size, (QMetaType::Type)qMetaTypeId<T>(), target, tag);
}

template <typename T>
inline void dtkDistributedCommunicator::send(T data, qint32 target, qint32 tag)
{
    QVariant v;
    v = dtk::variantFromValue<T>(data);
    send(v, target, tag);
}

template <typename T>
inline void dtkDistributedCommunicator::broadcast(T *data, std::size_t size, qint32 source)
{
    this->broadcast(data, size, (QMetaType::Type)qMetaTypeId<T>(), source);
}

template <typename T>
inline void dtkDistributedCommunicator::receive(T* data, std::size_t size, qint32 source, qint32 tag)
{
    return this->receive(data, size, (QMetaType::Type)qMetaTypeId<T>(), source, tag);
}

template <typename T>
inline void dtkDistributedCommunicator::receive(T* data, std::size_t size, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status)
{
    return this->receive(data, size, (QMetaType::Type)qMetaTypeId<T>(), source, tag, status);
}

template <typename T>
inline void dtkDistributedCommunicator::receive(T& data, qint32 source, qint32 tag)
{
    QVariant v;
    receive(v, source, tag);
    data = v.value<T>();
}

template <typename T>
inline void dtkDistributedCommunicator::receive(T& data, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status)
{
    QVariant v;
    receive(v, source, tag, status);
    data = v.value<T>();
}

template <typename T>
inline dtkDistributedRequest *dtkDistributedCommunicator::ireceive(T *data, std::size_t size, qint32 source, int tag)
{
    return this->ireceive(data, size, (QMetaType::Type)qMetaTypeId<T>(), source, tag);
}

template <typename T>
inline void dtkDistributedCommunicator::reduce(T *send, T *recv, std::size_t size, OperationType operationType, qint32 target, bool all)
{
    //this->reduce(send, recv, size, (QMetaType::Type)qMetaTypeId<T>(), operationType, target, all);
    this->reduce(send, recv, size, QMetaType::fromType<T>(), operationType, target, all);
}

template <typename T>
inline void dtkDistributedCommunicator::gather(T *send, T *recv, std::size_t size, qint32 target, bool all)
{
    return this->gather(send, recv, size, (QMetaType::Type)qMetaTypeId<T>(), target, all);
}

// // ///////////////////////////////////////////////////////////////////

// template <typename T, typename U>
// class runFunction : public QRunnable
// {
// public:
//     U (T::*func)();
//     T *object;

// public:
//     void run(void) {
//         (object->*func)();
//     }
// };

// // ///////////////////////////////////////////////////////////////////

// template <typename T, typename U>
// inline void dtkDistributedCommunicator::run(T *t, U (T::*functionPointer)())
// {
//     runFunction<T, U> runner;
//     runner.func = functionPointer;
//     runner.object = t;
//     this->exec(&runner);
// }

//
// dtkDistributedCommunicator.h ends here
