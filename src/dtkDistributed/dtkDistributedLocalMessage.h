// dtkDistributedLocalMessage.h
//

#pragma once

#include <QtCore>

class dtkDistributedLocalMessage
{
public:
             dtkDistributedLocalMessage(void);
             dtkDistributedLocalMessage(QVariant& v, qint32 source, qint32 tag);
    virtual ~dtkDistributedLocalMessage(void);

    void lock(void);
    void unlock(void);
    void wait(void);
    void wake(void);

    qint32 tag(void) const;
    qint32 source(void) const;
    bool wait_for_data(void)const;
    QVariant data(void) const;

    void setWaitData(bool wait_value);
    void setData(QVariant data);
    void setTag(qint32 tag);
    void setSource(qint32 source);

private:
    class dtkDistributedLocalMessagePrivate *d = nullptr;
};

//
// dtkDistributedLocalMessage.h ends here
