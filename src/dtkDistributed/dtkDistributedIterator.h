// dtkDistributedIterator.h
//

#pragma once

#include <QtCore>

// ///////////////////////////////////////////////////////////////////
// dtkDistributedIterator
// ///////////////////////////////////////////////////////////////////

template <typename Container> class dtkDistributedIterator
{
    const Container *c = nullptr;
    std::size_t gid = 0;

public:
    using iterator_category = std::random_access_iterator_tag;
    using value_type = typename Container::value_type;
    using difference_type = typename Container::difference_type;
    using pointer = value_type*;
    using reference = value_type&;

             dtkDistributedIterator(void) = delete;
    explicit dtkDistributedIterator(const Container& container, std::size_t index);
             dtkDistributedIterator(const dtkDistributedIterator& other);
            ~dtkDistributedIterator(void) = default;

    dtkDistributedIterator& operator = (const dtkDistributedIterator& other);

    std::size_t id(void) const;

    value_type operator *  (void) const;
    value_type operator [] (std::size_t j) const;

    bool operator == (const dtkDistributedIterator& o) const;
    bool operator != (const dtkDistributedIterator& o) const;
    bool operator <  (const dtkDistributedIterator& o) const;
    bool operator <= (const dtkDistributedIterator& o) const;
    bool operator >  (const dtkDistributedIterator& o) const;
    bool operator >= (const dtkDistributedIterator& o) const;

    dtkDistributedIterator& operator ++ (void);
    dtkDistributedIterator& operator -- (void);

    dtkDistributedIterator operator ++ (int);
    dtkDistributedIterator operator -- (int);

    dtkDistributedIterator& operator += (std::size_t j);
    dtkDistributedIterator& operator -= (std::size_t j);

    dtkDistributedIterator operator +  (std::size_t j) const;
    dtkDistributedIterator operator -  (std::size_t j) const;

    std::size_t operator - (const dtkDistributedIterator& o) const;
};

// ///////////////////////////////////////////////////////////////////

template <typename Container>
inline dtkDistributedIterator<Container>::dtkDistributedIterator(const Container& container, std::size_t index) : c(&container), gid(index)
{
}

template <typename Container>
inline dtkDistributedIterator<Container>::dtkDistributedIterator(const dtkDistributedIterator<Container>& o) : c(o.c), gid(o.gid)
{
}

template <typename Container>
inline dtkDistributedIterator<Container>& dtkDistributedIterator<Container>::operator = (const dtkDistributedIterator<Container>& o)
{
    c = o.c;
    gid = o.gid;

    return *this;
}

template <typename Container>
inline std::size_t dtkDistributedIterator<Container>::id(void) const
{
    return gid;
}

template <typename Container>
inline auto dtkDistributedIterator<Container>::operator * (void) const -> value_type
{
    return (*c)[gid];
}

template <typename Container>
inline auto dtkDistributedIterator<Container>::operator [] (std::size_t j) const -> value_type
{
    return (*c)[gid + j];
}

template <typename Container>
inline bool dtkDistributedIterator<Container>::operator == (const dtkDistributedIterator& o) const
{
    return gid == o.gid;
}

template <typename Container>
inline bool dtkDistributedIterator<Container>::operator != (const dtkDistributedIterator& o) const
{
    return gid != o.gid;
}

template <typename Container>
inline bool dtkDistributedIterator<Container>::operator < (const dtkDistributedIterator& o) const
{
    return gid < o.gid;
}

template <typename Container>
inline bool dtkDistributedIterator<Container>::operator <= (const dtkDistributedIterator& o) const
{
    return gid <= o.gid;
}

template <typename Container>
inline bool dtkDistributedIterator<Container>::operator > (const dtkDistributedIterator& o) const
{
    return gid > o.gid;
}

template <typename Container>
inline bool dtkDistributedIterator<Container>::operator >= (const dtkDistributedIterator& o) const
{
    return gid >= o.gid;
}

template <typename Container>
inline dtkDistributedIterator<Container>& dtkDistributedIterator<Container>::operator ++ (void)
{
    ++gid; return *this;
}

template <typename Container>
inline dtkDistributedIterator<Container>& dtkDistributedIterator<Container>::operator -- (void)
{
    if (gid != 0) {
        --gid;
    }
    return *this;
}

template <typename Container>
inline dtkDistributedIterator<Container> dtkDistributedIterator<Container>::operator ++ (int)
{
    ++gid; return dtkDistributedIterator(c, gid - 1);
}

template <typename Container>
inline dtkDistributedIterator<Container> dtkDistributedIterator<Container>::operator -- (int)
{
    if (gid != 0) {
        --gid;
    }
    return dtkDistributedIterator(c, gid + 1);
}

template <typename Container>
inline dtkDistributedIterator<Container>& dtkDistributedIterator<Container>::operator += (std::size_t j)
{
    gid += j; return *this;
}

template <typename Container>
inline dtkDistributedIterator<Container>& dtkDistributedIterator<Container>::operator -= (std::size_t j)
{
    if (gid > j) {
        gid -= j;
    } else {
        gid = 0;
    }
    return *this;
}

template <typename Container>
inline dtkDistributedIterator<Container> dtkDistributedIterator<Container>::operator + (std::size_t j) const
{
    return dtkDistributedIterator(*c, gid + j);
}

template <typename Container>
inline dtkDistributedIterator<Container> dtkDistributedIterator<Container>::operator - (std::size_t j) const
{
    return dtkDistributedIterator(*c, gid > j ? gid - j : 0);
}

template <typename Container>
inline std::size_t dtkDistributedIterator<Container>::operator - (const dtkDistributedIterator& o) const
{
    return gid < o.gid ? 0 : gid - o.gid;
}


//
// dtkDistributedIterator.h ends here
