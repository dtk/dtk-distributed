// dtkDistributed.cpp
//

#include "dtkDistributed.h"

#include "dtkDistributedApplication.h"
#include "dtkDistributedCommunicator.h"
#include "dtkDistributedCommunicatorQThread.h"
#include "dtkDistributedController.h"
#include "dtkDistributedDoubleInt.h"
#include "dtkDistributedPolicy.h"
#include "dtkDistributedSettings.h"

namespace dtkDistributed {

    namespace detail {
        dtkDistributed::Mode mode = dtkDistributed::Global;
        dtkDistributedApplication *app = nullptr;
    }

    void setMode(dtkDistributed::Mode mode)
    {
        detail::mode = mode;
    }

    dtkDistributed::Mode mode(void)
    {
        return detail::mode;
    }

    dtkDistributedApplication *create(int& argc, char *argv[])
    {
        for (int i = 0; i < argc; i++)
            if (!qstrcmp(argv[i], "-nw") || !qstrcmp(argv[i], "--nw") ||  !qstrcmp(argv[i], "-no-window") || !qstrcmp(argv[i], "--no-window") || !qstrcmp(argv[i], "-h") || !qstrcmp(argv[i], "--help") || !qstrcmp(argv[i], "--version")) {
                qputenv("QT_QPA_PLATFORM", QByteArrayLiteral("minimal"));
            }

        detail::app = new dtkDistributedApplication(argc, argv);
        return detail::app;
    }

    dtkDistributedApplication *app(void)
    {
        if (!detail::app ) {
            detail::app = dynamic_cast<dtkDistributedApplication *>(qApp);
        }

        return detail::app;
    }

    dtkDistributedPolicy *policy(void)
    {
        if (!app())
            return nullptr;

        return detail::app->policy();
    }

    void spawn(void)
    {
        if (!app())
            return;

        detail::app->spawn();
    }

    void exec(QRunnable *task)
    {
        if (!app())
            return;

        detail::app->exec(task);
    }

    void unspawn(void)
    {
        if (!app())
            return;

        detail::app->unspawn();
    }

    namespace communicator {

        namespace detail {
            dtkDistributedCommunicatorPluginFactory factory;
            dtkDistributedCommunicatorPluginManager manager;
            dtkDistributedCommunicator *communicator = nullptr;
        }

        dtkDistributedCommunicatorPluginFactory& pluginFactory(void)
        {
            return detail::factory;
        }

        dtkDistributedCommunicatorPluginManager& pluginManager(void)
        {
            return detail::manager;
        }

        void setVerboseLoading(bool verbose)
        {
            detail::manager.setVerboseLoading(verbose);
        }

        void initialize(const QString& path)
        {
            QString real_path = path;
            pluginFactory().record("qthread", dtkDistributedCommunicatorQThreadCreator);

            if (path.isEmpty()) {
                dtkDistributedSettings settings;
                settings.beginGroup("communicator");
                real_path = settings.value("plugins").toString();
                settings.endGroup();

                if (real_path.isEmpty() && qEnvironmentVariableIsSet("CONDA_PREFIX")) {
                    QString conda_prefix = qgetenv("CONDA_PREFIX");
                    real_path = conda_prefix + "/plugins/dtkDistributed";
                    dtkDebug() << "no plugin path configured, use default:" << real_path ;
                }
                pluginManager().initialize(real_path);

            } else {
                pluginManager().initialize(path);
            }

            qRegisterMetaType<dtkDistributedDoubleInt>();
        }
        // used to dtkDistributedGuiApplication: we can't link here to dtkWidgets so we need a setter
        void setInstance(dtkDistributedCommunicator *comm)
        {
            detail::communicator = comm;
        }

        dtkDistributedCommunicator *instance(void)
        {
            dtkDistributedApplication *app = dtkDistributed::app();

            if (detail::communicator) {
                return detail::communicator;
            } else if (app) {
                detail::communicator = app->communicator();
            } else {
                dtkInfo() << "no communicator, no application: create a default qthread communicator";
                dtkDistributedSettings settings;
                dtkDistributed::communicator::initialize(settings.value("plugins").toString());
                detail::communicator = dtkDistributed::communicator::pluginFactory().create("qthread");
            }

            return detail::communicator;
        }
    }
    namespace controller {

        namespace detail {
            dtkDistributedController *controller = nullptr;
            QThread *controllerThread = nullptr;
            QMutex mutex;
        }

        dtkDistributedController *instance(void)
        {
            QMutexLocker locker(&detail::mutex);
            if (detail::controller) {
                return detail::controller;
            } else {
                dtkInfo() << "no controller: create a default controller in its own thread";
                detail::controller = dtkDistributedController::instance();
                detail::controllerThread = new QThread();
                detail::controller->moveToThread(detail::controllerThread);

                QObject::connect(detail::controllerThread, &QThread::finished, detail::controllerThread, &QThread::deleteLater);
                detail::controllerThread->start();
                return detail::controller;
            }
        }

        bool connectSrv(const QUrl& server, bool ssh_rank, bool emit_connected)
        {
            bool res;
            QMetaObject::invokeMethod(detail::controller, "connectSrv", Qt::BlockingQueuedConnection, Q_RETURN_ARG(bool, res),
                                      Q_ARG(QUrl, server), Q_ARG(bool, ssh_rank), Q_ARG(bool, emit_connected) );
            return res;
        }

        bool deploy(const QUrl& server, QString type, bool ssh_tunnel, QString path, QString loglevel)
        {
            bool res;
            QMetaObject::invokeMethod(detail::controller, "deploy", Qt::BlockingQueuedConnection, Q_RETURN_ARG(bool, res),
                                      Q_ARG(QUrl, server), Q_ARG(QString, type), Q_ARG(bool, ssh_tunnel), Q_ARG(QString, path), Q_ARG(QString, loglevel) );
            return res;
        }

        bool submit(const QUrl& server, const QByteArray& resources, const QString& submit_id)
        {
            bool res;
            QMetaObject::invokeMethod(detail::controller, "submit", Qt::BlockingQueuedConnection, Q_RETURN_ARG(bool, res),
                                      Q_ARG(QUrl, server), Q_ARG(QByteArray, resources), Q_ARG(QString, submit_id) );
            return res;
        }
    }

}
