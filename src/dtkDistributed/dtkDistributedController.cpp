// dtkDistributedController.cpp
//

#include "dtkDistributedController.h"

#include "dtkDistributedMessage.h"
#include "dtkDistributedResourceManager.h"

#include <dtkLog/dtkLog.h>

#include <QtNetwork>

#if !defined(Q_OS_MAC) && !defined(Q_OS_WIN)
#include <unistd.h>
#endif

#if defined(Q_OS_WIN) && !defined(__MINGW32__)
#include <windows.h>
#endif

// /////////////////////////////////////////////////////////////////
// Helper function
// /////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////
// dtkDistributedControllerPrivate declaration
// /////////////////////////////////////////////////////////////////

class dtkDistributedControllerPrivate
{
public:
    QHash<QString, QTcpSocket *> sockets;

    QHash<QString, QString> running_jobs; // all jobs started by the controller and running
    QHash<QString, QString> queued_jobs; // all jobs submitted by the controller (running or not)

    QHash<QString, QVector<QProcess *>> servers;
    QHash<QString, bool> tunnel_option;

public:
    QProcess *run_process(const QUrl& server, const QString& command, QStringList args, bool clean = false, QString wrap_cmd = "ssh", bool ssh_tunnel = false, bool wait_output = true);

public:
    bool refreshing;
    dtkDistributedController *q = nullptr;
    QMutex mutex;
};

QProcess *dtkDistributedControllerPrivate::run_process(const QUrl& server,  const QString& command, QStringList args, bool clean, QString wrap_cmd, bool ssh_tunnel, bool wait_output)
{
    QProcess *process = new QProcess (this->q);

    QObject::connect(process, &QProcess::finished, this->q, [=] (int exitCode, QProcess::ExitStatus exitStatus)
    {
        if (exitStatus != QProcess::NormalExit) {
            dtkInfo() <<  "remote process failure" << command << exitStatus ;
        } else {
            dtkInfo() <<  "remote process stopped";
        }
        if (clean)
            delete process;
    });
    QObject::connect(process, &QProcess::errorOccurred, this->q, [=] (QProcess::ProcessError error) -> void
    {
        dtkWarn() <<  "remote server conda update failure" << error ;
    });
    QObject::connect(process, &QProcess::readyReadStandardOutput, this->q, [=] (void) -> void
    {
        dtkTrace() <<  process->readAll();
    });
    QObject::connect(process, &QProcess::readyReadStandardError, this->q, [=] (void) -> void
    {
        dtkWarn() <<  process->readAll();
    });

    QStringList real_args;
    if (server.host() != "localhost" && wrap_cmd == "ssh") {
        if (ssh_tunnel) {
            dtkTrace() << "ssh port forwarding is set for server " << server.host();
            int port = (server.port() == -1) ? dtkDistributedController::defaultPort() : server.port();
            real_args << "-L" << QString::number(port) + ":localhost:" + QString::number(port);
        }

        real_args << "-t"; // that way, ssh will forward the SIGINT signal,
        // and the server will stop when the ssh process
        // is killed
        real_args << "-t"; // do it twice to force tty allocation
        real_args << "-x"; // disable X11 forwarding
        real_args << server.host();
    }


    process->setProcessChannelMode(QProcess::MergedChannels);
    if (server.host() != "localhost") {
        if (wrap_cmd.isEmpty()) {
            wrap_cmd = command;
        } else {
            real_args << command;
        }
        real_args << args;
        process->start(wrap_cmd, real_args);
        dtkDebug() << wrap_cmd << real_args;
    } else {
        real_args << args;
        process->start(command, real_args);
        dtkDebug() << command << real_args;
    }

    // need to wait a bit for the server to open it's listening socket if we call connectSrv immediately after deploy.
    QThread::sleep(1);

    if (!process->waitForStarted(5000)) {
        dtkError() << "remote process server not started after 5 seconds, abort  " << command << real_args;
        process->close();
        delete process;
        return nullptr;
    }

    if (wait_output && !process->waitForReadyRead(5000)) {
        dtkError() << "no output from remote process after 5 seconds, abort  " << command << real_args;
        process->close();
        delete process;
        return nullptr;
    } else {
        dtkTrace() << "read data" << process->readAll();
    }

    return process;
}

// /////////////////////////////////////////////////////////////////
// dtkDistributedController implementation
// /////////////////////////////////////////////////////////////////

/*!
  \class dtkDistributedController
  \inmodule dtkDistributed

  \brief The controller is used to interact with a remote
  dtkDistributedServer and get/view resources from this server using
  a dtkDistributedResourceManager. Differents managers are
  implemented: OAR, Torque, and Local resources on the server.

  \sa dtkDistributedServer, dtkDistributedMessage, dtkDistributedResourceManager

 */

dtkDistributedController::dtkDistributedController(QObject *parent) : QObject(parent), d(new dtkDistributedControllerPrivate)
{
    d->q = this;
    d->refreshing = false;
}

dtkDistributedController::~dtkDistributedController(void)
{
    delete d;
    d = nullptr;
}

dtkDistributedController *dtkDistributedController::instance(void)
{
    if (!s_instance) {
        s_instance = new dtkDistributedController;
    }

    return s_instance;
}

//! define a default port that should be uniq among users: compute a CRC-16 hash using the username
quint16 dtkDistributedController::defaultPort(void)
{
    QProcessEnvironment pe = QProcessEnvironment::systemEnvironment();
    QString username = pe.value("USERNAME");

    if (username.isEmpty()) {
        username = pe.value("USER");
    }

    if (username.isEmpty()) {
        return 9999;
    }

    QByteArrayView bin(username.toUtf8());
    quint16 p =  qChecksum(bin);

    if (p < 1024) {// listen port should be higher than 1024
        p += 1024;
    }

    dtkInfo() << "default port is" << p;
    return p;
}

bool dtkDistributedController::isConnected(const QUrl& server)
{
    if (d->sockets.contains(server.toString())) {
        QTcpSocket *socket = d->sockets.value(server.toString());
        return (socket->state() == QAbstractSocket::ConnectedState);
    }
    return false;
}

bool dtkDistributedController::isDisconnected(const QUrl& server)
{
    if (d->sockets.contains(server.toString())) {
        QTcpSocket *socket = d->sockets.value(server.toString());
        return (socket->state() == QAbstractSocket::UnconnectedState);
    }
    return true;
}

bool dtkDistributedController::submit(const QUrl& server, const QByteArray& resources, const QString& submit_id)
{
    dtkDebug() << "Want to submit jobs with resources:" << resources ;
    QScopedPointer<dtkDistributedMessage> msg (new dtkDistributedMessage(dtkDistributedMessage::NEWJOB, "", submit_id.toInt(), resources.size(), "json", resources));

    if (!d->servers.contains(server.toString())) {
        dtkError() << "Can't submit job: unknown server " << server.toString();
        return false;
    }

    if (!d->sockets.contains(server.toString())) {
        dtkDebug() << "Needs to reconnect to server" << server.toString();
        this->connectSrv(server);
    }

    if (!isConnected(server)) {
        dtkWarn() << "Can't submit job:  server " << server.toString() << "is not connected";
        d->servers.remove(server.toString());
        d->tunnel_option.remove(server.toString());
        return false;
    }

    msg->send(d->sockets[server.toString()]);
    return true;
}

void dtkDistributedController::killjob(const QUrl& server, QString jobid)
{
    dtkDebug() << "Want to kill job" << jobid;

    if (!d->servers.contains(server.toString())) {
        dtkError() << "Can't kill job: unknown server " << server.toString();
        return;
    }

    if (!d->sockets.contains(server.toString())) {
        dtkDebug() << "Needs to reconnect to server" << server.toString();
        this->connectSrv(server);
    }

    if (!isConnected(server)) {
        dtkWarn() << "Can't kill job: server " << server.toString() << "is not connected";
        d->servers.remove(server.toString());
        d->tunnel_option.remove(server.toString());
        return;
    }

    dtkDistributedMessage *msg  = new dtkDistributedMessage(dtkDistributedMessage::DELJOB, jobid, dtkDistributedMessage::SERVER_RANK);
    msg->send(d->sockets[server.toString()]);
    delete msg;
}

void dtkDistributedController::stop(const QUrl& server)
{
    dtkDebug() << "Want to stop server on " << server.toString();

    if (!d->sockets.contains(server.toString())) {
        dtkDebug() << "Needs to reconnect to server" << server.toString();
        this->connectSrv(server);
    }

    if (!isConnected(server)) {
        dtkWarn() << "Can't stop server " << server.toString() << "not connected";
        d->servers.remove(server.toString());
        d->tunnel_option.remove(server.toString());
        return;
    }

    dtkDebug() << "Send stop message to server" << server.toString();
    dtkDistributedMessage msg(dtkDistributedMessage::STOP, "", dtkDistributedMessage::SERVER_RANK);
    msg.send(d->sockets[server.toString()]);
    this->disconnectSrv(server);
    d->servers.remove(server.toString());
    d->tunnel_option.remove(server.toString());
}

void dtkDistributedController::refresh(const QUrl& server)
{
    dtkDebug() << server;

    if (!d->sockets.contains(server.toString()))
        return;

    d->refreshing = true;

    QTcpSocket *socket = d->sockets.value(server.toString());
    dtkDistributedMessage *msg  = new dtkDistributedMessage(dtkDistributedMessage::STATUS);
    msg->send(socket);
    emit updated(server);
    delete msg;
}


// Install conda on given server
bool dtkDistributedController::bootstrapConda(const QUrl& server, const QString& miniconda_url)
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QEventLoop loop;
    QNetworkReply *reply = manager->get(QNetworkRequest(QUrl(miniconda_url)));
    loop.connect(manager, &QNetworkAccessManager::finished, &loop, &QEventLoop::quit );
    loop.exec();

    QTemporaryFile file;
    if (reply->error() == QNetworkReply::NoError) {
        if (file.open()) {
            dtkError() << "downloaded" << miniconda_url << file.fileName();
            file.setAutoRemove(false);
            file.write(reply->readAll());
            file.close();
        } else {
            dtkError() << "can't write file.";
            reply->deleteLater();
            return false;
        }
    } else {
        dtkError() << "Error while download miniconda." << miniconda_url;
        reply->deleteLater();
        return false;
    }

    reply->deleteLater();
    manager->deleteLater();

    // First, copy conda installation script to remote server:
    QStringList args;
    QString conda_archive = "conda_download_by_dtk_distributed.sh";
    args << file.fileName() << server.host() + ":" + conda_archive;
    auto process = d->run_process(server, "scp", args, true, "", false, false);
    if (!process)
        return false;
    process->waitForFinished();

    // Then, run the script in batch mode
    QString command = "bash";
    args.clear();
    args << conda_archive << "-b";
    process = d->run_process(server, command, args, false, "ssh", false, false);
    if (!process)
        return false;
    process->waitForFinished();

    // Finally, run conda init for all shells, assuming that the default install dir is ued (miniconda3)
    command =" miniconda3/bin/conda";
    args.clear();
    args << "init" << "--all";
    process = d->run_process(server, command, args, false, "ssh", false, false);
    if (!process)
        return false;
    process->waitForFinished();

    return true;
}

// Update conda environment on given server (or create if it does not exist)
bool dtkDistributedController::deployEnv(const QUrl& server, const QString& conda_env_file)
{
    QFile f(conda_env_file);
    QTemporaryFile *tmp = QTemporaryFile::createNativeFile(f);
    QString source_file_name = conda_env_file;
    if (tmp) { //resource content, extract it to the filesystem
        source_file_name = tmp->fileName();
        dtkDebug() << "will copy from " << conda_env_file << "to" << source_file_name;
    }

    dtkDebug() << "deploy conda env" << server << source_file_name <<  QThread::currentThreadId();
    QStringList args;

    QString command = "scp";
    QFileInfo file_info(source_file_name);
    QString original_file_name = QFileInfo(conda_env_file).fileName();
    args << source_file_name << server.host() + ":" + original_file_name;
    auto process = d->run_process(server, command, args, true, "", false, false);
    process->waitForFinished();
    if (tmp) delete tmp;

    command = "conda";
    args.clear();
    args << "env" << "update" << "-q" << "-f" << original_file_name;
    process = d->run_process(server, command, args, false, "ssh", false, false);

    QObject::connect(process, &QProcess::finished, this, [=] (int exitCode, QProcess::ExitStatus exitStatus)
    {
        if (exitStatus == QProcess::NormalExit) {
            dtkInfo() <<  "Conda Env"<< conda_env_file << "successfully updated";
            emit envUpdated(conda_env_file);
        } else {
            dtkError() <<  "Error while updating Conda Env"<< conda_env_file;
        }

        delete process;
    });

    return (  process != nullptr );
}

// deploy a server instance on remote host (to be executed before connect)
bool dtkDistributedController::deploy(const QUrl& server, const QString& type, bool ssh_tunnel, const QString& path, const QString& loglevel, const QString& working_directory)
{
    dtkDebug() << "deploy" << server << type << ssh_tunnel << path <<  QThread::currentThreadId();

    if (!d->servers.contains(server.toString())) {

        // test if we can connect to server, if true, it means the server is deployed (by someone else ?), stop server first, and redeploy
        if (this->connectSrv(server, false, false)) {
            // can connect, server already deployed by someone else
            dtkInfo() << "server" << server << "is already deployed, restart server";
            stop(server);
        }

        int port = (server.port() == -1) ? dtkDistributedController::defaultPort() : server.port();
        QStringList args;

        args << "-nw";

        if (!working_directory.isEmpty()) {
            args << "--working_directory";
            args << working_directory;
        }

        if (loglevel != "info") {
            args << "--loglevel";
            args << loglevel;
        }

        args << "--type";
        args << type;
        args << "-p";
        args << QString::number(port);

        QProcess  *process = d->run_process(server, path, args, false, "ssh", ssh_tunnel);
        if (process == nullptr) {
            return false;
        }
        QObject::connect (qApp, &QCoreApplication::aboutToQuit, this, &dtkDistributedController::cleanup);

        d->servers[server.toString()] << process;
        d->tunnel_option[server.toString()] = ssh_tunnel;
        dtkDebug() << "A server is deployed on" << server.host();
        return true;

    }

    dtkInfo() << "dtkDistributedServer already deployed on " << server.host();
    return true;
}

void dtkDistributedController::send(dtkDistributedMessage *msg)
{
    if (d->queued_jobs.contains(msg->jobid())) {
        QString server = d->queued_jobs[msg->jobid()];
        QTcpSocket *socket = d->sockets[server];

        msg->send(socket);
    } else
        dtkWarn() << "unknown job, can't send message" << msg->jobid();

}

void dtkDistributedController::send(QVariant v, QString jobid, qint16 rank)
{
    if (d->queued_jobs.contains(jobid)) {
        QString server = d->queued_jobs[jobid];
        QTcpSocket *socket = d->sockets[server];

        QString type = "qvariant";

        //FIXME: what about the size ?
        dtkDistributedMessage *msg = new dtkDistributedMessage(dtkDistributedMessage::DATA, jobid, rank, -1, type);
        msg->send(socket);
        QDataStream stream(socket);
        stream << v;
        delete msg;
    } else
        dtkWarn() << "unknown job, can't send message" << jobid;
}

QTcpSocket *dtkDistributedController::socket(const QString& jobid)
{
    if (d->queued_jobs.contains(jobid))
        if (d->sockets.contains(d->queued_jobs[jobid]))
            return d->sockets[d->queued_jobs[jobid]];

    return nullptr;
}

bool dtkDistributedController::connectSrv(const QUrl& server,  bool set_rank, bool emit_connected)
{
    QString serverStr= server.toString();

    if (!d->sockets.contains(serverStr)) {

        QTcpSocket *socket = new QTcpSocket(this);

        QString key;

        key = server.host();


        if (server.port() == -1) {
            const_cast<QUrl&>(server).setPort(dtkDistributedController::defaultPort());
        }

        int port = server.port();

        if (d->tunnel_option.contains(serverStr) && d->tunnel_option[serverStr]) {
            dtkDebug() << "using ssh tunnel, connecting to localhost";
            socket->connectToHost("localhost", port);
        } else {
            socket->connectToHost(server.host(), port);
        }

        if (socket->waitForConnected()) {

            QObject::connect(socket, &QTcpSocket::readyRead, this, &dtkDistributedController::read);
            QObject::connect(socket, &QTcpSocket::errorOccurred, this, &dtkDistributedController::error);

            d->sockets.insert(serverStr, socket);

            if (emit_connected)
                emit connected(server);

            if (set_rank) {
                dtkDebug() << "set rank for controller";
                dtkDistributedMessage msg(dtkDistributedMessage::SETRANK, "", dtkDistributedMessage::CONTROLLER_RANK);
                msg.send(socket);
            } else {
                dtkDebug() << "no need to set rank for controller";
            }

            return true;

        } else {

            dtkWarn() << "Unable to connect to" << serverStr;
            d->sockets.remove(serverStr);
            return false;
        }
    } else {
        dtkInfo() << "Already connected to server" << serverStr;
        return true;
    }
}

void dtkDistributedController::disconnectSrv(const QUrl& server)
{
    if (!d->sockets.contains(server.toString())) {
        dtkDebug() << "disconnect: unknown server" << server;
        return;
    }

    QTcpSocket *socket = d->sockets.value(server.toString());
    socket->disconnectFromHost();

    d->sockets.remove(server.toString());

    emit disconnected(server);
}

bool dtkDistributedController::is_running(const QString& jobid)
{
    return d->running_jobs.contains(jobid);
}

void dtkDistributedController::read(void)
{
    QTcpSocket *socket = (QTcpSocket *)sender();

    QString server = d->sockets.key(socket);
    QScopedPointer< dtkDistributedMessage> msg(new dtkDistributedMessage);
    QVariant result;

    msg->parse(socket);

    dtkDistributedMessage::Method method = msg->method();
    int submit_id;

    switch (method) {
    case dtkDistributedMessage::OKSTATUS:
        emit updated();
        break;

    case dtkDistributedMessage::OKJOB:
        dtkDebug() << "New job queued: " << msg->jobid();
        submit_id = msg->rank(); // use rank to identify the submitter id
        d->queued_jobs[msg->jobid()] = server;
        emit jobQueued(msg->jobid(), QString::number(submit_id));
        break;

    case dtkDistributedMessage::SETRANK:
        dtkDebug() << "set rank received";

        if (msg->rank() ==  dtkDistributedMessage::SLAVE_RANK ||  msg->rank() ==  0 ) {
            dtkDebug() << "job started";
            d->running_jobs[msg->jobid()] = server;
            emit jobStarted(msg->jobid());
        }

        break;

    case dtkDistributedMessage::ENDJOB:
        dtkDebug() << "job finished: " << msg->jobid();
        d->queued_jobs.remove(msg->jobid());
        d->running_jobs.remove(msg->jobid());
        emit jobEnded(msg->jobid());
        break;

    case dtkDistributedMessage::DATA:
        if (msg->header("content-type") == "qvariant") {
            result = msg->variant();
        } else {
            dtkWarn() << "Data received, but not a variant: " << msg->header("content-type");
            result = QVariant::fromValue(msg->content());
        }

        emit dataPosted(result);
        break;

    case dtkDistributedMessage::ERRORJOB:
        dtkDebug() << "job error: " << msg->jobid();
        if (!msg->jobid().isEmpty()) {
            d->queued_jobs.remove(msg->jobid());
            d->running_jobs.remove(msg->jobid());
        }
        emit jobError(msg->jobid());
        break;

    default:
        dtkWarn() << "unknown response from server ";
    };

    if (socket->bytesAvailable() > 0) {
        this->read();
    }
}

void dtkDistributedController::cleanup(void)
{
    for (auto it = d->servers.begin(); it != d->servers.end(); ++it) {
        for (QProcess *server : it.value()) {
            dtkDebug() << "terminating servers started on" << it.key();
            server->terminate();
        }
    }
}

void dtkDistributedController::error(int error)
{
    switch (error) {
    case QAbstractSocket::ConnectionRefusedError:
        dtkError() <<  "The connection was refused by the peer (or timed out).";
        break;

    case QAbstractSocket::RemoteHostClosedError:
        dtkError() <<  "The remote host closed the connection. Note that the slave socket (i.e., this socket) will be closed after the remote close notification has been sent.";
        break;

    case QAbstractSocket::HostNotFoundError:
        dtkError() <<  "The host address was not found.";
        break;

    case QAbstractSocket::SocketAccessError:
        dtkError() <<  "The socket operation failed because the application lacked the required privileges.";
        break;

    case QAbstractSocket::SocketResourceError:
        dtkError() <<  "The local system ran out of resources (e.g., too many sockets).";
        break;

    case QAbstractSocket::SocketTimeoutError:
        dtkError() <<  "The socket operation timed out.";
        break;

    case QAbstractSocket::DatagramTooLargeError:
        dtkError() <<  "The datagram was larger than the operating system's limit (which can be as low as 8192 bytes).";
        break;

    case QAbstractSocket::NetworkError:
        dtkError() <<  "An error occurred with the network (e.g., the network cable was accidentally plugged out).";
        break;

    case QAbstractSocket::AddressInUseError:
        dtkError() <<  "The address specified to QUdpSocket::bind() is already in use and was set to be exclusive.";
        break;

    case QAbstractSocket::SocketAddressNotAvailableError:
        dtkError() <<  "The address specified to QUdpSocket::bind() does not belong to the host.";
        break;

    case QAbstractSocket::UnsupportedSocketOperationError:
        dtkError() <<  "The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support).";
        break;

    case QAbstractSocket::ProxyAuthenticationRequiredError:
        dtkError() <<  "The socket is using a proxy, and the proxy requires authentication.";
        break;

    case QAbstractSocket::SslHandshakeFailedError:
        dtkError() <<  "The SSL/TLS handshake failed, so the connection was closed (only used in QSslSocket)";
        break;

    case QAbstractSocket::UnfinishedSocketOperationError:
        dtkError() <<  "Used by QAbstractSocketEngine only, The last operation attempted has not finished yet (still in progress in the background).";
        break;

    case QAbstractSocket::ProxyConnectionRefusedError:
        dtkError() <<  "Could not contact the proxy server because the connection to that server was denied";
        break;

    case QAbstractSocket::ProxyConnectionClosedError:
        dtkError() <<  "The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)";
        break;

    case QAbstractSocket::ProxyConnectionTimeoutError:
        dtkError() <<  "The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase.";
        break;

    case QAbstractSocket::ProxyNotFoundError:
        dtkError() <<  "The proxy address set with setProxy() (or the application proxy) was not found.";
        break;

    case QAbstractSocket::ProxyProtocolError:
        dtkError() <<  "The connection negotiation with the proxy server because the response from the proxy server could not be understood.";
        break;

    case QAbstractSocket::UnknownSocketError:
        dtkError() <<  "An unidentified error occurred.";
        break;

    default:
        break;
    }
}

dtkDistributedController *dtkDistributedController::s_instance = nullptr;

//
// dtkDistributedController.cpp ends here
