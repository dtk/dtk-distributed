// dtkDistributedArray.tpp
//

#pragma once

#include "dtkDistributed.h"
#include "dtkDistributedCommunicator.h"
#include "dtkDistributedMapper.h"
#include "dtkDistributedBufferManager.h"

#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// dtkDistributedArray private implementation
// ///////////////////////////////////////////////////////////////////

template <typename T>
inline void dtkDistributedArray<T>::allocate(size_type local_size)
{
    if (local_size != 0) {
        m_buffer_manager = m_comm->createBufferManager();
        m_begin = m_buffer_manager->allocate<T>(local_size);
        m_end = m_begin + local_size;
        m_cache = new Cache(this);

    } else {
        dtkWarn() << "allocation with size == 0 !" << m_comm->wid();
    }
}

template <typename T>
inline void dtkDistributedArray<T>::deallocate(void)
{
    if (!m_begin) {
        return;
    }
    if (!std::is_trivially_default_constructible<value_type>::value) {
        for (pointer p = m_begin; p != m_end; ++p) {
            p->~value_type();
        }
    }
    m_buffer_manager->deallocate(m_begin);
    m_begin = nullptr;
    m_end = nullptr;
    m_comm->destroyBufferManager(m_buffer_manager);
    m_buffer_manager = nullptr;
    delete m_cache;
    m_cache = nullptr;
}

template <typename T>
inline void dtkDistributedArray<T>::clear(void)
{
    m_comm->barrier();

    this->deallocate();
}

// ///////////////////////////////////////////////////////////////////
// dtkDistributedArray implementation
// ///////////////////////////////////////////////////////////////////

template <typename T>
inline dtkDistributedArray<T>::dtkDistributedArray(size_type size) : dtkDistributedArray(size, value_type())
{
    this->allocate(m_mapper->count(this->wid()));

    std::uninitialized_fill(m_begin, m_end, value_type(0));

    m_first_id = m_mapper->firstIndex(this->wid());
    m_comm->barrier();
}

template <typename T>
inline dtkDistributedArray<T>::dtkDistributedArray(size_type size, dtkDistributedMapper *mapper) : dtkDistributedContainer(size, mapper)
{
    if (m_mapper->count() == 0) {
        m_mapper->setMapping(size, m_comm->size());
    } else {
        Q_ASSERT(mapper->check());
    }

    this->allocate(m_mapper->count(this->wid()));

    std::uninitialized_fill(m_begin, m_end, value_type(0));

    m_first_id = m_mapper->firstIndex(this->wid());
    m_comm->barrier();
}

template <typename T>
inline dtkDistributedArray<T>::dtkDistributedArray(size_type size, const_reference value) : dtkDistributedContainer(size)
{
    this->allocate(m_mapper->count(this->wid()));

    std::uninitialized_fill(m_begin, m_end, value);

    m_first_id = m_mapper->firstIndex(this->wid());
    m_comm->barrier();
}

template <typename T>
inline dtkDistributedArray<T>::dtkDistributedArray(size_type size, const_pointer array) : dtkDistributedContainer(size)
{
    this->allocate(m_mapper->count(this->wid()));

    if (array) {
        size_type i = 0;
        for (auto it = m_begin; it != m_end; ++i, ++it) {
            *it = array[m_mapper->localToGlobal(i, this->wid())];
        }
    }
    m_first_id = m_mapper->firstIndex(this->wid());
    m_comm->barrier();
}

template <typename T>
template <typename InputIt, typename>
inline dtkDistributedArray<T>::dtkDistributedArray(InputIt first, InputIt last) : dtkDistributedContainer(std::distance(first, last))
{
    this->allocate(m_mapper->count(this->wid()));

    size_type i = 0;
    auto s_it = first;
    for (auto it = m_begin; it != m_end; ++i, ++it) {
        std::advance(s_it, m_mapper->localToGlobal(i, this->wid()));
        *it = *s_it;
        s_it = first;
    }

    m_first_id = m_mapper->firstIndex(this->wid());
    m_comm->barrier();
}

template <typename T>
template <typename InputIt, typename>
inline dtkDistributedArray<T>::dtkDistributedArray(size_type size, InputIt first, InputIt last) : dtkDistributedContainer(size)
{
    this->allocate(m_mapper->count(this->wid()));

    std::uninitialized_copy(first, last, m_begin);

    m_first_id = m_mapper->firstIndex(this->wid());
    m_comm->barrier();
}

template <typename T>
inline dtkDistributedArray<T>::dtkDistributedArray(const dtkDistributedArray& o) : dtkDistributedContainer(o.size(), o.mapper())
{
    this->allocate(m_mapper->count(this->wid()));

    std::uninitialized_copy(o.m_begin, o.m_end, m_begin);

    m_first_id = m_mapper->firstIndex(this->wid());
    m_comm->barrier();
}

template <typename T>
inline dtkDistributedArray<T>::~dtkDistributedArray(void)
{
    this->clear();
}

template <typename T>
inline dtkDistributedArray<T>& dtkDistributedArray<T>::operator = (const dtkDistributedArray<T>& o)
{
    if (m_size == o.m_size) {
        if (std::is_trivially_default_constructible<value_type>::value) {
            std::uninitialized_copy(o.m_begin, o.m_end, m_begin);
        } else {
            std::copy(o.m_begin, o.m_end, m_begin);
        }
        m_cache->clear();

    } else {
        qCritical() << "can't copy: not the same size!" <<  m_size << o.m_size;
        this->clear();
    }
    return *this;
}

template <typename T>
inline dtkDistributedArray<T>& dtkDistributedArray<T>::operator += (const dtkDistributedArray& o)
{
    size_type local_size = static_cast<size_type>(m_end - m_begin);
    pointer o_ptr = const_cast<pointer>(o.data());
    m_comm->barrier();
    this->wlock();
    this->addAssign(m_first_id, o_ptr, local_size);
    this->unlock();
    m_comm->barrier();
    return *this;
}

template <typename T>
inline dtkDistributedArray<T>& dtkDistributedArray<T>::operator -= (const dtkDistributedArray& o)
{
    size_type local_size = static_cast<size_type>(m_end - m_begin);
    pointer o_ptr = const_cast<pointer>(o.data());
    m_comm->barrier();
    this->wlock();
    this->subAssign(m_first_id, o_ptr, local_size);
    this->unlock();
    m_comm->barrier();
    return *this;
}

template <typename T>
inline dtkDistributedArray<T>& dtkDistributedArray<T>::operator *= (const_reference value)
{
    std::transform(m_begin, m_end, m_begin, [&value](auto&& s) { return value * s; });
    m_cache->clear();
    m_comm->barrier();
    return *this;
}

template <typename T>
inline void dtkDistributedArray<T>::remap(dtkDistributedMapper *remapper)
{
    auto old_begin = m_begin;
    auto old_end = m_end;
    m_begin = m_end = nullptr;
    auto old_buffer_manager = m_buffer_manager;

    this->allocate(remapper->count(this->wid()));

    auto new_begin = m_begin;
    auto new_end = m_end;
    auto new_buffer_manager = m_buffer_manager;
    m_begin = old_begin;
    m_end = old_end;
    m_buffer_manager = old_buffer_manager;

    if (!new_begin) {
        return;
    }
    size_type i = 0;
    for (auto it = new_begin; it != new_end; ++i, ++it) {
        *it = this->at(remapper->localToGlobal(i, this->wid()));
    }

    m_comm->barrier();

    this->setMapper(remapper);
    m_cache->clear();
    this->deallocate();
    m_begin = new_begin;
    m_end = new_end;
    m_buffer_manager = new_buffer_manager;

    m_first_id = m_mapper->firstIndex(this->wid());
    m_comm->barrier();
}

template <typename T>
inline void dtkDistributedArray<T>::rlock(qint32 owner)
{
    m_buffer_manager->rlock(owner);
}

template <typename T>
inline void dtkDistributedArray<T>::wlock(qint32 owner)
{
    m_buffer_manager->wlock(owner);
}

template <typename T>
inline void dtkDistributedArray<T>::unlock(qint32 owner)
{
    m_buffer_manager->unlock(owner);
}

template <typename T>
inline void dtkDistributedArray<T>::rlock(void)
{
    m_buffer_manager->rlock();
    m_locked = true;
}

template <typename T>
inline void dtkDistributedArray<T>::wlock(void)
{
    m_buffer_manager->wlock();
    m_locked = true;
}

template <typename T>
inline void dtkDistributedArray<T>::unlock(void)
{
    m_buffer_manager->unlock();
    m_locked = false;
}

template<typename T> inline void dtkDistributedArray<T>::clearCache(void) const
{
    m_cache->clear();
}

template <typename T>
inline void dtkDistributedArray<T>::fill(const_reference value)
{
    if (std::is_trivially_default_constructible<value_type>::value) {
        std::uninitialized_fill(m_begin, m_end, value);
    } else {
        std::fill(m_begin, m_end, value);
    }
    m_cache->clear();
    m_comm->barrier();
}

template <typename T>
inline void dtkDistributedArray<T>::setAt(size_type index, const_reference value)
{
    qint32 owner = m_mapper->owner(index);

    if (this->wid() == owner) {
        size_type pos = index - m_first_id;

        if (m_locked) {
            m_begin[pos] = value;
        } else {
            m_buffer_manager->put(owner, pos, &(const_cast<T&>(value)));
        }
    } else {
        size_type pos = m_mapper->globalToLocal(index, owner);
        m_buffer_manager->put(owner, pos, &(const_cast<T&>(value)));
    }
}

template <typename T>
inline void dtkDistributedArray<T>::setAt(size_type index, const_pointer array, size_type size)
{
    qint32 owner = m_mapper->owner(index);
    size_type pos = m_mapper->globalToLocal(index, owner);

    size_type owner_capacity = m_mapper->lastIndex(owner) - index + 1;

    if (size <= owner_capacity) {
        m_buffer_manager->put(owner, pos, const_cast<pointer>(array), size);

    } else {
        m_buffer_manager->put(owner, pos, const_cast<pointer>(array), owner_capacity);
        this->setAt(index + owner_capacity, const_cast<pointer>(array) + owner_capacity, size - owner_capacity);
    }
}

template <typename T>
inline auto dtkDistributedArray<T>::at(size_type index) const -> value_type
{
    qint32 owner = m_mapper->owner(index);

    if (this->wid() == owner) {
        size_type pos = index - m_first_id;

        if (m_locked) {
            return m_begin[pos];
        } else {
            m_buffer_manager->get(owner, pos, &m_val);
            return m_val;
        }

    } else if (m_buffer_manager->shouldCache(owner)) {
        return m_cache->value(index, owner);

    } else {
        size_type pos = m_mapper->globalToLocal(index, owner);
        m_buffer_manager->get(owner, pos, &m_val);
        return m_val;
    }
}

template <typename T>
inline void dtkDistributedArray<T>::range(size_type index, pointer array, size_type count) const
{
    qint32 owner = m_mapper->owner(index);
    size_type pos = m_mapper->globalToLocal(index, owner);

    size_type owner_capacity = m_mapper->lastIndex(owner) - index + 1;

    if (count <= owner_capacity) {
        m_buffer_manager->get(owner, pos, array, count);

    } else {
        m_buffer_manager->get(owner, pos, array, owner_capacity);
        this->get(index + owner_capacity, array + owner_capacity, count - owner_capacity);
    }
}

template
<typename T> inline auto dtkDistributedArray<T>::front(void) const -> value_type
{
    return this->at(0);
}

template <typename T>
inline auto dtkDistributedArray<T>::back(void) const -> value_type
{
    return this->at(this->size() - 1);
}

template <typename T>
inline auto dtkDistributedArray<T>::operator[](size_type index) const -> value_type
{
    return this->at(index);
}

template <typename T>
inline void dtkDistributedArray<T>::addAssign(size_type index, const_reference value)
{
    qint32 owner = m_mapper->owner(index);

    if (this->wid() == owner) {
        size_type pos = index - m_first_id;

        if (m_locked) {
            m_begin[pos] += value;
        } else {
            m_buffer_manager->addAssign(owner, pos, &(const_cast<reference>(value)));
        }
    } else {
        size_type pos = m_mapper->globalToLocal(index, owner);
        m_buffer_manager->addAssign(owner, pos, &(const_cast<reference>(value)));
    }
}

template <typename T>
inline void dtkDistributedArray<T>::subAssign(size_type index, const_reference value)
{
    qint32 owner = m_mapper->owner(index);

    if (this->wid() == owner) {
        size_type pos = index - m_first_id;

        if (m_locked) {
            m_begin[pos] -= value;
        } else {
            m_buffer_manager->subAssign(owner, pos, &(const_cast<reference>(value)));
        }
    } else {
        size_type pos = m_mapper->globalToLocal(index, owner);
        m_buffer_manager->subAssign(owner, pos, &(const_cast<reference>(value)));
    }
}

template <typename T>
inline void dtkDistributedArray<T>::mulAssign(size_type index, const_reference value)
{
    qint32 owner = m_mapper->owner(index);

    if (this->wid() == owner) {
        size_type pos = index - m_first_id;

        if (m_locked) {
            m_begin[pos] *= value;
        } else {
            m_buffer_manager->mulAssign(owner, pos, &(const_cast<reference>(value)));
        }
    } else {
        size_type pos = m_mapper->globalToLocal(index, owner);
        m_buffer_manager->mulAssign(owner, pos, &(const_cast<reference>(value)));
    }
}

template <typename T>
inline void dtkDistributedArray<T>::divAssign(size_type index, const_reference value)
{
    qint32 owner = m_mapper->owner(index);

    if (this->wid() == owner) {
        size_type pos = index - m_first_id;

        if (m_locked) {
            m_begin[pos] /= value;
        } else {
            m_buffer_manager->divAssign(owner, pos, &(const_cast<reference>(value)));
        }
    } else {
        size_type pos = m_mapper->globalToLocal(index, owner);
        m_buffer_manager->divAssign(owner, pos, &(const_cast<reference>(value)));
    }
}

template <typename T>
inline bool dtkDistributedArray<T>::compareAndSwap(size_type index, reference array, reference compare)
{
    qint32 owner = m_mapper->owner(index);
    size_type pos = m_mapper->globalToLocal(index, owner);

    return m_buffer_manager->compareAndSwap(owner, pos, &array, &compare);
}

template <typename T>
inline void dtkDistributedArray<T>::addAssign(size_type index, pointer array, size_type count)
{
    qint32 owner = m_mapper->owner(index);
    size_type pos = m_mapper->globalToLocal(index, owner);

    size_type owner_capacity = m_mapper->lastIndex(owner) - index + 1;

    if (count <= owner_capacity) {
        m_buffer_manager->addAssign(owner, pos, array, count);

    } else {
        m_buffer_manager->addAssign(owner, pos, array, owner_capacity);
        this->addAssign(index + owner_capacity, array + owner_capacity, count - owner_capacity);
    }
}

template <typename T>
inline void dtkDistributedArray<T>::subAssign(size_type index, pointer array, size_type count)
{
    qint32 owner = m_mapper->owner(index);
    size_type pos = m_mapper->globalToLocal(index, owner);

    size_type owner_capacity = m_mapper->lastIndex(owner) - index + 1;

    if (count <= owner_capacity) {
        m_buffer_manager->subAssign(owner, pos, array, count);

    } else {
        m_buffer_manager->subAssign(owner, pos, array, owner_capacity);
        this->subAssign(index + owner_capacity, array + owner_capacity, count - owner_capacity);
    }
}

template <typename T>
inline void dtkDistributedArray<T>::mulAssign(size_type index, pointer array, size_type count)
{
    qint32 owner = m_mapper->owner(index);
    size_type pos = m_mapper->globalToLocal(index, owner);

    size_type owner_capacity = m_mapper->lastIndex(owner) - index + 1;

    if (count <= owner_capacity) {
        m_buffer_manager->mulAssign(owner, pos, array, count);

    } else {
        m_buffer_manager->mulAssign(owner, pos, array, owner_capacity);
        this->mulAssign(index + owner_capacity, array + owner_capacity, count - owner_capacity);
    }
}

template <typename T>
inline void dtkDistributedArray<T>::divAssign(size_type index, pointer array, size_type count)
{
    qint32 owner = m_mapper->owner(index);
    size_type pos = m_mapper->globalToLocal(index, owner);

    size_type owner_capacity = m_mapper->lastIndex(owner) - index + 1;

    if (count <= owner_capacity) {
        m_buffer_manager->divAssign(owner, pos, array, count);

    } else {
        m_buffer_manager->divAssign(owner, pos, array, owner_capacity);
        this->divAssign(index + owner_capacity, array + owner_capacity, count - owner_capacity);
    }
}

template <typename T>
inline void dtkDistributedArray<T>::copyIntoArray(size_type index, qint32 owner, pointer array, size_type& size) const
{
    size = std::min(size, m_mapper->lastIndex(owner) - index + 1);
    size_type pos = m_mapper->globalToLocal(index, owner);

    m_buffer_manager->get(owner, pos, array, size);
}

template <typename T>
inline auto dtkDistributedArray<T>::begin(void) noexcept -> iterator
{
    return m_begin;
}

template <typename T>
inline auto dtkDistributedArray<T>::end(void) noexcept -> iterator
{
    return m_end;
}

template <typename T>
inline auto dtkDistributedArray<T>::begin(void) const noexcept -> const_iterator
{
    return m_begin;
}

template <typename T>
inline auto dtkDistributedArray<T>::end(void) const noexcept -> const_iterator
{
    return m_end;
}

template <typename T>
inline auto dtkDistributedArray<T>::cbegin(void) const noexcept -> const_iterator
{
    return m_begin;
}

template <typename T>
inline auto dtkDistributedArray<T>::cend(void) const noexcept -> const_iterator
{
    return m_end;
}

template <typename T>
inline auto dtkDistributedArray<T>::data(void) const noexcept -> const_pointer
{
    return m_begin;
}

template <typename T>
inline auto dtkDistributedArray<T>::data(void) noexcept -> pointer
{
    return m_begin;
}

template <typename T>
inline auto dtkDistributedArray<T>::constData(void) const noexcept -> const_pointer
{
    return m_begin;
}

template <typename T>
auto inline dtkDistributedArray<T>::toNavigator(void) const -> Navigator
{
    return Navigator(*this, 0, size());
}

template <typename T>
inline void dtkDistributedArray<T>::stats(void) const
{
    qDebug() << Q_FUNC_INFO << m_comm->rank() << "cache hitrate:" << m_cache->hitrate();
}

//
// dtkDistributedArray.tpp ends here
