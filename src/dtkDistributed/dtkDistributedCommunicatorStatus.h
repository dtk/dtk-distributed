// dtkDistributedCommunicatorStatus.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

class DTKDISTRIBUTED_EXPORT dtkDistributedCommunicatorStatus
{
public:
     dtkDistributedCommunicatorStatus(void);
    ~dtkDistributedCommunicatorStatus(void);

    int tag(void) const;
    qlonglong count(void) const;
    qint32 source(void) const;
    int error(void) const;

    void setTag(int tag);
    void setCount(qlonglong count);
    void setSource(qint32 source);
    void setError(int error);

private:
    class dtkDistributedCommunicatorStatusPrivate *d;
};

//
// dtkDistributedCommunicatorStatus.h ends here
