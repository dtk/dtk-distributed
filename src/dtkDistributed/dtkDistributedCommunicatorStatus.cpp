// dtkDistributedCommunicatorStatus.cpp
//

#include "dtkDistributedCommunicatorStatus.h"

class dtkDistributedCommunicatorStatusPrivate
{
public:
    int tag = 0;
    qlonglong count = -1;
    qint32 source = -1;
    int error = 0;
};

dtkDistributedCommunicatorStatus::dtkDistributedCommunicatorStatus(void) : d(new dtkDistributedCommunicatorStatusPrivate)
{

}

dtkDistributedCommunicatorStatus::~dtkDistributedCommunicatorStatus(void)
{
    delete d;
    d = nullptr;
}

qlonglong dtkDistributedCommunicatorStatus::count(void) const
{
    return d->count;
}

qint32 dtkDistributedCommunicatorStatus::source(void) const
{
    return d->source;
}

int dtkDistributedCommunicatorStatus::error(void) const
{
    return d->error;
}

int dtkDistributedCommunicatorStatus::tag(void) const
{
    return d->tag;
}

void dtkDistributedCommunicatorStatus::setTag(int tag)
{
    d->tag = tag;
}

void dtkDistributedCommunicatorStatus::setCount(qlonglong count)
{
    d->count = count;
}

void dtkDistributedCommunicatorStatus::setSource(qint32 source)
{
    d->source = source;
}

void dtkDistributedCommunicatorStatus::setError(qint32 error)
{
    d->error = error;
}

//
// dtkDistributedCommunicatorStatus.cpp ends here
