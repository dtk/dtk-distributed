// dtkDistributedPolicy.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

class dtkDistributedCommunicator;

class DTKDISTRIBUTED_EXPORT dtkDistributedPolicy : public QObject
{
    Q_OBJECT

public:
     dtkDistributedPolicy(void);
     dtkDistributedPolicy(const dtkDistributedPolicy& other);
    ~dtkDistributedPolicy(void);

    dtkDistributedPolicy& operator = (const dtkDistributedPolicy& other);

    void addHost(const QString& host);
    void setType(const QString& type);
    void setNWorkers(qlonglong nworkers);
    void setHostsFromEnvironment(void);

    Q_INVOKABLE QStringList types(void) const;

    QStringList hosts(void) const;
    dtkDistributedCommunicator *communicator(void) const;

protected:
    class dtkDistributedPolicyPrivate *d = nullptr;
};

//
// dtkDistributedPolicy.h ends here
