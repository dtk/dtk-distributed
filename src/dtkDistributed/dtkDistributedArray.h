// dtkDistributedArray.h
//

#pragma once

#include "dtkDistributedArrayCache.h"
#include "dtkDistributedContainer.h"
#include "dtkDistributedNavigator.h"

namespace dtkDistributed
{
    namespace detail
    {
        template <class It>
        using require_input_iter = typename std::enable_if<std::is_convertible<typename std::iterator_traits<It>::iterator_category,
                                                                               std::input_iterator_tag>::value>::type;
    }
}

class dtkDistributedMapper;
class dtkDistributedBufferManager;

// /////////////////////////////////////////////////////////////////
// dtkDistributedArray
// /////////////////////////////////////////////////////////////////

template <typename T> class dtkDistributedArray : public dtkDistributedContainer
{
public:
    using value_type = T;
    using pointer = T*;
    using const_pointer = const T*;
    using reference = T&;
    using const_reference = const T&;

    using size_type = qlonglong;
    using difference_type = std::ptrdiff_t;

    using iterator = pointer;
    using const_iterator = const_pointer;

    using Cache = dtkDistributedArrayCache<T>;
    using Navigator = dtkDistributedNavigator<dtkDistributedArray<T>>;

    dtkDistributedArray(size_type size);
    dtkDistributedArray(size_type size, dtkDistributedMapper *mapper);
    dtkDistributedArray(size_type size, const_reference value);
    dtkDistributedArray(size_type size, const_pointer array);

    template <class InputIt, class = dtkDistributed::detail::require_input_iter<InputIt>>
    dtkDistributedArray(InputIt first, InputIt last); // Global Init
    template <class InputIt, class = dtkDistributed::detail::require_input_iter<InputIt>>
    dtkDistributedArray(size_type size, InputIt first, InputIt last); // Local Init

    dtkDistributedArray(const dtkDistributedArray& o);

    ~dtkDistributedArray(void);

    dtkDistributedArray& operator  = (const dtkDistributedArray& o);
    dtkDistributedArray& operator += (const dtkDistributedArray& other);
    dtkDistributedArray& operator -= (const dtkDistributedArray& other);

    dtkDistributedArray& operator *= (const_reference value);

    void remap(dtkDistributedMapper *remapper);

    void rlock(qint32 owner);
    void wlock(qint32 owner);
    void unlock(qint32 owner);

    void rlock(void);
    void wlock(void);
    void unlock(void);

    void clearCache(void) const;

    void fill(const_reference value);

    void setAt(size_type index, const_reference value);
    void setAt(size_type index, const_pointer array, size_type size);

    value_type at(size_type index) const;

    void range(size_type index, pointer array, size_type count) const;

    value_type front(void) const;
    value_type  back(void) const;

    value_type operator[](size_type index) const;

    void addAssign(size_type index, const_reference value);
    void subAssign(size_type index, const_reference value);
    void mulAssign(size_type index, const_reference value);
    void divAssign(size_type index, const_reference value);
    bool compareAndSwap(size_type index, reference value, reference compare);

    void addAssign(size_type index, pointer array, size_type count);
    void subAssign(size_type index, pointer array, size_type count);
    void mulAssign(size_type index, pointer array, size_type count);
    void divAssign(size_type index, pointer array, size_type count);

    void copyIntoArray(size_type from, qint32 owner, pointer array, size_type& size) const;

    pointer data(void) noexcept;
    const_pointer data(void) const noexcept;
    const_pointer constData(void) const noexcept;

    iterator begin(void) noexcept;
    iterator end(void) noexcept;

    const_iterator begin(void) const noexcept;
    const_iterator end(void) const noexcept;

    const_iterator cbegin(void) const noexcept;
    const_iterator cend(void) const noexcept;

    Navigator toNavigator(void) const;

    void stats(void) const;

private:
    void allocate(size_type local_size);
    void deallocate(void);
    void clear(void);

    pointer m_begin = nullptr;
    pointer m_end = nullptr;
    mutable value_type m_val = value_type();
    mutable Cache *m_cache = nullptr;
    dtkDistributedBufferManager *m_buffer_manager = nullptr;
    bool m_locked = false;
    size_type m_first_id = 0;
};

// ///////////////////////////////////////////////////////////////////

#include "dtkDistributedArray.tpp"

//
// dtkDistributedArray.h ends here
