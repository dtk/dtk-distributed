// dtkDistributedCommunicator.cppVersion: $Id$
//

#include "dtkDistributedCommunicator.h"

#include "dtkDistributedRequest.h"

// /////////////////////////////////////////////////////////////////
// dtkDistributedCommunicator
// /////////////////////////////////////////////////////////////////


/*!
  \class dtkDistributedCommunicator
  \inmodule dtkDistributed
  \brief dtkDistributedCommunicator is the interface for distributed computing.

  It can be used to spawn processes/threads on one or several hosts (depending on the implementation), execute code on each processes, and gives access to communications and synchronisations methods.

  The communication API is very similar to the MPI API (send, receive, broadcast, barrier, ...), but can be used without MPI ( a plugin based on qthreads is provided).

  \code
  dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();
  PingPongWork *runnable = new PingPongWork();
  QStringList hosts;
  // run ping pong on 2 process on localhost
  hosts << "localhost" << "localhost";
  comm->spawn(hosts,1);
  comm->exec(runnable);
  comm->unspawn();
  \endcode

*/

/*! \enum dtkDistributedCommunicator::OperationType
    \value None
    \value Min
    \value Max
    \value MinLoc
    \value MaxLoc
    \value Sum
    \value Product
    \value BitwiseAnd
    \value BitwiseOr
    \value BitwiseXor
    \value LogicalAnd
    \value LogicalOr
    \value LogicalXor
*/

/*! \enum dtkDistributedCommunicator::MessageTag
    \value TagSend
    \value TagReceive
    \value TagReduce
*/

/*! \fn dtkDistributedCommunicator::dtkDistributedCommunicator(void)
 *  Constructor
 */

/*! \fn dtkDistributedCommunicator::~dtkDistributedCommunicator(void)
 *  Destructor
 */

void dtkDistributedCommunicator::setWid(qint32)
{
}

bool dtkDistributedCommunicator::active(void)
{
    return false;
}

/*! \fn dtkDistributedCommunicator::spawn (QStringList hostnames, QString wrapper)

  Spawn a communicator on all hostnames. Optionnaly use a wrapper to spawn slaves. Special case: if the string list has only one value equal to "nospawn", then spawning is disabled. This can be useful if MPI spawning is not working well with others tools (debugguer or batch schedulers). This is used by the --no-spawn option of dtkDistributedApplication.

*/

/*! \fn dtkDistributedCommunicator::barrier (void)

  Blocks until all processes in the communicator have reached this method. Use it to synchronize all the processes.

*/

/*! \fn dtkDistributedCommunicator::exec (QRunnable *runnable)

  Execute the given \a runnable: each process in the communicator will call the run method of the given object.

*/

/*! \fn qint32 dtkDistributedCommunicator::rank (void)

  alias to wid()

  \sa wid()
*/

/*! \fn qint32 dtkDistributedCommunicator::wid (void)

  Return the worker id (aka rank) of the current process

  \sa rank()
*/

/*! \fn qint32 dtkDistributedCommunicator::size (void)

  Return the size of the communicator (number of processes spawned).

*/

void dtkDistributedCommunicator::send(const QVariant& v, qint32 target, qint32 tag)
{
    QByteArray bytes;
    QDataStream stream(&bytes, QIODevice::WriteOnly);
    stream << v;
    this->send(bytes, target, tag);
}

void dtkDistributedCommunicator::receive(QVariant& v, qint32 target, qint32 tag)
{
    QByteArray bytes;
    this->receive(bytes, target, tag);
    QDataStream stream(&bytes, QIODevice::ReadOnly);
    stream >> v;
}

void dtkDistributedCommunicator::receive(QVariant& v, qint32 target, qint32 tag, dtkDistributedCommunicatorStatus& status)
{
    QByteArray bytes;
    this->receive(bytes, target, tag, status);
    QDataStream stream(&bytes, QIODevice::ReadOnly);
    stream >> v;
}

// void dtkDistributedCommunicator::reduce(void *send, void *recv, std::size_t size, QMetaType::Type dataType, OperationType operationType, qint32 target, bool all)
// {
//     qCritical() << Q_FUNC_INFO << "Default operator for reduce, not implemented";
// }

void dtkDistributedCommunicator::reduce(void *send, void *recv, std::size_t size, const QMetaType& metaType, OperationType operationType, qint32 target, bool all)
{
    qCritical() << Q_FUNC_INFO << "Default operator for reduce, not implemented";
}

void dtkDistributedCommunicator::gather(void *send, void *recv, std::size_t size, QMetaType::Type dataType, qint32 target, bool all)
{
    qCritical() << Q_FUNC_INFO << "Default operator for gather, not implemented";
}

//
// dtkDistributedCommunicator.cpp ends here
