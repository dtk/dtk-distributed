// dtkDistributedContainer.cpp
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDistributedContainer.h"

#include "dtkDistributed.h"
#include "dtkDistributedCommunicator.h"
#include "dtkDistributedMapper.h"

/*!
  \class dtkDistributedContainer
  \inmodule dtkDistributed
  \brief The dtkDistributedContainer class is the base class for any distributed container.

  dtkDistributedContainer contains all the required data for a
  distributed container, namely its global size, the mapper that
  describes how the data are distributed across all the process units
  and the communicator.
*/


/*! \fn dtkDistributedContainer::dtkDistributedContainer(void)

  Constructs a distributed container and instanciates a default mapper.
*/

dtkDistributedContainer::dtkDistributedContainer(void) : m_ref(1), m_size(0), m_mapper(new dtkDistributedMapper), m_comm(dtkDistributed::communicator::instance()), m_wid(m_comm->wid())
{
    m_mapper->ref();
}

/*! \fn dtkDistributedContainer::dtkDistributedContainer(std::size_t size)

  Constructs a distributed container of size \a size and instanciates a default mapper.

*/
dtkDistributedContainer::dtkDistributedContainer(qlonglong size) : m_ref(1), m_size(size), m_mapper(new dtkDistributedMapper), m_comm(dtkDistributed::communicator::instance()), m_wid(m_comm->wid())
{
    m_mapper->ref();

    if (m_size > 0) {
        m_mapper->setMapping(m_size, m_comm->size());
    }
}

/*! \fn dtkDistributedContainer::dtkDistributedContainer(std::size_t size, dtkDistributedMapper *mapper)

  Constructs a distributed container of size \a size and with the custom mapper \a mapper.

  \sa setMapper
*/
dtkDistributedContainer::dtkDistributedContainer(qlonglong size, dtkDistributedMapper *mapper) : m_ref(1), m_size(size), m_mapper(mapper), m_comm(dtkDistributed::communicator::instance()), m_wid(m_comm->wid())
{
    m_mapper->ref();
}

/*! \fn virtual dtkDistributedContainer::~dtkDistributedContainer(void)

  Destroys the distributed container.

*/
dtkDistributedContainer::~dtkDistributedContainer(void)
{
    if (!m_mapper->deref()) {
        delete m_mapper;
    }
    m_mapper = nullptr;
}

/*! \fn void dtkDistributedContainer::setMapper(dtkDistributedMapper *mapper)

  Sets the custom mapper \a mapper.

*/
void dtkDistributedContainer::setMapper(dtkDistributedMapper *mapper)
{
    if (!m_mapper->deref()) {
        delete m_mapper;
    }
    m_mapper = mapper;
    m_mapper->ref();
}

/*! \fn bool dtkDistributedContainer::empty(void) const

  Returns \c true if the distributed container has size 0; otherwise returns \c false.

  \sa size()

*/

/*! \fn qlonglong dtkDistributedContainer::size(void) const

  Returns the number of items in the distributed container.

  \sa empty()

*/

/*! \fn dtkDistributedMapper *dtkDistributedContainer::mapper(void) const

  Returns the mapper.

*/

/*! \fn dtkDistributedCommunicator *dtkDistributedContainer::communicator(void) const

  Returns the communicator.

*/

/*! \fn qint32 dtkDistributedContainer::wid(void) const

  Returns the current worker id.

*/

//
// dtkDistributedContainer.cpp ends here
