// dtkDistributedRequest.h
//

#pragma once

#include <dtkDistributedExport>

class DTKDISTRIBUTED_EXPORT dtkDistributedRequest
{
public:
    virtual ~dtkDistributedRequest(void) = default;
};

//
// dtkDistributedRequest.h ends here
