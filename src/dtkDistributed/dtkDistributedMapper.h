// dtkDistributedMapper.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

// /////////////////////////////////////////////////////////////////
// dtkDistributedMapper interface
// /////////////////////////////////////////////////////////////////

class DTKDISTRIBUTED_EXPORT dtkDistributedMapper : public QObject
{
public:
     dtkDistributedMapper(void);
     dtkDistributedMapper(const dtkDistributedMapper& o);
     dtkDistributedMapper(dtkDistributedMapper&& o);
    ~dtkDistributedMapper(void);

    dtkDistributedMapper *scaledClone(qlonglong factor) const;

    bool deref(void);
    bool   ref(void);

    void clear(void);
    bool check(void);

    void setMapping(qlonglong id_count, qint32 pu_count);

    void initMap(qlonglong map_size, qint32 pu_size);
    void setMap(qlonglong offset, qint32 pu_id);

    qlonglong localToGlobal(qlonglong local_id, qint32 pu_id) const;

    qlonglong globalToLocal(qlonglong global_id) const;
    qlonglong globalToLocal(qlonglong global_id, qint32 owner) const;

    qlonglong count(void) const;
    qlonglong count(qint32 pu_id) const;
    qlonglong countMax(void) const;

    qlonglong firstIndex(qint32 pu_id) const;
    qlonglong  lastIndex(qint32 pu_id) const;

    qint32 owner(qlonglong global_id) const;

    friend DTKDISTRIBUTED_EXPORT QDebug operator<<(QDebug dbg, dtkDistributedMapper& mapper);
    friend DTKDISTRIBUTED_EXPORT QDebug operator<<(QDebug dbg, dtkDistributedMapper *mapper);

private:
    class dtkDistributedMapperPrivate *d = nullptr;
};

//
// dtkDistributedMapper.h ends here
