// dtkDistributedContainer.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

class dtkDistributedCommunicator;
class dtkDistributedMapper;

// /////////////////////////////////////////////////////////////////
// dtkDistributedContainer
// /////////////////////////////////////////////////////////////////

class DTKDISTRIBUTED_EXPORT dtkDistributedContainer
{
public:
    dtkDistributedContainer(void);
    dtkDistributedContainer(qlonglong size);
    dtkDistributedContainer(qlonglong size, dtkDistributedMapper *mapper);

    virtual ~dtkDistributedContainer(void);

    void setMapper(dtkDistributedMapper *mapper);

    bool empty(void) const noexcept;
    qlonglong size(void) const noexcept;

    dtkDistributedMapper *mapper(void) const;
    dtkDistributedCommunicator *communicator(void) const;

    qint32 wid(void) const noexcept;

    bool deref(void);
    bool   ref(void);

protected:
    QAtomicInt m_ref = QAtomicInt(0);
    qlonglong m_size = 0;
    dtkDistributedMapper *m_mapper = nullptr;
    dtkDistributedCommunicator *m_comm = nullptr;
    qint32 m_wid = 0;
};

// /////////////////////////////////////////////////////////////////

inline bool dtkDistributedContainer::empty(void) const noexcept
{
    return !m_size;
}

inline qlonglong dtkDistributedContainer::size(void) const noexcept
{
    return  m_size;
}

inline dtkDistributedMapper *dtkDistributedContainer::mapper(void) const
{
    return m_mapper;
}

inline dtkDistributedCommunicator *dtkDistributedContainer::communicator(void) const
{
    return m_comm;
}

inline qint32 dtkDistributedContainer::wid(void) const noexcept
{
    return m_wid;
}

inline bool dtkDistributedContainer::deref(void)
{
    return m_ref.deref();
}

inline bool dtkDistributedContainer::ref(void)
{
    return m_ref.ref();
}

//
// dtkDistributedContainer.h ends here
