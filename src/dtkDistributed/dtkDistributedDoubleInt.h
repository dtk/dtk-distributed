// dtkDistributedDoubleInt.h ---
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

struct DTKDISTRIBUTED_EXPORT dtkDistributedDoubleInt
{
    double value;
    quint32 id;
};

Q_DECLARE_METATYPE(dtkDistributedDoubleInt)

DTKDISTRIBUTED_EXPORT dtkDistributedDoubleInt operator + (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);
DTKDISTRIBUTED_EXPORT dtkDistributedDoubleInt operator - (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);
DTKDISTRIBUTED_EXPORT dtkDistributedDoubleInt operator * (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);
DTKDISTRIBUTED_EXPORT dtkDistributedDoubleInt operator / (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);

DTKDISTRIBUTED_EXPORT bool operator == (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);
DTKDISTRIBUTED_EXPORT bool operator != (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);
DTKDISTRIBUTED_EXPORT bool operator <  (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);
DTKDISTRIBUTED_EXPORT bool operator >  (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);
DTKDISTRIBUTED_EXPORT bool operator <= (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);
DTKDISTRIBUTED_EXPORT bool operator >= (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs);

DTKDISTRIBUTED_EXPORT QDataStream& operator << (QDataStream& s, const dtkDistributedDoubleInt& di);
DTKDISTRIBUTED_EXPORT QDataStream& operator >> (QDataStream& s,       dtkDistributedDoubleInt& di);

DTKDISTRIBUTED_EXPORT QDebug& operator << (QDebug&, const dtkDistributedDoubleInt& di);

//
// dtkDistributedDoubleInt.h ends here
