// dtkDistributedApplication.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

class dtkDistributedCommunicator;
class dtkDistributedPolicy;

class DTKDISTRIBUTED_EXPORT dtkDistributedApplication : public QCoreApplication
{
public:
     dtkDistributedApplication(int& argc, char **argv);
    ~dtkDistributedApplication(void);

    virtual void initialize(void);
    virtual void exec(QRunnable *task);
    virtual void spawn(QMap<QString, QString> options = QMap<QString, QString>());
    virtual void unspawn(void);

    QCommandLineParser *parser(void);

    bool isMaster(void);
    virtual bool noGui(void);

    dtkDistributedCommunicator *communicator(void);
    dtkDistributedPolicy *policy(void);

private:
    class dtkDistributedApplicationPrivate *d = nullptr;
};

//
// dtkDistributedApplication.h ends here
