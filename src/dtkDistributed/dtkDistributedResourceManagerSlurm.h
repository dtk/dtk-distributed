// dtkDistributedResourceManagerSlurm.h
//

#pragma once

#include <dtkDistributedExport>

#include "dtkDistributedResourceManager.h"

class DTKDISTRIBUTED_EXPORT dtkDistributedResourceManagerSlurm : public dtkDistributedResourceManager
{
    Q_OBJECT

public:
     dtkDistributedResourceManagerSlurm(void) = default;
    ~dtkDistributedResourceManagerSlurm(void) = default;

public slots:
    QByteArray status(void) override;
    QString submit(QString input) override;
    QString deljob(QString jobid) override;
};

//
// dtkDistributedResourceManagerSlurm.h ends here
