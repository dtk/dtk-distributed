// dtkDistributedItem.h
//

#pragma once

#include "dtkDistributedCommunicator.h"

#include <dtkCore/dtkCoreTypeTraits>

// ///////////////////////////////////////////////////////////////////

template <typename T, bool E = dtk::is_numeric<T>::value>
class dtkDistributedItem
{
public:
    explicit dtkDistributedItem(dtkDistributedCommunicator *c);
            ~dtkDistributedItem(void) = default;

    dtkDistributedItem& operator  = (const T& t);
    dtkDistributedItem& operator += (const T& t);
    dtkDistributedItem& operator -= (const T& t);
    dtkDistributedItem& operator *= (const T& t);

    T operator * (void);

    void update(void);

protected:
    T data = T();
    T buffer = T();
    dtkDistributedCommunicator *comm = nullptr;
    dtkDistributedCommunicator::OperationType op = dtkDistributedCommunicator::None;
    bool updated = true;
};

// ///////////////////////////////////////////////////////////////////

template <typename T, bool E>
inline dtkDistributedItem<T,E>::dtkDistributedItem(dtkDistributedCommunicator *c) : comm(c)
{

}

template <typename T, bool E>
inline auto dtkDistributedItem<T,E>::operator = (const T& t) -> dtkDistributedItem&
{
    op = dtkDistributedCommunicator::None;
    data = t;
    buffer = T();
    updated = true;
    return *this;
}

template <typename T, bool E>
inline auto dtkDistributedItem<T,E>::operator += (const T& t) -> dtkDistributedItem&
{
    op = dtkDistributedCommunicator::Sum;
    buffer += t;
    updated = false;
    return *this;
}

template <typename T, bool E>
inline auto dtkDistributedItem<T,E>::operator -= (const T& t) -> dtkDistributedItem&
{
    op = dtkDistributedCommunicator::Sum;
    buffer -= t;
    updated = false;
    return *this;
}

template <typename T, bool E>
inline auto dtkDistributedItem<T,E>::operator *= (const T& t) -> dtkDistributedItem&
{
    op = dtkDistributedCommunicator::Product;
    buffer *= t;
    updated = false;
    return *this;
}

template <typename T, bool E>
inline T dtkDistributedItem<T,E>::operator * (void)
{
    this->update();
    return data;
}

template <typename T, bool E>
inline void dtkDistributedItem<T,E>::update(void)
{
    if (updated) {
        return;
    }
    if (op != dtkDistributedCommunicator::None) {
        T temp = buffer;
        comm->reduce(&temp, &buffer, 1, op, 0, true);
        switch (op) {
        case dtkDistributedCommunicator::Sum:
            data += buffer;
            break;
        case dtkDistributedCommunicator::Product:
            data *= buffer;
            break;
        default:
            break;
        }
        buffer = T();
    }
    updated = true;
    return;
}

//
// dtkDistributedItem.h ends here
