// dtkDistributedCommunicatorQThread.h
//

#pragma once

#include "dtkDistributedCommunicator.h"

class dtkDistributedBufferManager;
class dtkDistributedRequest;

class dtkDistributedCommunicatorQThread : public dtkDistributedCommunicator
{
public:
     dtkDistributedCommunicatorQThread(void);
    ~dtkDistributedCommunicatorQThread(void);

    void initialize(void) override;
    bool initialized(void) override;
    void uninitialize(void) override;
    bool active(void) override;

    void spawn(QStringList hostnames, QString wrapper, QMap<QString, QString> options) override;
    void exec(QRunnable *work) override;

    void barrier(void) override;
    void unspawn(void) override;

    dtkDistributedBufferManager *createBufferManager(void) override;
    void destroyBufferManager(dtkDistributedBufferManager *&) override;

    void send(void *data, std::size_t size, QMetaType::Type dataType, qint32 target, qint32 tag) override;
    void send(QByteArray& array, qint32 target, qint32 tag) override;
    //using dtkDistributedCommunicator::send;

    void broadcast(void *data, std::size_t size, QMetaType::Type dataType, qint32 source) override;
    void broadcast(QByteArray& array, qint32 source) override;
    void broadcast(QVariant& v, qint32 source) override;
    //using dtkDistributedCommunicator::broadcast;

    void receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag) override;
    void receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status) override;
    void receive(QByteArray& v, qint32 source, qint32 tag) override;
    void receive(QByteArray& v, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status) override;
    //using dtkDistributedCommunicator::receive;

    dtkDistributedRequest *ireceive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag) override;
    //using dtkDistributedCommunicator::ireceive;

    void wait(dtkDistributedRequest *req) override;

    //void reduce(void *send, void *recv, std::size_t size, QMetaType::Type dataType, OperationType operationType, qint32 root, bool all) override;
    void reduce(void *send, void *recv, std::size_t size, const QMetaType& metaType, OperationType operationType, qint32 root, bool all) override;

    void gather(void *send, void *recv, std::size_t size, QMetaType::Type dataType, qint32 root, bool all) override;

    qint32  wid(void) override;
    qint32 size(void) override;

    void setWid(qint32 id) override;

protected:
    class dtkDistributedCommunicatorQThreadPrivate *d = nullptr;
};

static dtkDistributedCommunicator *dtkDistributedCommunicatorQThreadCreator(void)
{
    return new dtkDistributedCommunicatorQThread;
}

//
// dtkDistributedCommunicatorQThread.h ends here
