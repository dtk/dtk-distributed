// dtkDistributedResourceManagerTorque.h
//

#pragma once

#include <dtkDistributedExport>

#include "dtkDistributedResourceManager.h"

class dtkDistributedResourceManagerTorque : public dtkDistributedResourceManager
{
    Q_OBJECT

public:
     dtkDistributedResourceManagerTorque(void) = default;
    ~dtkDistributedResourceManagerTorque(void) = default;

public slots:
    QByteArray status(void) override;
    QString submit(QString input) override;
    QString deljob(QString jobid) override;
};

//
// dtkDistributedResourceManagerTorque.h ends here
