// dtkDistributedResourceManager.h
//

#pragma once

#include <dtkDistributedExport>

#include <QObject>

class dtkDistributedServerDaemon;

class DTKDISTRIBUTED_EXPORT dtkDistributedResourceManager : public QObject
{
    Q_OBJECT

public:
     dtkDistributedResourceManager(void) = default;
    ~dtkDistributedResourceManager(void) = default;

    static QString protocol(void);

public slots:
    virtual QByteArray status(void) = 0;
    virtual QString submit(QString input) = 0;
    virtual QString deljob(QString jobid) = 0;

public:
    dtkDistributedServerDaemon *server(void) const;
    void setServer(dtkDistributedServerDaemon *server);

private:
    dtkDistributedServerDaemon *m_server = nullptr;
};

//
// dtkDistributedResourceManager.h ends here
