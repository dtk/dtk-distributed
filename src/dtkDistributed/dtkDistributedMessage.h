// dtkDistributedMessage.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

class QTcpSocket;

class DTKDISTRIBUTED_EXPORT dtkDistributedMessage
{
public:
    using dtkDistributedHeaders = QHash<QString, QString>;

    static const qint16 CONTROLLER_RANK = -1 ;
    static const qint16 SERVER_RANK = -2 ;
    static const qint16 CONTROLLER_RUN_RANK = -5 ;
    static const qint16 SLAVE_RANK = -4 ; // identify the slave tcp connection that handle composition send/receive.

    enum Method {
        STATUS  ,
        OKSTATUS,
        NEWJOB  ,
        OKJOB   ,
        ERRORJOB,
        DELJOB  ,
        OKDEL   ,
        ERRORDEL,
        ENDJOB  ,
        DATA    ,
        SETRANK ,
        ERROR_UNKNOWN,
        STOP
    };

    dtkDistributedMessage(void);
    dtkDistributedMessage(Method method, QString jobid = "", int rank = SERVER_RANK, qint64 size = 0, QString type = "json",  const QByteArray&  content = QByteArray(), const dtkDistributedHeaders& headers = dtkDistributedHeaders());
    dtkDistributedMessage(Method method, QString jobid, int rank, const QVariant&  v, const dtkDistributedHeaders& headers = dtkDistributedHeaders());

    virtual ~dtkDistributedMessage(void);

    void addHeader(QString name, QString value);
    void setHeader(const QString& headers);
    void setMethod(QString method);
    void setMethod(Method method);
    void setJobid(const QString& jobid);
    void setRank(qint16 rank);
    void setType(void);
    void setSize(void);
    void setContent(QByteArray& content);

    Method method(void) const;
    QString methodString(void) const;
    QString req(void) const;
    QString jobid(void) const;
    int rank(void) const;
    QString header(const QString& name) const;
    QString type(void) const;
    qint64 size(void) const;

    QByteArray content(void) const;
    QVariant variant(void);

    dtkDistributedHeaders headers(void) const;

    qlonglong  send(QTcpSocket *socket);
    qlonglong parse(QTcpSocket *socket);

private:
    class dtkDistributedMessagePrivate *d = nullptr;
};

//
// dtkDistributedMessage.h ends here
