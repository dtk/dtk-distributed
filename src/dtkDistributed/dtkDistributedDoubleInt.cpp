// dtkDistributedDoubleInt.cpp ---
//

#include "dtkDistributedDoubleInt.h"

dtkDistributedDoubleInt operator + (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    dtkDistributedDoubleInt res;
    res.value = lhs.value + rhs.value;
    res.id = lhs.id;
    return res;
}

dtkDistributedDoubleInt operator - (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    dtkDistributedDoubleInt res;
    res.value = lhs.value - rhs.value;
    res.id = lhs.id;
    return res;
}

dtkDistributedDoubleInt operator * (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    dtkDistributedDoubleInt res;
    res.value = lhs.value * rhs.value;
    res.id = lhs.id;
    return res;
}

dtkDistributedDoubleInt operator / (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    dtkDistributedDoubleInt res;
    res.value = lhs.value / rhs.value;

    res.id = lhs.id;
    return res;
}

bool operator == (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    return lhs.value == rhs.value;
}

bool operator != (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    return lhs.value != rhs.value;
}

bool operator < (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    return lhs.value < rhs.value;
}

bool operator > (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    return lhs.value > rhs.value;
}

bool operator <= (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    return lhs.value <= rhs.value;
}

bool operator >= (const dtkDistributedDoubleInt& lhs, const dtkDistributedDoubleInt& rhs)
{
    return lhs.value >= rhs.value;
}

QDataStream& operator << (QDataStream& s, const dtkDistributedDoubleInt& di)
{
    s << di.value;
    s << di.id;
    return s;
}

QDataStream& operator >> (QDataStream& s, dtkDistributedDoubleInt& di)
{
    s >> di.value;
    s >> di.id;
    return s;
}

QDebug& operator << (QDebug& dbg, const dtkDistributedDoubleInt& di)
{
    const bool old_setting = dbg.autoInsertSpaces();
    dbg.nospace() << QString("dtkDistributedDoubleInt: { %1, %2 }").arg(di.value).arg(di.id);
    dbg.setAutoInsertSpaces(old_setting);
    return dbg.maybeSpace();
}

//
// dtkDistributedDoubleInt.cpp ends here
