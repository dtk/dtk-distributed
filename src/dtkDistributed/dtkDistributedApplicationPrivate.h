// dtkDistributedApplicationPrivate.h
//

#pragma once

#include <dtkDistributedExport>

#include <dtkCore/dtkCoreApplication_p.h>

#include "dtkDistributedPolicy.h"


class DTKDISTRIBUTED_EXPORT dtkDistributedApplicationPrivate : public dtkCoreApplicationPrivate
{
public:
     dtkDistributedApplicationPrivate(void) = default;
    ~dtkDistributedApplicationPrivate(void);

    void initialize(void) override;

    void exec(QRunnable *task);
    void spawn(QMap<QString, QString> options = QMap<QString, QString>());
    void unspawn(void);

    dtkDistributedPolicy policy;
    bool spawned = false;
    bool nospawn = false;
    QString wrapper;
    QString smp;
};

//
// dtkDistributedApplicationPrivate.h ends here
