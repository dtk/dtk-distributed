// dtkDistributedBufferManagerQThread.cpp
//

#include "dtkDistributedBufferManagerQThread.h"

#include "dtkDistributedCommunicator.h"

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferManagerQThreadPrivate
// ///////////////////////////////////////////////////////////////////

class dtkDistributedBufferManagerQThreadPrivate
{
public:
    dtkDistributedCommunicator *comm = nullptr;

    qlonglong size; // size = # of threads
    qlonglong object_size;
    QVarLengthArray<char *, 64> buffers;
    QVarLengthArray<QReadWriteLock *, 64> locks;
    QVarLengthArray<QAtomicInt, 64> locked;
};

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferManagerQThread implementation
// ///////////////////////////////////////////////////////////////////

dtkDistributedBufferManagerQThread::dtkDistributedBufferManagerQThread(dtkDistributedCommunicator *comm) : d(new dtkDistributedBufferManagerQThreadPrivate)
{
    Q_ASSERT(comm);

    d->comm = comm;
    d->size = d->comm->size();
    d->buffers.resize(d->size);
    d->locks.resize(d->size);
    d->locked.resize(d->size);

    for (int i = 0; i < d->size; ++i) {
        d->locks[i] = new QReadWriteLock;
        d->locked[i].storeRelaxed(0);
    }
}

dtkDistributedBufferManagerQThread::~dtkDistributedBufferManagerQThread(void)
{
    for (int i = 0; i < d->size; ++i) {
        delete d->locks[i];
    }

    delete d;
    d = nullptr;
}

void *dtkDistributedBufferManagerQThread::allocate(std::size_t objectSize, std::size_t capacity, int metatype_id)
{
    Q_UNUSED(metatype_id);

    if (capacity == 0) {
        return nullptr;
    }

    char *buffer;
    d->comm->barrier();
    d->object_size = objectSize;
    qint32 wid = d->comm->wid();
    d->locks[wid]->lockForWrite();
    d->buffers[wid] = static_cast<char *>(::malloc(objectSize * capacity));
    buffer = d->buffers[wid];
    d->locks[wid]->unlock();
    d->comm->barrier();

    return buffer;
}

void dtkDistributedBufferManagerQThread::deallocate(void *buffer, std::size_t objectSize)
{
    Q_ASSERT(d->object_size == objectSize);

    if (!buffer) {
        return;
    }

    d->comm->barrier();
    qint32 wid = d->comm->wid();

    if (d->buffers[wid] == buffer) {
        d->locks[wid]->lockForWrite();
        ::free(d->buffers[wid]);
        d->locks[wid]->unlock();
        d->object_size = 0;
    }

    d->comm->barrier();
}

bool dtkDistributedBufferManagerQThread::shouldCache(qint32 owner)
{
    return false;
}

void dtkDistributedBufferManagerQThread::rlock(void)
{
    d->comm->barrier();
    qint32 wid = d->comm->wid();

    if (d->locked[wid].testAndSetRelaxed(0, 1)) {
        d->locks[wid]->lockForRead();
    }

    d->comm->barrier();
}

void dtkDistributedBufferManagerQThread::rlock(qint32 wid)
{
    if (d->locked[wid].testAndSetRelaxed(0, 1)) {
        d->locks[wid]->lockForRead();
    }
}


void dtkDistributedBufferManagerQThread::wlock(void)
{
    d->comm->barrier();
    qint32 wid = d->comm->wid();

    if (d->locked[wid].testAndSetRelaxed(0, 1)) {
        d->locks[wid]->lockForWrite();
    }

    d->comm->barrier();
}

void dtkDistributedBufferManagerQThread::wlock(qint32 wid)
{
    d->locked[wid].storeRelaxed(1);
    d->locks[wid]->lockForWrite();
}

void dtkDistributedBufferManagerQThread::unlock(qint32 wid)
{
    d->locks[wid]->unlock();
    d->locked[wid].storeRelaxed(0);
}

void dtkDistributedBufferManagerQThread::unlock(void)
{
    d->comm->barrier();
    qint32 wid = d->comm->wid();

    if (d->locked[wid].testAndSetRelaxed(1, 0)) {
        d->locks[wid]->unlock();
    }

    d->comm->barrier();
}

bool dtkDistributedBufferManagerQThread::locked(qint32 wid)
{
    return (d->locked[wid].loadRelaxed() == 1);
}

void dtkDistributedBufferManagerQThread::get(qint32 from, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((from >= 0 || from < d->comm->size()));

    char *buffer = d->buffers[from];
    int locked = d->locked[from].loadRelaxed();

    if (!locked) {
        QReadWriteLock *lock = d->locks[from];
        lock->lockForRead();
        memcpy(array, buffer + position * d->object_size, d->object_size * nelements);
        lock->unlock();
    } else {
        memcpy(array, buffer + position * d->object_size, d->object_size * nelements);
    }
}

void dtkDistributedBufferManagerQThread::put(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));
    char *buffer = d->buffers[dest];
    int locked = d->locked[dest].loadRelaxed();

    if (!locked) {
        QReadWriteLock *lock = d->locks[dest];
        lock->lockForWrite();
        memcpy(buffer + position * d->object_size, array, d->object_size * nelements);
        lock->unlock();
    } else {
        memcpy(buffer + position * d->object_size, array, d->object_size * nelements);
    }
}

void dtkDistributedBufferManagerQThread::addAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));
    char *buffer = d->buffers[dest];
    int locked = d->locked[dest].loadRelaxed();

    if (!locked) {
        QReadWriteLock *lock = d->locks[dest];
        lock->lockForWrite();
        this->operation_manager->addAssign(buffer + position * d->object_size, array, nelements);
        lock->unlock();
    } else {
        this->operation_manager->addAssign(buffer + position * d->object_size, array, nelements);
    }
}

void dtkDistributedBufferManagerQThread::subAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));
    char *buffer = d->buffers[dest];
    int locked = d->locked[dest].loadRelaxed();

    if (!locked) {
        QReadWriteLock *lock = d->locks[dest];
        lock->lockForWrite();
        this->operation_manager->subAssign(buffer + position * d->object_size, array, nelements);
        lock->unlock();
    } else {
        this->operation_manager->subAssign(buffer + position * d->object_size, array, nelements);
    }
}

void dtkDistributedBufferManagerQThread::mulAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));
    char *buffer = d->buffers[dest];
    int locked = d->locked[dest].loadRelaxed();

    if (!locked) {
        QReadWriteLock *lock = d->locks[dest];
        lock->lockForWrite();
        this->operation_manager->mulAssign(buffer + position * d->object_size, array, nelements);
        lock->unlock();
    } else {
        this->operation_manager->mulAssign(buffer + position * d->object_size, array, nelements);
    }
}

void dtkDistributedBufferManagerQThread::divAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));
    char *buffer = d->buffers[dest];
    int locked = d->locked[dest].loadRelaxed();

    if (!locked) {
        QReadWriteLock *lock = d->locks[dest];
        lock->lockForWrite();
        this->operation_manager->divAssign(buffer + position * d->object_size, array, nelements);
        lock->unlock();
    } else {
        this->operation_manager->divAssign(buffer + position * d->object_size, array, nelements);
    }
}

bool dtkDistributedBufferManagerQThread::compareAndSwap(qint32 dest, std::size_t position, void *array, void *compare)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));
    char *buffer = d->buffers[dest];

    // should we lock ?
    return this->operation_manager->compareAndSwap(buffer + position * d->object_size, array, compare);
}


bool dtkDistributedBufferManagerQThread::canHandleOperationManager(void)
{
    return (d->comm->wid() == 0);
}


//
// dtkDistributedBufferManagerQThread.cpp ends here
