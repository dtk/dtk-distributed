// dtkDistributedResourceManagerLocal.h
//

#pragma once

#include <dtkDistributedExport>

#include "dtkDistributedResourceManager.h"

class DTKDISTRIBUTED_EXPORT dtkDistributedResourceManagerLocal : public dtkDistributedResourceManager
{
    Q_OBJECT

public:
     dtkDistributedResourceManagerLocal(void);
    ~dtkDistributedResourceManagerLocal(void);

public slots:
    QByteArray status(void) override;
    QString submit(QString input) override;
    QString deljob(QString jobid) override;

private:
    class dtkDistributedResourceManagerLocalPrivate *d = nullptr;
};

//
// dtkDistributedResourceManagerLocal.h ends here
