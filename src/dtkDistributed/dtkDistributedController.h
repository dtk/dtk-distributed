// dtkDistributedController.h
//

#pragma once

#include <dtkDistributedExport>

#include <QtCore>

class QTcpSocket;
class dtkDistributedMessage;

class DTKDISTRIBUTED_EXPORT dtkDistributedController : public QObject
{
    Q_OBJECT

public:
     dtkDistributedController(QObject *parent = 0);
    ~dtkDistributedController(void);

    static dtkDistributedController *instance(void);

    Q_INVOKABLE static quint16 defaultPort(void);

    bool    isConnected(const QUrl& server);
    bool isDisconnected(const QUrl& server);

    QTcpSocket *socket(const QString& jobid);

    bool is_running(const QString& jobid);

signals:
    void    connected(const QUrl& server);
    void disconnected(const QUrl& server);

signals:
    void updated(void);
    void updated(const QUrl& server);
    void envUpdated(const QString& conda_env_file);

signals:
    void dataPosted(QVariant data);
    void jobEnded(QString jobid);
    void jobError(QString jobid);
    void jobStarted(QString jobid);
    void jobQueued(QString jobid, QString submit_id);

public slots:
    bool    connectSrv(const QUrl& server, bool ssh_rank = false, bool emit_connected = true);
    void disconnectSrv(const QUrl& server);
    void       stop(const QUrl& server);
    void    refresh(const QUrl& server);
    void    killjob(const QUrl& server, QString jobid);
    void       send(dtkDistributedMessage *msg);
    void       send(QVariant v, QString jobid, qint16 destrank);
    bool bootstrapConda(const QUrl& server, const QString& miniconda_url);
    bool  deployEnv(const QUrl& server, const QString& conda_env_file);
    bool     deploy(const QUrl& server, const QString& type = "local", bool ssh_tunnel = false, const QString& path = "./dtkDistributedServer", const QString& loglevel = "info", const QString& working_directory = "");
    bool     submit(const QUrl& server, const QByteArray& resources, const QString& submit_id = "");

protected slots:
    void read(void);
    void error(int error);
    void cleanup(void);

private:
    static dtkDistributedController *s_instance;

    class dtkDistributedControllerPrivate *d;
};

//
// dtkDistributedController.h ends here
