// dtkDistributedGuiApplication.h
//

#pragma once

#include <dtkDistributedWidgetsExport>

#include <QtWidgets/QApplication>

#include <QtCore>

class dtkDistributedCommunicator;
class dtkDistributedPolicy;

class DTKDISTRIBUTEDWIDGETS_EXPORT dtkDistributedGuiApplication : public QApplication
{
public:
     dtkDistributedGuiApplication(int& argc, char **argv);
    ~dtkDistributedGuiApplication(void);

    virtual void initialize(void);
    virtual void exec(QRunnable *task);
    virtual void spawn(QMap<QString, QString> options = QMap<QString, QString>() );
    virtual void unspawn(void);

    virtual bool noGui(void);

    bool isMaster(void);
    dtkDistributedCommunicator *communicator(void);
    dtkDistributedPolicy *policy(void);

    QCommandLineParser *parser(void);

    static dtkDistributedGuiApplication *create(int& argc, char *argv[]);

private:
    class dtkDistributedApplicationPrivate *d;
};

//
// dtkDistributedGuiApplication.h ends here
