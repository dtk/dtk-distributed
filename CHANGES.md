# Changelog

## version 3.1.0 - 2024-01-23
- switch to dtk-core 3.1
- fix bug in shared communicator
- cmake updates

## version 3.0.0 - 2023-09-21

- update to Qt6
- switch to qt6-main + add osx-arm64
- expand ~ in path (was not working on osx with zsh)
- don't use legacy signal/slot syntax + fix regexp
- handle JOBERROR message
- add deployEnv method
- add absolute path if needed
- raise timeout for oarnodes
- fix: QProcess object declared outside the if block. add ssh options
- don't build dtkDistributedDashboard by default. Only depends on Core
- add 'tunnel' option in wrapper to use ssh tunnel to connect to server
- fix QTcpSocket signal
- try to update server URL when needed
- slurm: fix jobid parsing
- make sure the working dir exists
- add dtkDistributedWrapper app
- add working_directory option in dtkDistributedServer
- only prepend absolute path if option is set in settingz
- send end job message with Local manager
- don't use ssh for localhost + fix argument + display standby_nodes in dashboard
- try to resurrect old dtkDistributedDashboard app + handle empty jobs in
- OAR resource manager
- handle new syntax of oar json output
- add receive method of raw data with status in top level communicator API
- increase timeout for oarnodes
- use startCommand instead of start
- split into subfunctions to make it testable
- use latest openmpi
- Qint32 instead of qlonglong.
- lock/unlock must be done by all workers
- add write method (MetisDirected format) for graphTopology
- add methods to set neighbour_count and vertex_to_edge arrays
- add MPI_DOUBLE_INT type for reduce
- add mpi operations mpi_minloc and mpi_maxloc

## version 2.0.0 - 2021-03-26
  - remove dtkArray
  - include mpi plugin into layer
  - add conda packages
  - cleanup
