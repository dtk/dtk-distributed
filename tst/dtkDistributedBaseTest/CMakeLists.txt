### CMakeLists.txt ---
##

project(dtkDistributedBaseTest
VERSION
  ${dtkDistributed_VERSION}
LANGUAGES
  CXX)

## #################################################################
## Input
## #################################################################

set(${PROJECT_NAME}_HEADERS
  dtkDistributedBaseTest
  dtkDistributedBaseTest.h
  dtkDistributedBaseTestArrayRunnable.h
  dtkDistributedBaseTestBufferManagerRunnable.h
  dtkDistributedBaseTestCommRunnable.h
  dtkDistributedBaseTestContainerRunnable.h
  dtkDistributedBaseTestGraphTopologyRunnable.h
)

## #################################################################
## Build rules
## #################################################################

qt_add_library(${PROJECT_NAME} INTERFACE)

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME} INTERFACE Qt6::Core)
target_link_libraries(${PROJECT_NAME} INTERFACE Qt6::Test)

target_link_libraries(${PROJECT_NAME} INTERFACE dtkDistributed)

target_include_directories(${PROJECT_NAME} INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/..
  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
  $<INSTALL_INTERFACE:include>)

add_library(dtk::DistributedBaseTest ALIAS dtkDistributedBaseTest)

## ###################################################################
## Install rules - files
## ###################################################################

install(
FILES
  ${${PROJECT_NAME}_HEADERS}
DESTINATION
  include/${PROJECT_NAME})

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

install(EXPORT ${PROJECT_NAME}-targets
FILE
  ${PROJECT_NAME}Targets.cmake
DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/dtkDistributed)

export(EXPORT ${PROJECT_NAME}-targets
FILE
  ${CMAKE_BINARY_DIR}/${PROJECT_NAME}Targets.cmake)

######################################################################
### CMakeLists.txt ends here
