#pragma once

#include <QtTest>

#include <dtkDistributed/dtkDistributedApplication>

#define DTKDISTRIBUTEDTEST_MAIN(TestMain, TestObject)   \
    int TestMain(int argc, char *argv[])            \
    {                                               \
        QApplication app(argc, argv);               \
        TestObject tc;                              \
        return QTest::qExec(&tc, argc, argv);       \
    }

#define DTKDISTRIBUTEDTEST_MAIN_GUI(TestMain, TestObject) \
    int TestMain(int argc, char *argv[])              \
    {                                                 \
        QGuiApplication app(argc, argv);              \
        TestObject tc;                                \
        return QTest::qExec(&tc, argc, argv);         \
    }

#define DTKDISTRIBUTEDTEST_MAIN_NOGUI(TestMain, TestObject)	\
    int TestMain(int argc, char *argv[]) {              \
        dtkDistributedApplication app(argc, argv);       \
        TestObject tc;                                  \
        return QTest::qExec(&tc, argc, argv);           \
    }
