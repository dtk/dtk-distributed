// dtkDistributedResourceManagerTest.h
//

#pragma once

#include <QtCore>

class dtkDistributedResourceManagerTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testOar(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

//
// dtkDistributedResourceManagerTest.h ends here
