// dtkDistributedArrayTest.cpp
//

#include "dtkDistributedArrayTest.h"

#include <dtkDistributed>
#include <dtkDistributedBaseTest/dtkDistributedBaseTestArrayRunnable.h>

void dtkDistributedArrayTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

void dtkDistributedArrayTestCase::init(void)
{

}

void dtkDistributedArrayTestCase::testMapper(void)
{

    dtkDistributedMapper mapper;
    qlonglong pu_count = 7;
    mapper.setMapping(16, pu_count);
    qWarning() << mapper;
    for (int i=0; i < pu_count; ++i)  {
        QVERIFY(mapper.count(i) > 0);
    }
}


void dtkDistributedArrayTestCase::testAll(void)
{
    dtkDistributed::communicator_array_test::runAll("qthread");
}

void dtkDistributedArrayTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedArrayTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedArrayTest, dtkDistributedArrayTestCase)

//
// dtkDistributedArrayTest.cpp ends here
