// dtkDistributedSendRecvTest.cpp
//

#include "dtkDistributedSendRecvTest.h"

#include <dtkDistributed>
#include <dtkDistributedBaseTest/dtkDistributedBaseTest.h>
#include <dtkDistributedBaseTest/dtkDistributedBaseTestCommRunnable.h>


void dtkDistributedSendRecvTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
}

void dtkDistributedSendRecvTestCase::init(void)
{

}

void dtkDistributedSendRecvTestCase::testAll(void)
{
    dtkDistributed::communicator_send_test::runAll("qthread");
}

void dtkDistributedSendRecvTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedSendRecvTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedSendRecvTest, dtkDistributedSendRecvTestCase)

//
// dtkDistributedSendRecvTest.cpp ends here
