// dtkDistributedBufferManagerTest.cpp
//

#include "dtkDistributedBufferManagerTest.h"

#include <dtkDistributed>
#include <dtkDistributedBaseTest/dtkDistributedBaseTestBufferManagerRunnable.h>

void dtkDistributedBufferManagerTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();

}

void dtkDistributedBufferManagerTestCase::init(void)
{

}

void dtkDistributedBufferManagerTestCase::testBufferOperationManager(void)
{
    dtkDistributedBufferOperationManager *op_manager = new dtkDistributedBufferOperationManagerTyped<double>();

    int N = 3;
    std::vector<double> source = {3.14159, 1.414, 1.732};
    QVector<double> result = {1, 1, 1};

    op_manager->addAssign((char*)result.data(), source.data(), N);
    for (int i = 0; i < N; ++i) {
        QCOMPARE(result[i] - 1, source[i]);
    }

    result = {0, 0, 0};
    op_manager->subAssign((char*)result.data(), source.data(), N);
    for (int i = 0; i < N; ++i) {
        QCOMPARE(result[i], -source[i]);
    }

    result = {1, 1, 1};
    op_manager->mulAssign((char*)result.data(), source.data(), N);
    for (int i = 0; i < N; ++i) {
        QCOMPARE(result[i], source[i]);
    }

    result = {1, 1, 1};
    op_manager->divAssign((char*)result.data(), source.data(), N);
    for (int i = 0; i < N; ++i) {
        QCOMPARE(result[i], 1./source[i]);
    }

    op_manager->negate(result.data(), source.data(), N);
    for (int i = 0; i < N; ++i) {
        QCOMPARE(result[i], -source[i]);
    }

    op_manager->invert(result.data(), source.data(), N);
    for (int i = 0; i < N; ++i) {
        QCOMPARE(result[i], 1./source[i]);
    }

    double s = 3.14159;
    double r = 1.414;
    double c = 1.414;
    op_manager->compareAndSwap((char*)(&r), &s, &c);
    QCOMPARE(r, s);
}

void dtkDistributedBufferManagerTestCase::testAll(void)
{
    dtkDistributed::communicator_buffermanager_test::runAll("qthread");
}

void dtkDistributedBufferManagerTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedBufferManagerTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedBufferManagerTest, dtkDistributedBufferManagerTestCase)

//
// dtkDistributedBufferManagerTest.cpp ends here
