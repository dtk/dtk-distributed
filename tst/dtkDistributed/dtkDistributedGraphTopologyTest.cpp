//
//

#include "dtkDistributedGraphTopologyTest.h"

#include <dtkDistributed>
#include <dtkDistributedBaseTest>

void dtkDistributedGraphTopologyTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();

}

void dtkDistributedGraphTopologyTestCase::init(void)
{

}
void dtkDistributedGraphTopologyTestCase::testAll(void)
{
    dtkDistributed::communicator_graphtopology_test::runAll("qthread");
}

void dtkDistributedGraphTopologyTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedGraphTopologyTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedGraphTopologyTest, dtkDistributedGraphTopologyTestCase)

//
// dtkDistributedGraphTopologyTest.cpp ends here
