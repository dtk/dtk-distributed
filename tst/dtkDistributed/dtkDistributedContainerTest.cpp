// dtkDistributedContainerTest.cpp
//

#include "dtkDistributedContainerTest.h"

#include <dtkDistributed>
#include <dtkDistributedBaseTest/dtkDistributedBaseTestContainerRunnable.h>

void dtkDistributedContainerTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

void dtkDistributedContainerTestCase::init(void)
{

}

void dtkDistributedContainerTestCase::testAll(void)
{
    dtkDistributed::communicator_container_test::runAll("qthread");
}

void dtkDistributedContainerTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedContainerTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedContainerTest, dtkDistributedContainerTestCase)

//
// dtkDistributedContainerTest.cpp ends here
