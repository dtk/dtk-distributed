// dtkDistributedBufferManagerTest.h
//

#pragma once

#include <QtCore>

class dtkDistributedBufferManagerTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testBufferOperationManager(void);
    void testAll(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

};

//
// dtkDistributedBufferManagerTest.h ends here
