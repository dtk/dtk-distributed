// dtkDistributedContainerTest.h
//

#pragma once

#include <QtCore>

class dtkDistributedContainerTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testAll(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

//
// dtkDistributedContainerTest.h ends here
