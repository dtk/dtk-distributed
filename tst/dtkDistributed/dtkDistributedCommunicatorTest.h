// dtkDistributedCommunicatorTest.h
//

#pragma once

#include <QtCore>

class dtkDistributedCommunicatorTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testPluginManager(void);
    void testPluginFactory(void);
    void testPlugin(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

//
// dtkDistributedCommunicatorTest.h ends here
