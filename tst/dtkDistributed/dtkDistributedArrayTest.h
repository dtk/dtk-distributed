// dtkDistributedArrayTest.h
//

#pragma once

#include <QtCore>

class dtkDistributedArrayTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testMapper(void);
    void testAll(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

};

//
// dtkDistributedArrayTest.h ends here
