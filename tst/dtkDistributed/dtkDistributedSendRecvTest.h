// dtkDistributedSendRecvTest.h
//

#pragma once

#include <QtCore>

class dtkDistributedSendRecvTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testAll(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

//
// dtkDistributedSendRecvTest.h ends here
