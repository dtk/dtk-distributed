// dtkDistributedResourceManagerTest.cpp
//

#include "dtkDistributedResourceManagerTest.h"

#include <dtkDistributed>
#include <dtkDistributed/dtkDistributedResourceManagerOar.h>
#include <dtkDistributedBaseTest/dtkDistributedBaseTest.h>


void dtkDistributedResourceManagerTestCase::initTestCase(void)
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
}

void dtkDistributedResourceManagerTestCase::init(void)
{

}

void dtkDistributedResourceManagerTestCase::testOar(void)
{
    QString file = QFINDTESTDATA("../../tst/resources/oarstat.json");
    QString res;
    {
        QVERIFY(!file.isEmpty());
        QFile f(file);
        QVERIFY(f.open(QFile::ReadOnly | QFile::Text)) ;
        QTextStream in(&f);
        res = in.readAll();
        dtkDistributedResourceManagerOar oar_mgr;
        QVERIFY(oar_mgr.jobStatus(res));

        file = QFINDTESTDATA("../../tst/resources/oarnodes_nef.json");
        QFile f2(file);
        QVERIFY(f2.open(QFile::ReadOnly | QFile::Text)) ;
        QTextStream in2(&f2);
        res = in2.readAll();
        QVERIFY(oar_mgr.nodesStatus(res));
    }
    {
        dtkDistributedResourceManagerOar oar_mgr;
        file = QFINDTESTDATA("../../tst/resources/oarstat_g5k_2.json");
        QVERIFY(!file.isEmpty());
        QFile f4(file);
        QVERIFY(f4.open(QFile::ReadOnly | QFile::Text)) ;
        QTextStream in4(&f4);
        res = in4.readAll();
        QVERIFY(oar_mgr.jobStatus(res));

        file = QFINDTESTDATA("../../tst/resources/oarnodes_g5k.json");
        QFile f3(file);
        QVERIFY(f3.open(QFile::ReadOnly | QFile::Text)) ;
        QTextStream in3(&f3);
        res = in3.readAll();
        QVERIFY(oar_mgr.nodesStatus(res));
    }

}

void dtkDistributedResourceManagerTestCase::cleanupTestCase(void)
{
}

void dtkDistributedResourceManagerTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedResourceManagerTest, dtkDistributedResourceManagerTestCase)

//
// dtkDistributedResourceManagerTest.cpp ends here
