#!/bin/bash

mkdir build
cd build

if [ `uname` = "Darwin" ]; then
  if [ `arch` = "arm64" ]; then
    export QT_HOST_PATH=$PREFIX
  fi
fi

cmake .. \
      -G 'Ninja' \
      -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
      -DDTKDISTRIBUTED_ENABLE_WIDGETS=OFF \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_LIBDIR=lib

cmake --build . --target install
