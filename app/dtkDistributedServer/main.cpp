/* @(#)main.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2014 - Nicolas Niclausse, Inria.
 * Created: 2014/04/25 16:28:19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include <dtkDistributed/dtkDistributedServerDaemon.h>
#include <dtkCore>
#include <dtkLog>

#include <QtCore>

int main(int argc, char **argv)
{
    dtkCoreApplication *application = dtkCoreApplication::create(argc, argv);
    application->setApplicationName("dtkDistributedServer");
    application->setApplicationVersion("1.7.0");
    application->setOrganizationName("inria");
    application->setOrganizationDomain("fr");

    QCommandLineParser *parser = application->parser();

    parser->setApplicationDescription("DTK distributed server application.");
    QCommandLineOption typeOption("type", QCoreApplication::translate("main", "type of server "), "oar| torque| local | slurm");
    parser->addOption(typeOption);
    QCommandLineOption workingDirOption("working_directory", QCoreApplication::translate("main", "working directory "), "dir");
    parser->addOption(workingDirOption);
    int port = 9999;
    QCommandLineOption portOption("p", QCoreApplication::translate("main", "listen port"), QString::number(port));
    parser->addOption(portOption);

    application->initialize();

    if (!parser->isSet(typeOption)) {
        qCritical() << "Error, no type set" ;
        return 1;
    }

    if (parser->isSet(portOption)) {
        port = parser->value(portOption).toInt();
    }

    std::cout << "Starting dtkDistributedServer" << std::endl; // DO NOT REMOVE THIS

    QSettings settings("inria", "dtk");
    settings.beginGroup("server");

    if (settings.contains("log_level"))
        dtkLogger::instance().setLevel(settings.value("log_level").toString());

    settings.endGroup();

    dtkDistributedServerDaemon server(port);
    server.setManager( parser->value(typeOption));

    if (parser->isSet(workingDirOption)) {
        QString wd = parser->value(workingDirOption);
        if (wd.startsWith("~")) {
            wd.replace("~",QDir::homePath());
        }
        server.setWorkingDirectory(wd);
    }

    qDebug() << "server started";

    int status = qApp->exec();

    return status;
}
