

import QtQuick
import QtQuick.Controls

ApplicationWindow {
    title: "DTK Distributed Dashboard"
    visible: true

    minimumHeight: 400
    minimumWidth: 1060
    Dashboard {
        id: dashboard
    }
}
