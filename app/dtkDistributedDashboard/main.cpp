/* main.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2014 - Nicolas Niclausse, Inria.
 * Created: 2014/04/30 09:54:08
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */


#include <dtkDistributed/dtkDistributed.h>
#include <dtkDistributed/dtkDistributedCommunicator.h>
#include <dtkDistributed/dtkDistributedController.h>
#include <dtkDistributed/dtkDistributedPolicy.h>
#include <dtkDistributed/dtkDistributedSettings.h>

#include <dtkLog>
//#include <dtkWidgets/dtkApplication.h>

#include <QtQml>
#include <QtGui>
#include <QtQml/QQmlApplicationEngine>
#include <QtQuick>

int main(int argc, char *argv[])
{

    QGuiApplication app(argc, argv);
    app.setOrganizationName("inria");
    app.setOrganizationDomain("fr");
    app.setApplicationName("dtkDistributedDashboard");

    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");

    // QCommandLineParser *parser = app->parser();
    // parser->setApplicationDescription("DTK Distributed Dashboard. Example application using dtkDistributedController and QML.");

//    app->initialize();

    qmlRegisterType<dtkDistributedController>("dtkDistributed", 1, 0, "DistributedController");
    qmlRegisterType<dtkDistributedPolicy>("dtkDistributed", 1, 0, "DistributedPolicy");

    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();

    QQmlApplicationEngine engine(QUrl("qrc:/dashmain.qml"));
    return app.exec();

}

