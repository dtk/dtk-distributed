function url(check_tunnel)
{
    if (check_tunnel && tunnel.checked) {
        return "http://localhost:" +controller.defaultPort()
    } else {
        return "http://" + combo.currentText + ":" +controller.defaultPort()
    }
}

function getJobIndex(jobid)
{
    for(var j = 0; j < myjobModel.count; j++) {
        if (myjobModel.get(j).id == jobid) {
            return j
        }
    }
}

function policy()
{
    return comboPolicies.currentText
}

function guess_type(server)
{
    console.log("find ", server)
     if (/nef/.test(server)) {
        return "oar"
    } else if (/grid5000/.test(server)) {
        return "oar"
    } else {
        return "local"
    }
}

function setWalltime(hours, min, sec)
{
    return hours + ":" + min +":" + sec ;
}

function submit(nodes, cores, is_dtk_app, walltime)
{
    console.debug("submit " + cores + " nodes " +nodes )
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", url(true) +"/job", true);
    // timeout doesn't work in qml ?
    xhr.timeout = 5;
    xhr.ontimeout = function () { console.debug("Timeout !!!" )}
    xhr.onreadystatechange = function()
    {
        if ( xhr.readyState == xhr.DONE)
        {
            if ( xhr.status == 201)
            {
                var jobid = xhr.getResponseHeader("X-DTK-JobId");
                console.debug("jobid is " + jobid )
                // jobModel.append({"id": jobid,
                //                  "user": "me",
                //                  "nodes" : NaN,
                //                  "cores" : NaN,
                //                  "queue" : "unknown"})

            } else {
                console.debug("bad status " + xhr.status )
            }
        }
    }
    var policy_str = policy();
    var app;
    var is_mpi = false; //FIXME
    if (is_dtk_app) {
        app = slavePath.text +" --nw --server " + url(false) + " --policy " + policy_str
    } else if (is_mpi) {
        app = "mpirun -np " + cores + " " +slavePath.text;
    } else {
        app = slavePath.text;
    }

    console.log("will run " , app)
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var resources = {  "resources": {"nodes": nodes, "cores": cores },
                       "walltime": walltime,
                       "application": app
                    }
    xhr.send(JSON.stringify(resources))
}

function show()
{
    console.debug("show")
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url(true) +"/status",true);
    xhr.onreadystatechange = function()
    {
        if ( xhr.readyState == xhr.DONE)
        {
            if ( xhr.status == 200)
            {
                console.debug("status OK, parse" )
                console.log(xhr.responseText)
                var jsonObject = JSON.parse(xhr.responseText);
                var cores = 0
                var cores_busy = 0
                var cores_down = 0
                var cores_free = 0
                var nodes_free = 0
                var nodes_busy = 0
                var nodes_down = 0
                var nodes_standby = 0
                for ( var index in jsonObject.nodes ) {
                    cores += jsonObject.nodes[index].cores.length;
                    cores_busy += jsonObject.nodes[index].cores_busy;
                    if (jsonObject.nodes[index].state == "free") {
                        nodes_free ++;
                    } else if (jsonObject.nodes[index].state == "down") {
                        nodes_down ++;
                        cores_down += jsonObject.nodes[index].cores.length;
                    } else if (jsonObject.nodes[index].state == "busy") {
                        nodes_busy ++;
                    } else if (jsonObject.nodes[index].state == "standby") {
                        nodes_standby ++;
                    }
                }
                cores_free = cores - cores_busy - cores_down
                cores_pie.chartData = [{value: cores_free, color: "green"},
                                       {value: cores_busy, color: "orange"},
                                       {value: cores_down, color: "red"}];
                coresFree.text = "Free cores: " + cores_free
                coresTotal.text = "Total cores: " + cores
                coresBusy.text = "Busy cores: " + cores_busy
                coresDown.text = "Dead cores: " + cores_down
                nodesFree.text = "Free nodes: " + nodes_free
                nodesDead.text = "Dead nodes: " + nodes_down
                nodesBusy.text = "Busy nodes: " + nodes_busy
                nodesStandby.text = "Standby nodes: " + nodes_standby
                nodesTotal.text = "Total nodes: " + (nodes_free+nodes_busy+nodes_down+ nodes_standby)
                cores_pie.repaint();
                nodes_pie.chartData = [{value: nodes_free, color: "green"},
                                       {value: nodes_busy, color: "orange"},
                                       {value: nodes_standby, color: "cyan"},
                                       {value: nodes_down, color: "red"}];
                nodes_pie.repaint();
                console.debug("cores " + cores )
                console.debug("cores busy " + cores_busy )
                console.debug("nodes free " + nodes_free )
                console.debug("nodes busy " + nodes_busy )
                console.debug("nodes dead " + nodes_down )
                console.debug("nodes standby " + nodes_standby )

                jobModel.clear()
                for ( var index in jsonObject.jobs ) {
                    console.log("append row")
                    console.log(jsonObject.jobs[index].id,  jsonObject.jobs[index].username)
                    jobModel.appendRow({"jobid": jsonObject.jobs[index].id,
                                    "User": jsonObject.jobs[index].username,
                                    "Nodes" : jsonObject.jobs[index].resources.nodes,
                                    "Cores" : jsonObject.jobs[index].resources.cores,
                                    "Queue" : jsonObject.jobs[index].queue })
                }
            } else {
                console.debug("status NOK" + xhr.status)
            }
        }
    }
    xhr.send();
}
