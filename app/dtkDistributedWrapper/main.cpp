/* @(#)main.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2014 - Nicolas Niclausse, Inria.
 * Created: 2014/04/25 16:28:19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */




#include <dtkDistributed>
#include <dtkLog>

#include <QtCore>

int main(int argc, char **argv)
{
    dtkCoreApplication *app = dtkCoreApplication::create(argc, argv);
    app->setApplicationName("dtkDistributedWrapper");
    app->setApplicationVersion("1.5.1");
    app->setOrganizationName("inria");

    QCommandLineParser *parser = app->parser();
    parser->setApplicationDescription("DTK distributed wrapper: it connect to the DTK distributed server  .");

    QCommandLineOption serverOption("server", "DTK distributed server URL", "URL");
    parser->addOption(serverOption);
    QCommandLineOption tunnelOption("tunnel", "Create a ssh tunnel to the server");
    parser->addOption(tunnelOption);
    QCommandLineOption wrapOption("wrap", "Command to be wrapped with arguments ", "command with args");
    parser->addOption(wrapOption);
    QCommandLineOption workingDirOption("working_directory", QCoreApplication::translate("main", "working directory "), "dir");
    parser->addOption(workingDirOption);

    app->initialize();
    if (!parser->isSet(serverOption)) {
        qCritical() << "Error: no server set ! Use --server <url> " ;
        return 1;
    }
    if (!parser->isSet(wrapOption)) {
        qCritical() << "Error: no wrap command set ! Use --wrap <cmd> " ;
        return 1;
    }
    bool ssh_tunnel = parser->isSet(tunnelOption);

    QString server = parser->value(serverOption);
    QString   wrap = parser->value(wrapOption);
    dtkTrace() << "wrap option:" << wrap;
    QStringList rargs = wrap.split(QLatin1Char(' '), Qt::SkipEmptyParts);

    QString cmd = rargs.takeFirst();
    QProcess ssh_proc;
    if (ssh_tunnel) {
        QUrl server_url = QUrl(server);
        QStringList ssh_args;
        dtkDebug() << "ssh port forwarding is set for server " << server_url.host();
        int port = (server_url.port() == -1) ? dtkDistributedController::defaultPort() : server_url.port();
        ssh_args << "-L" << QString::number(port) + ":localhost:" + QString::number(port);
        ssh_args << "-t" << "-t" << "-4" << "-N";
        ssh_args << "-x"; // disable X11 forwarding
        ssh_args << server_url.host();
        dtkDebug() << "using ssh options: " << ssh_args;
        server = "http://localhost:"+QString::number(port);
        ssh_proc.start("ssh", ssh_args);
        if (!ssh_proc.waitForStarted(5000)) {
            dtkError() << "ssh connection not started after 5 seconds, abort  " << ssh_args;
            ssh_proc.close();
            return 1;
        }

    }
    dtkDistributedSlave *slave = new dtkDistributedSlave;
    QThread::sleep(2);

    slave->connectFromJob(server);
    auto jobid = slave->jobId();

    QProcess *stat = new QProcess;
    if (parser->isSet(workingDirOption)) {
        QString wd_path = parser->value(workingDirOption);
        if (wd_path.startsWith("~")) {
            wd_path.replace("~",QDir::homePath());
        }
        if (!QDir(wd_path).exists()) {
            dtkError() << "set working directory: non existing directory ! Abort" << wd_path << jobid;
            dtkDistributedMessage msg(dtkDistributedMessage::ENDJOB, jobid, dtkDistributedMessage::CONTROLLER_RANK);
            msg.send(slave->socket());
            slave->socket()->flush();
            exit(1);
        }
        dtkTrace() << "set working directory" << wd_path ;
        stat->setWorkingDirectory(wd_path);
    }
    if (!QFileInfo(cmd).isAbsolute()) {
        // if cmd is not absolute, check if it's the current directory, which
        // may be not in the path, so add the pathname
        dtkDebug() << "Check if cmd" << cmd << "is in directory" << qApp->applicationDirPath();
        QFileInfo cmd_abs(qApp->applicationDirPath(),cmd);
        if (cmd_abs.isExecutable()) {
            dtkDebug() << "Yes! cmd" << cmd << "is in directory" << qApp->applicationDirPath();
            cmd = cmd_abs.absoluteFilePath();
        }
    }
    dtkTrace() << "starting command"<<  cmd << rargs;
    stat->start(cmd, rargs);
    QObject::connect(stat, &QProcess::readyReadStandardOutput,  [=] (void) -> void
    {
        QString data = stat->readAll();
        QVariant v(data);
        dtkDistributedMessage msg(dtkDistributedMessage::DATA, jobid, dtkDistributedMessage::CONTROLLER_RANK,v);
        msg.send(slave->socket());
        slave->socket()->flush();
        dtkTrace() <<  data;
    });
    QObject::connect(stat, &QProcess::readyReadStandardError,  [=] (void) -> void
    {
        dtkWarn() <<  stat->readAll();
    });
    QObject::connect(stat, &QProcess::errorOccurred, [=] (QProcess::ProcessError error) {
        dtkWarn() << "JOB error" << jobid << error;
        dtkDistributedMessage msg(dtkDistributedMessage::ENDJOB, jobid, dtkDistributedMessage::CONTROLLER_RANK);
        msg.send(slave->socket());
        slave->socket()->flush();
        exit(1);
    });
    QObject::connect(stat, &QProcess::finished, [=] (int exitCode, QProcess::ExitStatus exitStatus) {
        dtkWarn() << "JOB finished" << jobid << exitCode;
        dtkDistributedMessage msg(dtkDistributedMessage::ENDJOB, jobid, dtkDistributedMessage::CONTROLLER_RANK);
        msg.send(slave->socket());
        slave->socket()->flush();
        exit(exitCode);
    });
    QObject::connect(qApp, &QCoreApplication::aboutToQuit, [=] (void) {
            dtkInfo() << Q_FUNC_INFO << "about to quit";
            dtkDistributedMessage msg(dtkDistributedMessage::ENDJOB, jobid, dtkDistributedMessage::CONTROLLER_RANK);
            msg.send(slave->socket());
            slave->socket()->flush();
        });

    int status = qApp->exec();

    delete slave;
    return 0;
}
