/* dtkDistributedContainerTest.cpp --- 
 * 
 * Author: Thibaud Kloczko
 * Created: 2013 Mon Feb  4 15:37:54 (+0100)
 */

/* Commentary: 
 * 
 */

/* Change log:
 * 
 */

#include "dtkDistributedSendRecvTest.h"

#include <dtkDistributedBaseTest>

void dtkDistributedSendRecvTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

void dtkDistributedSendRecvTestCase::init(void)
{

}

void dtkDistributedSendRecvTestCase::testAll(void)
{
    dtkDistributed::communicator_send_test::runAll("mpi");
}

void dtkDistributedSendRecvTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedSendRecvTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedSendRecvTest, dtkDistributedSendRecvTestCase)
