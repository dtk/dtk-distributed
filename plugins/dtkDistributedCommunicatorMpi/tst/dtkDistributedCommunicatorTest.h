/* dtkDistributedCommunicatorTest.h --- 
 * 
 * Author: Julien Wintz
 * Created: Mon Feb  4 13:58:16 2013 (+0100)
 * Version: 
 * Last-Updated: mer. avril 17 12:57:50 2013 (+0200)
 *           By: Nicolas Niclausse
 *     Update #: 32
 */

/* Change Log:
 * 
 */

#pragma once

#include <QTest>

class dtkDistributedCommunicatorTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testPluginManager(void);
    void testPluginFactory(void);
    void testPlugin(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};
