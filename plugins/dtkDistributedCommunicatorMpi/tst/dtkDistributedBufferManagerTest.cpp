// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "dtkDistributedBufferManagerTest.h"

#include <dtkDistributedBaseTest>

void dtkDistributedBufferManagerTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    dtkDistributed::communicator::pluginManager().setVerboseLoading(true);
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
    settings.endGroup();
}

void dtkDistributedBufferManagerTestCase::init(void)
{

}

void dtkDistributedBufferManagerTestCase::testAll(void)
{
    dtkDistributed::communicator_buffermanager_test::runAll("mpi");
}

void dtkDistributedBufferManagerTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedBufferManagerTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedBufferManagerTest, dtkDistributedBufferManagerTestCase)

// 
// dtkDistributedBufferManagerTest.cpp ends here
