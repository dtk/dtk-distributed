// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "dtkDistributedArrayTest.h"

#include <dtkDistributed>
#include <dtkDistributedBaseTest>

void dtkDistributedArrayTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

void dtkDistributedArrayTestCase::init(void)
{

}

void dtkDistributedArrayTestCase::testAll(void)
{
    dtkDistributed::communicator_array_test::runAll("mpi");
}

void dtkDistributedArrayTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedArrayTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedArrayTest, dtkDistributedArrayTestCase)

// 
// dtkDistributedArrayTest.cpp ends here
