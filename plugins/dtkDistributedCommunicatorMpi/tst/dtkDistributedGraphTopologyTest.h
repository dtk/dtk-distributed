// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <QTest>

class dtkDistributedGraphTopologyTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testAll(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

};

// 
// dtkDistributedGraphTopologyTest.h ends here
