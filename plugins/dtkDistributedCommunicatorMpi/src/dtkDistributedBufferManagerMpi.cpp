// dtkDistributedBufferManagerMpi.cpp
//

#include "dtkDistributedBufferManagerMpi.h"

#include <dtkDistributed/dtkDistributedCommunicator>

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferManagerMpiPrivate
// ///////////////////////////////////////////////////////////////////

class dtkDistributedBufferManagerMpiPrivate
{
public:
    dtkDistributedCommunicator *comm;
    int wid;
    int comm_size;

public:
    qlonglong object_size;
    MPI_Datatype object_mpi_type;
    QVarLengthArray<char *> buffers;
    QVarLengthArray<bool> rlocked;
    QVarLengthArray<bool> wlocked;

public:
    MPI_Win win;
    MPI_Info alloc_shared_info;

 public:
    MPI_Comm comm_shared;
    MPI_Win win_shared;
    QHash<int, int> shared_map; // maps the rank of each process in the global communicator to the corresponding rank in the shared communicator.

public:
    MPI_Datatype mpiDataType(int metatype_id);
};

MPI_Datatype dtkDistributedBufferManagerMpiPrivate::mpiDataType(int metatype_id)
{
    switch(metatype_id) {
    case QMetaType::Char:     return MPI_CHAR;
    case QMetaType::Int:      return MPI_INT;
    case QMetaType::Long:     return MPI_LONG;
    case QMetaType::LongLong: return MPI_LONG_LONG;
    case QMetaType::Float:    return MPI_FLOAT;
    case QMetaType::Double:   return MPI_DOUBLE;
    default:                  return MPI_BYTE;
    }
}

// /////////////////////////////////////////////////////////////////////////////
// dtkDistributedBufferManagerMpi implementation
// /////////////////////////////////////////////////////////////////////////////


dtkDistributedBufferManagerMpi::dtkDistributedBufferManagerMpi(dtkDistributedCommunicator *comm, MPI_Comm comm_shared, const QHash<int, int>& shared_map) : d(new dtkDistributedBufferManagerMpiPrivate)
{
    d->comm = comm;
    d->comm_size = comm->size();
    d->wid = comm->wid();
    d->rlocked.resize(d->comm_size);
    d->wlocked.resize(d->comm_size);
    d->buffers.resize(d->comm_size);
    for(qlonglong i = 0; i < d->comm_size; ++i) {
        d->rlocked[i] = false;
        d->wlocked[i] = false;
        d->buffers[i] = nullptr;
    }

    d->comm_shared = comm_shared;
    d->shared_map = shared_map;

}

dtkDistributedBufferManagerMpi::~dtkDistributedBufferManagerMpi(void)
{
    if (d->buffers[d->wid]) {
        this->deallocate(d->buffers[d->wid], d->object_size);
    }

    delete d;
    d = nullptr;
}

bool dtkDistributedBufferManagerMpi::shouldCache(qint32 owner)
{
    // we should cache only remote processes
    return (d->buffers[owner] == 0);
}

void dtkDistributedBufferManagerMpi::rlock(qint32 wid)
{
    // we can't rlock mpi because each 'get' needs to be synchronous, and
    // therefore requires a MPI_Lock/unlock to achieve the RMA
    // transfert
    /* MPI_Win_lock(MPI_LOCK_SHARED, wid, MPI_MODE_NOCHECK, d->win); */
    // however, we still set locked to true in order to optimize shared buffer access
    d->rlocked[wid] = true;
}

void dtkDistributedBufferManagerMpi::rlock(void)
{
    // we can't rlock mpi because each 'get' needs to be synchronous, and
    // therefore requires a MPI_Lock/unlock to achieve the RMA
    // transfert
    /* MPI_Win_lock(MPI_LOCK_SHARED, wid, MPI_MODE_NOCHECK, d->win); */
    // however, we still set locked to true in order to optimize shared buffer access
    for(qint32 i = 0; i < d->comm_size; ++i) {
        d->rlocked[i] = true;
    }
}

void dtkDistributedBufferManagerMpi::wlock(qint32 wid)
{
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, wid, 0, d->win);
    d->wlocked[wid] = true;
}

void dtkDistributedBufferManagerMpi::wlock(void)
{
    /* can't use MPI_win_lock_all since it is only MPI_LOCK_SHARED */
    /* MPI_Win_lock_all(MPI_MODE_NOCHECK, d->win); */

    for(qint32 i = 0; i < d->comm_size; ++i) {
        bool res = MPI_Win_lock(MPI_LOCK_EXCLUSIVE, i, 0, d->win);
        if (res != MPI_SUCCESS) {
            dtkError() << "Error while locking window belonging to rank" << i << ", our rank is" << d->wid;
        } else {
            d->wlocked[i] = true;
        }
    }
}

void dtkDistributedBufferManagerMpi::unlock(qint32 wid)
{
    if (d->wlocked[wid])
        MPI_Win_unlock(wid, d->win);
    d->wlocked[wid] = false;
    d->rlocked[wid] = false;
}

void dtkDistributedBufferManagerMpi::unlock(void)
{
    for(qint32 i = 0; i < d->comm_size; ++i) {
        if (d->wlocked[i]) {
            bool res = MPI_Win_unlock(i, d->win);
            if (res != MPI_SUCCESS) {
                dtkError() << "Error while unlocking window belonging to rank" << i << ", our rank is " << d->wid;
            }
        }
        d->wlocked[i] = false;
        d->rlocked[i] = false;
    }
}

 bool dtkDistributedBufferManagerMpi::locked(qint32 wid)
{
    return (d->wlocked[wid] || d->rlocked[wid]);
}

 void dtkDistributedBufferManagerMpi::get(qint32 from, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((from >= 0 || from < d->comm->size()));

    char *buffer = d->buffers[from];
    auto array_size = d->object_size * nelements;
    bool locked = d->rlocked[from] || d->wlocked[from];

    if (buffer) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_SHARED, from,  0, d->win);
            memcpy(array, buffer + position * d->object_size, array_size);
            MPI_Win_unlock(from, d->win);

        } else {
            memcpy(array, buffer + position * d->object_size, array_size);
        }

    } else {
        MPI_Win_lock(MPI_LOCK_SHARED, from,  0, d->win);
        MPI_Get(array, array_size, MPI_BYTE, from, position, array_size, MPI_BYTE, d->win);
        MPI_Win_unlock(from, d->win);
    }
}

 void dtkDistributedBufferManagerMpi::put(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffers[dest];
    auto array_size = d->object_size * nelements;
    bool locked = d->wlocked[dest];

    if (buffer) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            memcpy(buffer + position * d->object_size, array, array_size);
            MPI_Win_unlock(dest, d->win);
            //MPI_Put(array, array_size, MPI_BYTE, dest, position, array_size, MPI_BYTE, d->win_shared);
            //MPI_Win_flush_local(dest, d->win_shared);

        } else {
            memcpy(buffer + position * d->object_size, array, array_size);
        }
    } else {
        if (!locked) {
            /* MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, MPI_MODE_NOCHECK, d->win); */
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Put(array, array_size, MPI_BYTE, dest, position, array_size, MPI_BYTE, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Put(array, array_size, MPI_BYTE, dest, position, array_size, MPI_BYTE, d->win);
        }
    }
}

void dtkDistributedBufferManagerMpi::addAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffers[dest];
    bool locked = d->wlocked[dest];

    if (buffer) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            this->operation_manager->addAssign(buffer + position * d->object_size, array, nelements);
        }
    } else {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
        }
    }
}

 void dtkDistributedBufferManagerMpi::subAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffers[dest];
    bool locked = d->wlocked[dest];

    if (buffer) {
        if (!locked) {
            void *sub_array = std::malloc(nelements * d->object_size);
            this->operation_manager->negate(sub_array, array, nelements);
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(sub_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
            MPI_Win_unlock(dest, d->win);
            std::free(sub_array);

        } else {
            this->operation_manager->subAssign(buffer + position * d->object_size, array, nelements);
        }
    } else {
        void *sub_array = std::malloc(nelements * d->object_size);
        this->operation_manager->negate(sub_array, array, nelements);
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(sub_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Accumulate(sub_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
        }
        std::free(sub_array);
    }
}

void dtkDistributedBufferManagerMpi::mulAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffers[dest];
    bool locked = d->wlocked[dest];

    if (buffer) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            this->operation_manager->mulAssign(buffer + position * d->object_size, array, nelements);
        }
    } else {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
        }
    }
}

 void dtkDistributedBufferManagerMpi::divAssign(qint32 dest, std::size_t position, void *array, std::size_t nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffers[dest];
    bool locked = d->wlocked[dest];

    if (buffer) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            void *div_array = std::malloc(nelements * d->object_size);
            this->operation_manager->invert(div_array, array, nelements);
            MPI_Accumulate(div_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
            MPI_Win_unlock(dest, d->win);
            std::free(div_array);
        } else {
            this->operation_manager->divAssign(buffer + position * d->object_size, array, nelements);
        }
    } else {
        void *div_array = std::malloc(nelements * d->object_size);
        this->operation_manager->invert(div_array, array, nelements);
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(div_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Accumulate(div_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
        }
        std::free(div_array);
    }
}

bool dtkDistributedBufferManagerMpi::compareAndSwap(qint32 dest, std::size_t position, void *array, void *compare)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));
    bool locked = d->wlocked[dest];

    void *result ;
    bool res;
    int mpi_res = 0;
    result = std::malloc(d->object_size);
    if (!locked) {
      MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
      mpi_res = MPI_Compare_and_swap (array, compare, result, d->object_mpi_type, dest, position, d->win);
      MPI_Win_unlock(dest, d->win);
      res = (memcmp(result, compare, d->object_size) == 0 );
    } else {
      mpi_res = MPI_Compare_and_swap (array, compare, result, d->object_mpi_type, dest, position, d->win);
      MPI_Win_unlock(dest, d->win);
      res = (memcmp(result, compare, d->object_size) == 0 );
      MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
    }
    std::free(result);
    return (mpi_res == 0 && res);
}

void *dtkDistributedBufferManagerMpi::allocate(std::size_t objectSize, std::size_t capacity, int metatype_id)
{
    d->object_mpi_type = d->mpiDataType(metatype_id);

    if (capacity == 0)
        return nullptr;

    d->object_size = objectSize;
    std::size_t buffer_size = capacity * objectSize;
    MPI_Info_create(&d->alloc_shared_info);
    MPI_Info_set(d->alloc_shared_info, "alloc_shared_noncontig", "true");

    MPI_Win_allocate_shared(buffer_size, objectSize, d->alloc_shared_info, d->comm_shared, &(d->buffers[d->wid]), &(d->win_shared));

    int size_tmp;
    MPI_Aint count_tmp;
    for(int i = 0; i < d->shared_map.count(); ++i) {
        if (d->shared_map[i] > -1) {
            MPI_Win_shared_query(d->win_shared, d->shared_map[i], &count_tmp, &size_tmp, &(d->buffers[i]));
        }
    }

    MPI_Win_create(d->buffers[d->wid], buffer_size, objectSize, MPI_INFO_NULL, MPI_COMM_WORLD, &(d->win));

    return d->buffers[d->wid];
}

void dtkDistributedBufferManagerMpi::deallocate(void *buffer, std::size_t objectSize)
{
    Q_ASSERT(d->object_size == objectSize);
    Q_ASSERT(buffer == d->buffers[d->wid]);
    Q_UNUSED(objectSize);
    Q_UNUSED(buffer);
    if (!buffer)
        return;

    MPI_Win_free(&d->win);
    MPI_Win_free(&d->win_shared);
    for(qint32 i = 0; i < d->comm_size; ++i) {
        d->buffers[i] = nullptr;
    }
    MPI_Info_free(&d->alloc_shared_info);
}

//
// dtkDistributedBufferManagerMpi.cpp ends here
