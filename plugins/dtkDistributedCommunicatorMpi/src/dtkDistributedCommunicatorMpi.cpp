// dtkDistributedCommunicatorMpi.cpp
// Author: Nicolas Niclausse
// Created: 2013/02/11 09:47:55
//

#include "dtkDistributedCommunicatorMpi.h"
#include "dtkDistributedBufferManagerMpi.h"

#include <dtkDistributed/dtkDistributedCommunicatorStatus>
#include <dtkDistributed/dtkDistributedRequest>
#include <dtkDistributed/dtkDistributedDoubleInt>

#include <dtkLog/dtkLogger.h>

#include <QtCore>

#include <mpi.h>

#include <unistd.h>

// /////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////

MPI_Op operation_type(dtkDistributedCommunicator::OperationType type)
{
    switch(type) {
    case dtkDistributedCommunicator::Min:        return MPI_MIN;
    case dtkDistributedCommunicator::Max:        return MPI_MAX;
    case dtkDistributedCommunicator::MinLoc:     return MPI_MINLOC;
    case dtkDistributedCommunicator::MaxLoc:     return MPI_MAXLOC;
    case dtkDistributedCommunicator::Sum:        return MPI_SUM;
    case dtkDistributedCommunicator::Product:    return MPI_PROD;
    case dtkDistributedCommunicator::BitwiseAnd: return MPI_BAND;
    case dtkDistributedCommunicator::BitwiseOr:  return MPI_BOR;
    case dtkDistributedCommunicator::BitwiseXor: return MPI_BXOR;
    case dtkDistributedCommunicator::LogicalAnd: return MPI_LAND;
    case dtkDistributedCommunicator::LogicalOr:  return MPI_LOR;
    case dtkDistributedCommunicator::LogicalXor: return MPI_LXOR;
    default:
        dtkInfo() << "mpiDistributedCommunicator: operation type not handled.";
        return MPI_MIN;
    }
}

// /////////////////////////////////////////////////////////////////
// dtkDistributedRequestMpi
// /////////////////////////////////////////////////////////////////

class dtkDistributedRequestMpi: public dtkDistributedRequest
{
public:
    MPI_Request m_request;
};

// /////////////////////////////////////////////////////////////////
// dtkDistributedCommunicatorMpiPrivate
// /////////////////////////////////////////////////////////////////

class dtkDistributedCommunicatorMpiPrivate
{
public:
    bool nodesFromScheduler(void);
    void setSharedCommunicator(qint32 rank, int size);
    MPI_Datatype mpiDataType(int metatype_id);

public:
    int wid;
    qlonglong id;
    qlonglong nspawn;        // number of time spawned was called
    qlonglong nspawn_parent; // number of time spawned was called by the parent

public:
    char **argv;

public:
    MPI_Comm comm;

public:
    bool uninitialized;
    bool spawned;
    bool no_spawn;
    QString multithread;
    int  size_shared;

public:
    MPI_Comm comm_shared;
    QHash <int, int> shared_map;
};

// /////////////////////////////////////////////////////////////////

void dtkDistributedCommunicatorMpiPrivate::setSharedCommunicator(qint32 wid, int size)
{
    if (size_shared < 1) {
        // fixme: rank instead of 0 ?
        MPI_Comm_split_type(comm, MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, &comm_shared);
        MPI_Comm_size(comm_shared, &size_shared);

        if (wid == 0 && size_shared == 1) {
            dtkWarn() << "shared communicator is only of size one" ;
        }
        dtkDebug() << "size of shared communicator:" << size_shared;
        MPI_Group world_group;
        MPI_Group shared_group;
        MPI_Comm_group( comm, &world_group );
        MPI_Comm_group( comm_shared, &shared_group );

        int *ranks     = new int[size_shared];
        int *ranks_out = new int[size_shared];

        for (int rank = 0; rank < size; ++rank) {
            shared_map.insert(rank, -1);
        }

        for (int i=0; i<size_shared; ++i)
            ranks[i] = i;

        MPI_Group_translate_ranks( shared_group, size_shared, ranks, world_group, ranks_out );

        // fill map  global rank -> shared rank
        for (int i=0; i<size_shared; ++i) {
            dtkTrace() << wid << "rank" << ranks_out[i] << "is local";
            shared_map.insert(ranks_out[i], i);
        }
    }
}

bool dtkDistributedCommunicatorMpiPrivate::nodesFromScheduler(void)
{
    QByteArray useTorque  = qgetenv("PBS_JOBID");
    QByteArray useOAR     = qgetenv("OAR_JOBID");
    QByteArray useMVAPICH = qgetenv("MV2_COMM_WORLD_SIZE");
    QByteArray useMPICH   = qgetenv("PMI_RANK");
    QByteArray useDtkTest = qgetenv("DTK_TEST");
    // kludge:
    // In the case of OAR, only openmpi can detect OAR ressources (if
    // the OAR RAS patch is applied), but since the master mpi is not
    // started through mpirun, it's difficult to detect it; so we
    // check if MPICH2 or MVAPICH2 is used; if not, we assume openmpi
    // is used.
    // When using dtkTest, define a DTK_TEST environment to disable the use of add-host
    // otherwise openmpi doesn't allow oversubscribing

    return (!useTorque.isEmpty() || !useDtkTest.isEmpty() ||  ( !useOAR.isEmpty() && useMVAPICH.isEmpty()  && useMPICH.isEmpty() ));
}

MPI_Datatype dtkDistributedCommunicatorMpiPrivate::mpiDataType(int metatype_id)
{
    switch(metatype_id) {
    case QMetaType::Char:     return MPI_CHAR;
    case QMetaType::Int:      return MPI_INT;
    case QMetaType::Long:     return MPI_LONG;
    case QMetaType::LongLong: return MPI_LONG_LONG;
    case QMetaType::Float:    return MPI_FLOAT;
    case QMetaType::Double:   return MPI_DOUBLE;
    default:
        if (metatype_id == qMetaTypeId<dtkDistributedDoubleInt>()) {
            return MPI_DOUBLE_INT;
        }
        return MPI_BYTE;
    }
}

// /////////////////////////////////////////////////////////////////
// dtkDistributedCommunicatorMpi
// /////////////////////////////////////////////////////////////////


dtkDistributedCommunicatorMpi::dtkDistributedCommunicatorMpi(void) : dtkDistributedCommunicator(), d(new dtkDistributedCommunicatorMpiPrivate)
{
    d->wid           = -1;
    d->id            = 0;
    d->size_shared   = 0;
    d->argv          = nullptr;
    d->uninitialized = false;
    d->spawned       = false;
    d->no_spawn      = false;
    d->nspawn        = 0;
    d->nspawn_parent = 0;
    d->comm          = MPI_COMM_WORLD;
}

dtkDistributedCommunicatorMpi::~dtkDistributedCommunicatorMpi(void)
{
    MPI_Finalize();

    if (d->argv) {
        int i = 0;
        while (d->argv[i]) {
            delete d->argv[i];
            i++;
        }
        delete d->argv;
    }

    delete d;

    d = nullptr;
}

void dtkDistributedCommunicatorMpi::spawn(QStringList hostnames, QString wrapper, QMap<QString, QString> options)
{
    d->nspawn++;

    if (options.contains("smp")) {
        d->multithread = options.value("smp");
        dtkDebug() << "smp option: set multithread to " << d->multithread;
    }
    this->initialize();

    qlonglong np = hostnames.count();
    if (np == 1 && hostnames.first() == "nospawn") {
        d->no_spawn = true;
        if (wid() == 0) {
            dtkInfo() << "spawned disabled, only initialize";
        }
        d->setSharedCommunicator(wid(), size());
        return;
    }

    MPI_Comm parentcomm, intercomm;
    MPI_Comm_get_parent(&parentcomm);
    QStringList args = qApp->arguments();

    if (parentcomm == MPI_COMM_NULL) {
        dtkDebug() << "I'm the parent" << args;

        QByteArray   appname = qApp->applicationFilePath().toLocal8Bit();
        QStringList wraplist = wrapper.split(QRegularExpression("\\s+"));
        int wrap_count = 0 ;
        int argc = args.count();
        if (wrapper.length() > 0) {
            appname = wraplist.takeFirst().toLocal8Bit();
            wraplist << qApp->applicationFilePath();
            wrap_count = wraplist.count();
            args = wraplist + args;
            argc +=  wrap_count ;
        }
        char **argv = (char**)malloc(sizeof(char*)*(argc+1));
        for (int i = 0 ; i < wrap_count; ++i){
            QByteArray tmp = wraplist[i].toLocal8Bit();
            argv[i] = strdup(tmp.data());
        }

        for (int i = wrap_count; i < argc; ++i){
            QByteArray tmp = args[i].toLocal8Bit();
            argv[i] = strdup(tmp.data());
        }
        argv[argc] = nullptr;
        int errs[np];
        MPI_Info info;
        MPI_Info_create(&info );

        if (!d->nodesFromScheduler()) {
            // Not in torque context, we must provide hostnames
            dtkDebug() << "MPI spawn: add hostnames via MPI_info";
            for (QString host: hostnames) {
                QByteArray file = host.toLocal8Bit();
                MPI_Info_set(info, const_cast<char*>("add-host"),file.data());
            }
        } else {
            dtkDebug() << "MPI spawn: the scheduler will discover hosts by itself, don't add them in MPI_info";
            // if (d->multithread)
            //     MPI_Info_set(info, const_cast<char*>("map_bynode"),const_cast<char*>("true"));
        }

        QByteArray wdir = QDir::currentPath().toLocal8Bit();
        MPI_Info_set(info, const_cast<char*>("wdir"), wdir.data());

        dtkDebug() << "MPI spawn:" << appname << hostnames << np << qApp->applicationDirPath() << "args" << argv[0];

        MPI_Comm_spawn( appname.data(), argv ,np, info, 0, d->comm, &intercomm, errs );

        MPI_Request request;
        MPI_Status status;

        MPI_Bcast(&(d->nspawn), 1, MPI_LONG_LONG, MPI_ROOT, intercomm);

        dtkDebug() << "spawner: wait for childs to end";
        // openmpi is eating 100% cpu while blocking on barrier (aggressive mode)
        // instead, do a irecv and sleep 1s before each MPI_Test
        int fake = 0;
        MPI_Irecv(&fake, 1, MPI_INT, MPI_ANY_SOURCE, 0, intercomm,  &request);
        int flag = 0;
        while ( !flag ) {
            sleep(1);
            MPI_Test(&request, &flag, &status);
        }
        MPI_Barrier(intercomm);

        sleep(1);
        dtkDebug() << "finalize & exit spawner";
        this->uninitialize();

    } else {
        dtkDebug() << "I'm a spawned process";
        d->spawned = true;
        if (d->nspawn == 1) {
            MPI_Bcast(&(d->nspawn_parent), 1, MPI_LONG_LONG, 0, parentcomm);
        }
        d->setSharedCommunicator(wid(), size());
    }
}

void dtkDistributedCommunicatorMpi::unspawn(void)
{
    if (active()) {
        this->uninitialize();
        exit(0);
    }

    d->uninitialized = false;

}

// return true if the communicator is participatin to the current work
// this is not the case of the initial spawner and depends on the
// current work for others
bool dtkDistributedCommunicatorMpi::active(void)
{
    return (d->no_spawn ||  (d->spawned && (d->nspawn == d->nspawn_parent)));
}

void dtkDistributedCommunicatorMpi::exec(QRunnable *work)
{
    if (active()) {
        barrier();
        work->run();
        barrier();
    }
}

void dtkDistributedCommunicatorMpi::initialize(void)
{
    if (initialized())
        return;

    QStringList args = qApp->arguments();
    int    argc = args.size();

    dtkDebug() << "args:" << args;

    if (!d->argv && argc > 0) {
        d->argv = new char*[argc + 1];
        for (int i = 0; i < argc; i++) {
            d->argv[i] = new char[strlen(args.at(i).toStdString().c_str())+1];
            memcpy(d->argv[i], args.at(i).toStdString().c_str(), strlen(args.at(i).toStdString().c_str())+1);
        }
        d->argv[argc] = nullptr;
    }

    if (!d->multithread.isEmpty()) {
        int provided;
        int required = MPI_THREAD_MULTIPLE;
        if (d->multithread == "serialized") {
            required = MPI_THREAD_SERIALIZED;
        } else if (d->multithread == "funneled") {
            required = MPI_THREAD_FUNNELED;
        } else if (d->multithread == "single") {
            required = MPI_THREAD_SINGLE;
        }

        dtkTrace() << "Init Thread" ;
        int status =  MPI_Init_thread( &argc, &d->argv, required, &provided );

        if (status != MPI_SUCCESS) {
            qCritical() << "Error while initializing MPI communicator ";
        }
        if (provided < required)
            qWarning() << "WARNING, MPI thread support provided is too low" << provided;
    } else {
        dtkDebug() << "Calling MPI Init";
        MPI_Init(&argc, &d->argv);
    }

}

bool dtkDistributedCommunicatorMpi::initialized(void)
{
    int initialized;
    MPI_Initialized(&initialized);
    return initialized;
}


void dtkDistributedCommunicatorMpi::uninitialize(void)
{
    dtkDebug() << "uninitialize" << this->rank();
    if (!d->uninitialized){
        if (d->no_spawn) {
            MPI_Finalize();
            d->uninitialized = true;
            return;
        }
        if (d->spawned) {
            MPI_Comm parentcomm;
            MPI_Comm_get_parent(&parentcomm);
            if (this->wid() == 0) {
                int fake = 0;
                MPI_Send(&fake, 1, MPI_INT, 0, 0, parentcomm);
            }
            MPI_Barrier(parentcomm);
            MPI_Comm_disconnect(&parentcomm);
            MPI_Finalize();
        }
    }
    d->uninitialized = true;
}

void dtkDistributedCommunicatorMpi::barrier(void)
{
    MPI_Barrier(d->comm);
}

qint32 dtkDistributedCommunicatorMpi::wid(void)
{
    if (d->wid < 0) {
        MPI_Comm_rank(d->comm, &d->wid);
    }
    return d->wid;
}

int dtkDistributedCommunicatorMpi::size(void)
{
    int size;
    MPI_Comm_size(d->comm, &size);
    return size;
}

void *dtkDistributedCommunicatorMpi::data(void)
{
    return &d->comm;
}

dtkDistributedBufferManager *dtkDistributedCommunicatorMpi::createBufferManager(void)
{
    return new dtkDistributedBufferManagerMpi(this, d->comm_shared, d->shared_map);
}

void dtkDistributedCommunicatorMpi::destroyBufferManager(dtkDistributedBufferManager *& buffer_manager)
{
    delete buffer_manager;
    buffer_manager = nullptr;
}

void dtkDistributedCommunicatorMpi::send(void *data, std::size_t size, QMetaType::Type dataType, qint32 target, qint32 tag)
{
    MPI_Send(data, size, d->mpiDataType(dataType), target, tag, d->comm);
}

void dtkDistributedCommunicatorMpi::send(QByteArray &array, qint32 target, qint32 tag)
{
    qint64   arrayLength = array.length();
    dtkDistributedCommunicator::send(array.data(), arrayLength, target, tag);

}

// void dtkDistributedCommunicatorMpi::send(QVariant &v, qint32 target, qint32 tag)
// {
//     QByteArray array;
//     QDataStream s(array, QIODevice::WriteOnly);
//     s << v;
//     dtkDistributedCommunicator::send(array, target, tag);

// }

void dtkDistributedCommunicatorMpi::broadcast(void *data,  std::size_t size, QMetaType::Type dataType, qint32 source)
{
    MPI_Bcast(data, size, d->mpiDataType(dataType), source, d->comm);
}

void dtkDistributedCommunicatorMpi::broadcast(QVariant &v, qint32 source)
{
    QByteArray array;

    if (wid() == source) {
        QDataStream s(&array, QIODevice::WriteOnly);
        s << v;
        broadcast(array, source) ;
    } else {
        broadcast(array, source) ;
        QDataStream s(&array, QIODevice::ReadOnly);
        s >> v;
    }
}

void dtkDistributedCommunicatorMpi::broadcast(QByteArray &array, qint32 source)
{
    int size;
    if (wid() == source) {
        size = array.size();
    }
    MPI_Bcast(&size, 1, d->mpiDataType(QMetaType::Int), source, d->comm);
    if (wid() != source)
        array.resize(size);
    MPI_Bcast(array.data(), size, d->mpiDataType(QMetaType::Char), source, d->comm);
}
void dtkDistributedCommunicatorMpi::receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag)
{
    MPI_Recv(data, size, d->mpiDataType(dataType), source, tag, d->comm, MPI_STATUS_IGNORE);
}

void dtkDistributedCommunicatorMpi::receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status)
{
    MPI_Status mpi_status;
    MPI_Recv(data, size, d->mpiDataType(dataType), source, tag, d->comm, &mpi_status );
    int count;
    MPI_Get_count(&mpi_status, d->mpiDataType(dataType), &count);
    status.setCount( count );
    status.setTag(mpi_status.MPI_TAG);
    status.setSource(mpi_status.MPI_SOURCE);
    status.setError(mpi_status.MPI_ERROR);
}

void dtkDistributedCommunicatorMpi::receive(QByteArray &array, qint32 source, qint32 tag)
{
    dtkDistributedCommunicatorStatus status;
    this->receive(array, source, tag, status);
}

void dtkDistributedCommunicatorMpi::receive(QByteArray &array, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status )
{
    MPI_Status mpi_status;
    MPI_Probe(source, tag, d->comm, &mpi_status);
    int count;
    MPI_Get_count(&mpi_status,MPI_CHAR, &count);
    status.setCount(count);
    status.setTag(mpi_status.MPI_TAG);
    status.setSource(mpi_status.MPI_SOURCE);
    status.setError(mpi_status.MPI_ERROR);
//    dtkTrace() << "probe mpi: count/source/tag : " << status.count() << status.source() << status.tag();
    array.resize(count);
    dtkDistributedCommunicator::receive(array.data(), count, source, tag);

}

// void dtkDistributedCommunicatorMpi::receive(QVariant &v, qint32 source, qint32 tag)
// {
//     QByteArray array;
//     this->receive(array, source, tag);

//     QDataStream s(array, QIODevice::ReadOnly);
//     s >> v;
// }

// void dtkDistributedCommunicatorMpi::receive(QVariant &v, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status )
// {
//     QByteArray array;
//     this->receive(array, source, tag, status);

//     QDataStream s(array, QIODevice::ReadOnly);
//     s >> v;
// }

dtkDistributedRequest *dtkDistributedCommunicatorMpi::ireceive(void   *data, std::size_t size, QMetaType::Type dataType, qint32 source, int tag)
{
    dtkDistributedRequestMpi *mpi_req = new dtkDistributedRequestMpi;
    MPI_Irecv(data, size, d->mpiDataType(dataType), source, tag, d->comm, &(mpi_req->m_request));
    return mpi_req;
}

// void dtkDistributedCommunicatorMpi::reduce(void *send, void *recv, std::size_t size, QMetaType::Type dataType, OperationType operationType, qint32 target, bool all)
// {
//     if(all)
//         MPI_Allreduce(send, recv, size, d->mpiDataType(dataType), operation_type(operationType), d->comm);
//     else
//         MPI_Reduce(send, recv, size, d->mpiDataType(dataType), operation_type(operationType), target, d->comm);
// }

void dtkDistributedCommunicatorMpi::reduce(void *send, void *recv, std::size_t size, const QMetaType& metaType, OperationType operationType, qint32 target, bool all)
{
    if(all)
        MPI_Allreduce(send, recv, size, d->mpiDataType(metaType.id()), operation_type(operationType), d->comm);
    else
        MPI_Reduce(send, recv, size, d->mpiDataType(metaType.id()), operation_type(operationType), target, d->comm);
}

void dtkDistributedCommunicatorMpi::gather(void *send, void *recv, std::size_t size, QMetaType::Type dataType, qint32 target, bool all)
{
    if(all)
        MPI_Allgather(send, size, d->mpiDataType(dataType), recv, size, d->mpiDataType(dataType), d->comm);
    else
        MPI_Gather(send, size, d->mpiDataType(dataType), recv, size, d->mpiDataType(dataType), target, d->comm);
}

void dtkDistributedCommunicatorMpi::wait(dtkDistributedRequest *request)
{
    dtkDistributedRequestMpi *req = dynamic_cast<dtkDistributedRequestMpi *>(request);
    MPI_Status mpi_status;
    MPI_Wait(&(req->m_request), &mpi_status);
    delete req;
}

//
// dtkDistributedCommunicatorMpi.cpp ends here
