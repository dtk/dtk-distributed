// dtkDistributedCommunicatorMPI.h
//
// Author: Nicolas Niclausse
// Created: 2013/02/11 09:45:37

#pragma once

#include <dtkDistributed/dtkDistributedCommunicator>

class dtkDistributedCommunicatorMpiPrivate;
class dtkDistributedBufferManager;
class mpi3DistributedRequest;

// /////////////////////////////////////////////////////////////////////////////
// dtkDistributedCommunicatorMpi
// /////////////////////////////////////////////////////////////////////////////

class dtkDistributedCommunicatorMpi : public dtkDistributedCommunicator
{
public:
     dtkDistributedCommunicatorMpi(void);
    ~dtkDistributedCommunicatorMpi(void);

    void initialize(void) override;
    bool initialized(void) override;
    void uninitialize(void) override;
    bool active(void) override;

    void spawn(QStringList hostnames, QString wrapper, QMap<QString, QString> options) override;
    void exec(QRunnable *work) override;

    void barrier(void) override;
    void unspawn(void) override;

    dtkDistributedBufferManager *createBufferManager(void) override;
    void destroyBufferManager(dtkDistributedBufferManager *& buffer_manager) override;

    void send(void *data, std::size_t size, QMetaType::Type dataType, qint32 target, qint32 tag) override;
    void send(QByteArray& array, qint32 target, qint32 tag) override;

    void broadcast(void *data, std::size_t size, QMetaType::Type dataType, qint32 source) override;
    void broadcast(QByteArray &array, qint32 source) override;
    void broadcast(QVariant &v, qint32 source) override;

    void receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag) override;
    void receive(QByteArray &v, qint32 source, qint32 tag) override;
    void receive(QByteArray &v, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status) override;

    void receive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status) override;

    dtkDistributedRequest *ireceive(void *data, std::size_t size, QMetaType::Type dataType, qint32 source, int tag) override;

    void wait(dtkDistributedRequest *req) override;

    //void reduce(void *send, void *recv, std::size_t size, QMetaType::Type dataType, OperationType operationType, qint32 target, bool all) override;
    void reduce(void *send, void *recv, std::size_t size, const QMetaType& metaType, OperationType operationType, qint32 target, bool all) override;

    void gather(void *send, void *recv, std::size_t size, QMetaType::Type dataType, qint32 target, bool all) override;

    qint32 wid(void) override;
    qint32 size(void) override;

    void *data(void) override;

    class dtkDistributedCommunicatorMpiPrivate *d;
};

//
// dtkDistributedCommunicatorMpi.h ends here
