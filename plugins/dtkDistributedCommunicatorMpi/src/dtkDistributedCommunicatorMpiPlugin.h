// dtkDistributedCommunicatorMpiPlugin.h
//

#pragma once

#include <dtkDistributed/dtkDistributedCommunicator>

#include <QtCore>
#include <QtDebug>

class dtkDistributedCommunicatorMpiPlugin : public dtkDistributedCommunicatorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkDistributedCommunicatorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkDistributedCommunicatorPlugin" FILE "dtkDistributedCommunicatorMpiPlugin.json")

public:
     dtkDistributedCommunicatorMpiPlugin(void) = default;
    ~dtkDistributedCommunicatorMpiPlugin(void) = default;

    void initialize(void);
    void uninitialize(void);
};

//
// dtkDistributedCommunicatorMpiPlugin.h ends here
