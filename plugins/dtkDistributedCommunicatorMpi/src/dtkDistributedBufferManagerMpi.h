// dtkDistributedBufferManagerMpi.h
//

#pragma once

#include <dtkDistributed/dtkDistributedBufferManager>

#include <QtCore>

#include <mpi.h>

class dtkDistributedCommunicator;

// ///////////////////////////////////////////////////////////////////
// dtkDistributedBufferManagerMpi
// ///////////////////////////////////////////////////////////////////

class dtkDistributedBufferManagerMpi : public dtkDistributedBufferManager
{
public:
     dtkDistributedBufferManagerMpi(dtkDistributedCommunicator *comm, MPI_Comm comm_shared, const QHash<int, int>& shared_map);
    ~dtkDistributedBufferManagerMpi(void);

    bool shouldCache(qint32 owner) override;

    void rlock(qint32 wid) override;
    void rlock(void) override;
    void wlock(qint32 wid) override;
    void wlock(void) override;

    void unlock(qint32 wid) override;
    void unlock(void) override;

    bool locked(qint32 wid) override;

    void get(qint32 from, std::size_t position, void *array, std::size_t nelements = 1) override;
    void put(qint32 dest, std::size_t position, void *array, std::size_t nelements = 1) override;
    void addAssign(qint32 dest, std::size_t position, void *array, std::size_t count = 1) override;
    void subAssign(qint32 dest, std::size_t position, void *array, std::size_t count = 1) override;
    void mulAssign(qint32 dest, std::size_t position, void *array, std::size_t count = 1) override;
    void divAssign(qint32 dest, std::size_t position, void *array, std::size_t count = 1) override;
    bool compareAndSwap(qint32 dest, std::size_t position, void *array, void *compare) override;

    void *allocate(std::size_t objectSize, std::size_t capacity, int metatype_id) override;
    void  deallocate(void *buffer, std::size_t objectSize) override;

    class dtkDistributedBufferManagerMpiPrivate *d = nullptr;
};

//
// dtkDistributedBufferManagerMpi.h ends here
